extern "C"
{
#include "Bits.h"
}

#include "CppUTest/TestHarness.h"


TEST_GROUP(Bits)
{
    void setup()
    {
    }

    void teardown()
    {
    }
};

TEST(Bits, BitIsSet_true)
{
    uint8_t var = 0x80;
    uint8_t offset = 7;
    CHECK_EQUAL(1, BitIsSet(&var, offset));
}

TEST(Bits, BitIsSet_false)
{
    uint8_t var = 127;
    uint8_t offset = 7;
    CHECK_EQUAL(0, BitIsSet(&var, offset));
}

TEST(Bits, SetBit_retainBitsOnSet)
{
    uint8_t var = 0xF;
    uint8_t offset = 4;
    SetBit(&var, offset, 1);
    CHECK_EQUAL(0x1F, var);
}

TEST(Bits, SetBit_retainBitsOnClear)
{
    uint8_t var = 0x1F;
    uint8_t offset = 4;
    SetBit(&var, offset, 0);
    CHECK_EQUAL(0x0F, var);
}

TEST(Bits, UpdateBits_clearBeforeSet)
{
    uint8_t var = 0x1F;
    uint8_t mask = (1 << 2)|(1 << 1)|(1 << 0);
    uint8_t value = 4;
    UpdateBits(&var, mask, value);
    CHECK_EQUAL(0x1C, var);
}
