extern "C"
{
#include "IOSpy.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(IOSpy)
{
    uint8_t fakeReg;
    uint8_t fakeReg2;
    uint8_t fakeReg3;

    void setup()
    {
      IOSpy_Create(8);
    }

    void teardown()
    {
       IOSpy_Destroy();
    }
};

TEST(IOSpy, GetLastReadAddr_isZeroWhenNotCalled)
{
    POINTERS_EQUAL(0, IOSpy_GetLastReadAddr());
}

TEST(IOSpy, Read_setsLastReadAddr)
{
    IOSpy_Read(&fakeReg);
    POINTERS_EQUAL(&fakeReg, IOSpy_GetLastReadAddr());
}

TEST(IOSpy, GetCallsMadeToRead_isZeroOnCreate)
{
    CHECK_EQUAL(0, IOSpy_GetCallsMadeToRead());
}

TEST(IOSpy, Read_IncrementsNumberOfCallsMade)
{
    uint8_t i;
    for (i = 0; i < 5; i++)
        IOSpy_Read(&fakeReg);
    CHECK_EQUAL(5, IOSpy_GetCallsMadeToRead());
}

TEST(IOSpy, GetReadAddr_recordsMultipleAddresses)
{
    IOSpy_Read(&fakeReg);
    IOSpy_Read(&fakeReg2);
    IOSpy_Read(&fakeReg3);

    POINTERS_EQUAL(&fakeReg, IOSpy_GetReadAddr());
    POINTERS_EQUAL(&fakeReg2, IOSpy_GetReadAddr());
    POINTERS_EQUAL(&fakeReg3, IOSpy_GetReadAddr());
}

/* IOSpy_Write() related *********************************************/

TEST(IOSpy, GetLastWriteAddr_isZeroWhenNotCalled)
{
    POINTERS_EQUAL(0, IOSpy_GetLastWriteAddr());
}

TEST(IOSpy, Write_setsLastWriteAddr)
{
    IOSpy_Write(&fakeReg, 0);
    POINTERS_EQUAL(&fakeReg, IOSpy_GetLastWriteAddr());
}

TEST(IOSpy, GetLastWriteValue_isZeroWhenNotCalled)
{
    CHECK_EQUAL(0, IOSpy_GetLastWriteValue());
}

TEST(IOSpy, Write_setsLastWriteValue)
{
    IOSpy_Write(&fakeReg, 0xAA);
    CHECK_EQUAL(0xAA, IOSpy_GetLastWriteValue());
}
