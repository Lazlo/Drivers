extern "C"
{
#include "IO.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(IO)
{
    uint8_t fakeReg;

    void setup()
    {
      IO_Create();
    }

    void teardown()
    {
       IO_Destroy();
    }
};

TEST(IO, Read)
{
    fakeReg = 0x33;
    CHECK_EQUAL(0x33, IO_Read(&fakeReg));
}

TEST(IO, Write)
{
    fakeReg = 0;
    IO_Write(&fakeReg, 0xAA);
    CHECK_EQUAL(0xAA, fakeReg);
}
