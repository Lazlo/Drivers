extern "C"
{
#include "Pin.h"
}

#include "CppUTest/TestHarness.h"

#define PIN_CREATE(offset) Pin_Create(offset, &pinReg.dir, &pinReg.out, &pinReg.in)

typedef struct PinRegStruct
{
    uint8_t dir;
    uint8_t in;
    uint8_t out;
} PinReg;

TEST_GROUP(Pin)
{
    Pin pin;
    uint8_t offset;
    PinReg pinReg;

    void setup()
    {
        offset = 6;
        pin = PIN_CREATE(offset);
    }

    void teardown()
    {
        Pin_Destroy(pin);
    }
};

TEST(Pin, Create_noChangeToDataDirectionReg)
{
    Pin p;
    pinReg.dir = 0x55;
    p = PIN_CREATE(0);
    Pin_Destroy(p);
    CHECK_EQUAL(0x55, pinReg.dir);
}

TEST(Pin, Create_noChangeToOutputReg)
{
    Pin p;
    pinReg.out = 0xAA;
    p = PIN_CREATE(0);
    Pin_Destroy(p);
    CHECK_EQUAL(0xAA, pinReg.out);
}

TEST(Pin, Create_noChangeToInputReg)
{
    Pin p;
    pinReg.in = 0x55;
    p = PIN_CREATE(0);
    Pin_Destroy(p);
    CHECK_EQUAL(0x55, pinReg.in);
}

TEST(Pin, GetOffset)
{
    CHECK_EQUAL(offset, Pin_GetOffset(pin));
}

TEST(Pin, GetDirection)
{
    pinReg.dir = (1 << 6);
    CHECK_EQUAL(1, Pin_GetDirection(pin));
}

TEST(Pin, SetDirection_output)
{
    pinReg.dir = 0;
    Pin_SetDirection(pin, 1);
    CHECK_EQUAL(1, Pin_GetDirection(pin));
}

TEST(Pin, SetDirection_input)
{
    pinReg.dir = 0xFF;
    Pin_SetDirection(pin, 0);
    CHECK_EQUAL(0, Pin_GetDirection(pin));
}

TEST(Pin, SetDirection_retainRegisterBitsOnSet)
{
    pinReg.dir = 0x0F;
    Pin_SetDirection(pin, 1);
    CHECK_EQUAL(0x0F + (1 << offset), pinReg.dir);
}

TEST(Pin, SetDirection_retainRegisterBitsOnClear)
{
    pinReg.dir = 0xFF;
    Pin_SetDirection(pin, 0);
    CHECK_EQUAL(0xFF - (1 << offset), pinReg.dir);
}

TEST(Pin, GetPullUpState)
{
    pinReg.out = (uint8_t)(1 << offset);
    CHECK_EQUAL(1, Pin_GetPullUpState(pin));
}

TEST(Pin, SetPullUpState_retainRegisterBitsOnTurnOn)
{
    pinReg.out = (uint8_t)(0xF0 - (1 << offset));
    Pin_SetPullUpState(pin, 1);
    CHECK_EQUAL(0xF0, pinReg.out);
}

TEST(Pin, SetPullUpState_retainRegisterBitsOnTurnOff)
{
    pinReg.out = 0xFF;
    Pin_SetPullUpState(pin, 0);
    CHECK_EQUAL(0xFF - (1 << offset), pinReg.out);
}

TEST(Pin, Write_high)
{
    pinReg.out = 0x0F;
    Pin_Write(pin, 1);
    CHECK_EQUAL(0x0F + (1 << offset), pinReg.out);
}

TEST(Pin, Write_low)
{
    pinReg.out = 0xF0;
    Pin_Write(pin, 0);
    CHECK_EQUAL(0xF0 - (1 << offset), pinReg.out);
}

TEST(Pin, Read_high)
{
    pinReg.in = (uint8_t)(1 << offset);
    CHECK_EQUAL(1, Pin_Read(pin));
}

TEST(Pin, Read_low)
{
    pinReg.in = (uint8_t)(0xFF - (1 << offset));
    CHECK_EQUAL(0, Pin_Read(pin));
}
