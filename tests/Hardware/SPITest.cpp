extern "C"
{
#include "SPI.h"
#include "SPIInternal.h"
}

#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"

TEST_GROUP(SPI)
{
    volatile uint8_t spiReg_spcr;
    volatile uint8_t spiReg_spsr;
    volatile uint8_t spiReg_spdr;

    void setup()
    {
      SPI_Create(&spiReg_spcr, &spiReg_spsr, &spiReg_spdr);
    }

    void teardown()
    {
       SPI_Destroy();
    }
};

TEST(SPI, SetInterruptEnable_turnOn)
{
    spiReg_spcr = 0;
    SPI_SetInterruptEnable(1);
    CHECK_EQUAL((1 << SPIE), spiReg_spcr);
}

TEST(SPI, SetInterruptEnable_turnOff)
{
    spiReg_spcr = (1 << SPIE);
    SPI_SetInterruptEnable(0);
    CHECK_EQUAL(0, spiReg_spcr);
}

TEST(SPI, Enable_turnOn)
{
    spiReg_spcr = 0;
    SPI_Enable(1);
    CHECK_EQUAL((1 << SPE), spiReg_spcr);
}

TEST(SPI, Enable_turnOff)
{
    spiReg_spcr = (1 << SPE);
    SPI_Enable(0);
    CHECK_EQUAL(0, spiReg_spcr);
}

TEST(SPI, SetDataOrder_LSBfirst)
{
    spiReg_spcr = 0;
    SPI_SetDataOrder(1);
    CHECK_EQUAL((1 << DORD), spiReg_spcr);
}

TEST(SPI, SetDataOrder_MSBfirst)
{
    spiReg_spcr = (1 << DORD);
    SPI_SetDataOrder(0);
    CHECK_EQUAL(0, spiReg_spcr);
}

TEST(SPI, SetMasterMode_masterMode)
{
    spiReg_spcr = 0;
    SPI_SetMasterMode(1);
    CHECK_EQUAL((1 << MSTR), spiReg_spcr);
}

TEST(SPI, SetMasterMode_slaveMode)
{
    spiReg_spcr = (1 << MSTR);
    SPI_SetMasterMode(0);
    CHECK_EQUAL(0, spiReg_spcr);
}

TEST(SPI, SetClockPolarity_leadingFallingTrailingRising)
{
    spiReg_spcr = 0;
    SPI_SetClockPolarity(1);
    CHECK_EQUAL((1 << CPOL), spiReg_spcr);
}

TEST(SPI, SetClockPolarity_leadingRisingTrailingFalling)
{
    spiReg_spcr = (1 << CPOL);
    SPI_SetClockPolarity(0);
    CHECK_EQUAL(0, spiReg_spcr);
}

TEST(SPI, SetClockPhase_leadingSetupTraillingSample)
{
    spiReg_spcr = 0;
    SPI_SetClockPhase(1);
    CHECK_EQUAL((1 << CPHA), spiReg_spcr);
}

TEST(SPI, SetClockPhase_leadingSampleTraillingSetup)
{
    spiReg_spcr = (1 << CPHA);
    SPI_SetClockPhase(0);
    CHECK_EQUAL(0, spiReg_spcr);
}

TEST(SPI, SetClockRate_set2)
{
    spiReg_spcr = (1 << SPR1)|(1 << SPR0);
    spiReg_spsr = 0;
    SPI_SetClockRate(2);
    CHECK_EQUAL(0, spiReg_spcr);
    CHECK_EQUAL((1 << SPI2X), spiReg_spsr);
}

TEST(SPI, SetClockRate_set4)
{
    spiReg_spcr = (1 << SPR1)|(1 << SPR0);
    spiReg_spsr = (1 << SPI2X);
    SPI_SetClockRate(4);
    CHECK_EQUAL(0, spiReg_spcr);
    CHECK_EQUAL(0, spiReg_spsr);
}

TEST(SPI, SetClockRate_set8)
{
    spiReg_spcr = (1 << SPR1);
    spiReg_spsr = 0;
    SPI_SetClockRate(8);
    CHECK_EQUAL((1 << SPR0), spiReg_spcr);
    CHECK_EQUAL((1 << SPI2X), spiReg_spsr);
}

TEST(SPI, SetClockRate_set16)
{
    spiReg_spcr = (1 << SPR1);
    spiReg_spsr = (1 << SPI2X);
    SPI_SetClockRate(16);
    CHECK_EQUAL((1 << SPR0), spiReg_spcr);
    CHECK_EQUAL(0, spiReg_spsr);
}

TEST(SPI, SetClockRate_set32)
{
    spiReg_spcr = (1 << SPR0);
    spiReg_spsr = 0;
    SPI_SetClockRate(32);
    CHECK_EQUAL((1 << SPR1), spiReg_spcr);
    CHECK_EQUAL((1 << SPI2X), spiReg_spsr);
}

TEST(SPI, SetClockRate_set64)
{
    spiReg_spcr = (1 << SPR0);
    spiReg_spsr = (1 << SPI2X);
    SPI_SetClockRate(64);
    CHECK_EQUAL((1 << SPR1), spiReg_spcr);
    CHECK_EQUAL(0, spiReg_spsr);
}

TEST(SPI, SetClockRate_set128)
{
    spiReg_spcr = 0;
    spiReg_spsr = (1 << SPI2X);
    SPI_SetClockRate(128);
    CHECK_EQUAL((1 << SPR1)|(1 << SPR0), spiReg_spcr);
    CHECK_EQUAL(0, spiReg_spsr);
}

TEST(SPI, SetClockRate_noChangeOnOutOfBounds)
{
    spiReg_spcr = 0xFF;
    spiReg_spsr = 0xFF;
    SPI_SetClockRate(255);
    CHECK_EQUAL(0xFF, spiReg_spcr);
    CHECK_EQUAL(0xFF, spiReg_spsr);
}

TEST(SPI, GetInterruptFlag)
{
    spiReg_spsr = (1 << SPIF);
    CHECK_EQUAL(1, SPI_GetInterruptFlag());
}

TEST(SPI, GetWriteColisionFlag)
{
    spiReg_spsr = (1 << WCOL);
    CHECK_EQUAL(1, SPI_GetWriteCollisionFlag());
}

TEST(SPI, Read)
{
    spiReg_spdr = 0x55;
    CHECK_EQUAL(0x55, SPI_Read());
}

TEST(SPI, Write)
{
    spiReg_spdr = 0;
    SPI_Write(0xAA);
    CHECK_EQUAL(0xAA, spiReg_spdr);
}

static void FakeSPI_Write(const uint8_t byte)
{
    mock().actualCall("SPI_Write").withParameter("byte", byte);
}

#if 1
static uint8_t FakeSPI_GetInterruptFlag(void)
{
    mock().actualCall("SPI_GetInterruptFlag");
    return 1;
}
#else
static uint8_t fakeSPI_GetInterruptFlag_returnTrueOnNthCall = 10;
static uint8_t fakeSPI_GetInterruptFlag_calledNTimes = 0;

static uint8_t FakeSPI_GetInterruptFlag(void)
{
    uint8_t rc = 0;
    if (++fakeSPI_GetInterruptFlag_calledNTimes == fakeSPI_GetInterruptFlag_returnTrueOnNthCall)
        rc = 1;
    mock().actualCall("SPI_GetInterruptFlag");
    return rc;
}
#endif

static uint8_t FakeSPI_Read(void)
{
    uint8_t data;
    mock().actualCall("SPI_Read");
    data = (uint8_t)mock().getData("spi_read_return_value").getIntValue();
    return data;
}

TEST(SPI, Trx_writeWaitRead)
{
    uint8_t expected_spi_write_value = 0x77;
    uint8_t expected_spi_read_value = 0x88;
    uint8_t actual_spi_read_value;

    void (*fp_saved_SPI_Write)(const uint8_t) = SPI_Write;
    uint8_t (*fp_saved_SPI_GetInterruptFlag)(void) = SPI_GetInterruptFlag;
    uint8_t (*fp_saved_SPI_Read)(void) = SPI_Read;

    spiReg_spsr = (1 << SPIF);

    SPI_Write = FakeSPI_Write;
    SPI_GetInterruptFlag = FakeSPI_GetInterruptFlag;
    SPI_Read = FakeSPI_Read;

    mock().expectOneCall("SPI_Write").withParameter("byte", expected_spi_write_value);
    mock().expectOneCall("SPI_GetInterruptFlag");
    mock().expectOneCall("SPI_Read").andReturnValue(expected_spi_read_value);
    mock().setData("spi_read_return_value", expected_spi_read_value);

    actual_spi_read_value = SPI_Trx(expected_spi_write_value);

    SPI_Write = fp_saved_SPI_Write;
    SPI_GetInterruptFlag = fp_saved_SPI_GetInterruptFlag;
    SPI_Read = fp_saved_SPI_Read;

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(expected_spi_read_value, actual_spi_read_value);
}
