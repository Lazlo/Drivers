extern "C"
{
#include "Timer.h"
}

#include "CppUTest/TestHarness.h"

#define CS_MAX	((1 << 2)|(1 << 1)|(1 << 0))

#define TIMER_CREATE(target, num) Timer_Create(target, num, &timerReg.tccr, &timerReg.tccrb, \
                                        &timerReg.tcnt, &timerReg.tcnth, \
                                        &timerReg.ocr, &timerReg.ocrh, \
                                        &timerReg.ocrb, &timerReg.ocrbh, \
                                        &timerReg.icr, &timerReg.icrh, \
                                        &timerReg.timsk, &timerReg.tifr)

struct TimerRegisterStruct
{
    uint8_t tccr;
    uint8_t tccrb;
    uint8_t tcnt;
    uint8_t tcnth;
    uint8_t ocr;
    uint8_t ocrh;
    uint8_t ocrb;
    uint8_t ocrbh;
    uint8_t icr;
    uint8_t icrh;
    uint8_t timsk;
    uint8_t tifr;
};

TEST_GROUP(Timer)
{
    Timer timer0;
    Timer timer1;
    Timer timer2;
    struct TimerRegisterStruct timerReg;

    uint8_t expectTCCR;
    uint8_t expectTCCRB;

    void setup()
    {
        timer0 = TIMER_CREATE(0, 0);
        timer1 = TIMER_CREATE(0, 1);
        timer2 = TIMER_CREATE(0, 2);
    }

    void teardown()
    {
        Timer_Destroy(timer0);
        Timer_Destroy(timer1);
        Timer_Destroy(timer2);
    }
};

TEST_GROUP(Timer_m328p)
{
    Timer timer0;
    Timer timer1;
    Timer timer2;
    struct TimerRegisterStruct timerReg;

    void setup()
    {
        timer0 = TIMER_CREATE(1, 0);
        timer1 = TIMER_CREATE(1, 1);
        timer2 = TIMER_CREATE(1, 2);
    }
    void teardown()
    {
        Timer_Destroy(timer0);
        Timer_Destroy(timer1);
        Timer_Destroy(timer2);
    }
};

/* Create *************************************************************/

TEST(Timer, Create_Singleton)
{
    Timer a, b;

    a = TIMER_CREATE(0, 0);
    b = TIMER_CREATE(0, 0);

    POINTERS_EQUAL(a, b);
}

TEST(Timer, Create_invalidIdDoesNoHarm)
{
    CHECK_FALSE(TIMER_CREATE(0, 255));
}

TEST(Timer, Create_initializeFpSetClockSource)
{
	POINTERS_EQUAL(Timer_SetClockSource, fp_Timer_SetClockSource);
}

/* GetId **************************************************************/

TEST(Timer, GetId)
{
    uint8_t id = 255;
    id  = Timer_GetId(timer0);
    CHECK_EQUAL(0, id);
}

/* SetClockSource *****************************************************/

TEST(Timer, SetClockSource_retainRegisterBits)
{
    uint8_t cs = CS_MAX;
    expectTCCRB = 255;
    timerReg.tccrb = 255 - CS_MAX;
    Timer_SetClockSource(timer2, cs);
    CHECK_EQUAL(expectTCCRB, timerReg.tccrb);
}

TEST(Timer, SetClockSource_clearAllBits)
{
    uint8_t cs = 0;
    expectTCCRB = 0;
    timerReg.tccrb = CS_MAX;
    Timer_SetClockSource(timer2, cs);
    CHECK_EQUAL(expectTCCRB, timerReg.tccrb);
}

TEST(Timer, SetClockSource_setAllBits)
{
    uint8_t cs = 7;
    expectTCCRB = 255;
    timerReg.tccrb = 255 - CS_MAX;
    Timer_SetClockSource(timer2, cs);
    CHECK_EQUAL(expectTCCRB, timerReg.tccrb);
}

TEST(Timer, SetClockSource_clearBitsOnSet)
{
    /* We created this test to make sure, the bit setting function
     * will also clear bits in cases where the previous clock source
     * value was for example 7 and we want to set the clock source to
     * a lower value that requires the implementaion to clear some bits
     * like in the case of 3. */
    uint8_t cs = 3;
    expectTCCRB = 3;
    timerReg.tccrb = CS_MAX;
    Timer_SetClockSource(timer2, cs);
    CHECK_EQUAL(expectTCCRB, timerReg.tccrb);
}

TEST(Timer, SetClockSource_noChangeOnOutOfBounds)
{
    uint8_t cs = 255;
    expectTCCRB = 0;
    Timer_SetClockSource(timer2, cs);
    CHECK_EQUAL(expectTCCRB, timerReg.tccrb);
}

/* SetPrescaler *******************************************************/

static void (*fp_saved_Timer_SetClockSource)(Timer, const uint8_t);

static void replace_fp_Timer_SetClockSource(void (*fp)(Timer, const uint8_t))
{
	fp_saved_Timer_SetClockSource = fp_Timer_SetClockSource;
	fp_Timer_SetClockSource = fp;
}

static void restore_fp_Timer_SetClockSource(void)
{
	fp_Timer_SetClockSource = fp_saved_Timer_SetClockSource;
}

static uint8_t last_cs;

static void FakeTimer_SetClockSource(Timer self, const uint8_t cs)
{
	last_cs = cs;
}

TEST(Timer, SetPrescaler_timer0_ps1)
{
	const uint16_t ps = 1;
	const uint8_t cs = 1;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer0, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer0_ps8)
{
	const uint16_t ps = 8;
	const uint8_t cs = 2;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer0, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer0_ps64)
{
	const uint16_t ps = 64;
	const uint8_t cs = 3;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer0, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer0_ps256)
{
	const uint16_t ps = 256;
	const uint8_t cs = 4;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer0, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer0_ps1024)
{
	const uint16_t ps = 1024;
	const uint8_t cs = 5;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer0, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer2_ps1)
{
	const uint16_t ps = 1;
	const uint8_t cs = 1;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer2, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer2_ps8)
{
	const uint16_t ps = 8;
	const uint8_t cs = 2;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer2, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer2_ps32)
{
	const uint16_t ps = 32;
	const uint8_t cs = 3;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer2, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer2_ps64)
{
	const uint16_t ps = 64;
	const uint8_t cs = 4;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer2, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer2_ps128)
{
	const uint16_t ps = 128;
	const uint8_t cs = 5;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer2, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer2_ps256)
{
	const uint16_t ps = 256;
	const uint8_t cs = 6;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer2, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

TEST(Timer, SetPrescaler_timer2_ps1024)
{
	const uint16_t ps = 1024;
	const uint8_t cs = 7;
	replace_fp_Timer_SetClockSource(FakeTimer_SetClockSource);
	Timer_SetPrescaler(timer2, ps);
	CHECK_EQUAL(cs, last_cs);
	restore_fp_Timer_SetClockSource();
}

/* SetCompareMode *****************************************************/

TEST(Timer, SetCompareMode_retainRegisterBits)
{
    uint8_t com = 3;
    expectTCCR = 255;
    timerReg.tccr = 255 - ((1 << 5)|(1 << 4));
    Timer_SetCompareMode(timer2, com, 0);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

TEST(Timer, SetCompareMode_clearAllBits)
{
    uint8_t com = 0;
    expectTCCR = 255 - ((1 << 5)|(1 << 4));
    timerReg.tccr = 255;
    Timer_SetCompareMode(timer2, com, 0);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

TEST(Timer, SetCompareMode_setAllBits)
{
    uint8_t com = 3;
    expectTCCR = 255;
    timerReg.tccr = 255 - ((1 << 5)|(1 << 4));
    Timer_SetCompareMode(timer2, com, 0);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

TEST(Timer, SetCompareMode_clearBitsOnSet)
{
    uint8_t com = 2;
    expectTCCR = 255 - (1 << 4);
    timerReg.tccr = 255;
    Timer_SetCompareMode(timer2, com, 0);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

TEST(Timer, SetCompareMode_noChangeOnOutOfBounds)
{
    uint8_t com = 8;
    expectTCCR = 0;
    Timer_SetCompareMode(timer2, com, 0);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

/* SetCompareMode - Timer specific -----------------------------------*/

TEST(Timer, SetCompareMode_noChangeForTimer0)
{
    uint8_t com = 3;
    expectTCCR = 0;
    Timer_SetCompareMode(timer0, com, 0);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

TEST(Timer, SetCompareMode_timer1_unitA)
{
    uint8_t com = 3;
    expectTCCR = (1 << 7)|(1 << 6);
    Timer_SetCompareMode(timer1, com, 0);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

/* TODO Check out of bounds for "unit" argument */
/* TODO Check for timer2 in combination with unit argument */

/* SetWaveGenerationMode **********************************************/

TEST(Timer, SetWaveGenerationMode_retainRegisterBits)
{
    uint8_t wgm = 3;
    expectTCCR = 255;
    timerReg.tccr = 255 - ((1 << 6)|(1 << 3));
    Timer_SetWaveGenerationMode(timer2, wgm);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

TEST(Timer, SetWaveGenerationMode_clearAllBits)
{
    uint8_t wgm = 0;
    expectTCCR = 183;
    timerReg.tccr = 255;
    Timer_SetWaveGenerationMode(timer2, wgm);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

TEST(Timer, SetWaveGenerationMode_setAllBits)
{
    uint8_t wgm = 3;
    expectTCCR = 255;
    timerReg.tccr = 255 - ((1 << 6)|(1 << 3));
    Timer_SetWaveGenerationMode(timer2, wgm);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

TEST(Timer, SetWaveGenerationMode_clearBitsOnSet)
{
    uint8_t wgm = 1;
    expectTCCR = 255 - (1 << 3);
    timerReg.tccr = 255 - (1 << 6);
    Timer_SetWaveGenerationMode(timer2, wgm);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

TEST(Timer, SetWaveGenerationMode_noChangeOnOutOfBounds)
{
    uint8_t wgm = 7;
    expectTCCR = 255 - ((1 << 6)|(1 << 3));
    timerReg.tccr = 255 - ((1 << 6)|(1 << 3));
    Timer_SetWaveGenerationMode(timer2, wgm);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

/* SetWaveGenerationMode - Timer specific ----------------------------*/

TEST(Timer, SetWaveGenerationMode_noChangeForTimer0)
{
    uint8_t wgm = 3;
    expectTCCR = 0;
    Timer_SetWaveGenerationMode(timer0, wgm);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
}

TEST(Timer, SetWaveGenerationMode_timer1)
{
    uint8_t wgm = 15;
    expectTCCR = (1 << 1)|(1 << 0);
    expectTCCRB = (1 << 4)|(1 << 3);
    Timer_SetWaveGenerationMode(timer1, wgm);
    CHECK_EQUAL(expectTCCR, timerReg.tccr);
    CHECK_EQUAL(expectTCCRB, timerReg.tccrb);
}

/* SetCounter *********************************************************/

TEST(Timer, SetCounter)
{
    Timer_SetCounter(timer2, 201);
    CHECK_EQUAL(201, timerReg.tcnt);
}

TEST(Timer, SetCounter_timer1)
{
    uint16_t top = 0xFFFF;
    Timer_SetCounter(timer1, top);
    CHECK_EQUAL(0xFFFF, (timerReg.tcnth << 8)|timerReg.tcnt);
}

/* SetCompareMatch ****************************************************/

TEST(Timer, SetCompareMatch_noChangeForTimer0)
{
    const uint16_t top = 0xFFFF;
    Timer_SetCompareMatch(timer0, top, 0);
    CHECK_EQUAL(0, (timerReg.ocrh << 8)|timerReg.ocr);
}

TEST(Timer, SetCompareMatch_timer1)
{
    const uint16_t top = 0xAAAA;
    Timer_SetCompareMatch(timer1, top, 0);
    CHECK_EQUAL(0xAAAA, (timerReg.ocrh << 8)|timerReg.ocr);
}

TEST(Timer, SetCompareMatch_timer1_unitB)
{
    const uint16_t top = 0x5555;
    Timer_SetCompareMatch(timer1, top, 1);
    CHECK_EQUAL(0x5555, (timerReg.ocrbh << 8)|timerReg.ocrb);
}

TEST(Timer, SetCompareMatch_timer2NoChangeToOCRH)
{
    const uint16_t top = 0x01FF;
    Timer_SetCompareMatch(timer2, top, 0);
    CHECK_EQUAL(0xFF, (timerReg.ocrh << 8)|timerReg.ocr);
}

/* GetInputCaptureValue **********************************************/

TEST(Timer, GetInputCaptureValue_timer0_noChange)
{
    timerReg.icr = 0xFF;
    timerReg.icrh = 0xFF;
    CHECK_EQUAL(0, Timer_GetInputCaptureValue(timer0));
}

TEST(Timer, GetInputCaptureValue_timer1)
{
    uint16_t val;
    timerReg.icr = 0xAA;
    timerReg.icrh = 0x55;
    val = Timer_GetInputCaptureValue(timer1);
    CHECK_EQUAL(0x55AA, val);
}

TEST(Timer, GetInputCaptureValue_timer2_noChange)
{
    timerReg.icr = 0xFF;
    timerReg.icrh = 0xFF;
    CHECK_EQUAL(0, Timer_GetInputCaptureValue(timer2));
}

/* SetInputCaptureValue ***********************************************/

TEST(Timer, SetInputCaptureValue_timer0_noChange)
{
    Timer_SetInputCaptureValue(timer0, 0xFFFF);
    CHECK_EQUAL(0, (timerReg.icrh << 8)|timerReg.icr);
}

TEST(Timer, SetInputCaptureValue_timer1)
{
    Timer_SetInputCaptureValue(timer1, 0xAA55);
    CHECK_EQUAL(0xAA55, (timerReg.icrh << 8)|timerReg.icr);
}

TEST(Timer, SetInputCaptureValue_timer2_noChange)
{
    Timer_SetInputCaptureValue(timer2, 0xFFFF);
    CHECK_EQUAL(0, (timerReg.icrh << 8)|timerReg.icr);
}

/* SetOverflowInterrupt ***********************************************/

TEST(Timer, SetOverflowInterrupt_turnOn)
{
    Timer_SetOverflowInterrupt(timer2, 1);
    CHECK_EQUAL((1 << 6), timerReg.timsk);
}

TEST(Timer, SetOverflowInterrupt_turnOff)
{
    timerReg.timsk = (1 << 6);
    Timer_SetOverflowInterrupt(timer2, 0);
    CHECK_EQUAL(0, timerReg.timsk);
}

TEST(Timer_m328p, SetOverflowInterrupt_turnOn)
{
    timerReg.timsk = 0;
    Timer_SetOverflowInterrupt(timer2, 1);
    CHECK_EQUAL((1 << 0), timerReg.timsk);
}

TEST(Timer, SetOverflowInterrupt_retainRegisterBitsOnSet)
{
    timerReg.timsk = 255 - (1 << 6);
    Timer_SetOverflowInterrupt(timer2, 1);
    CHECK_EQUAL(255, timerReg.timsk);
}

TEST(Timer, SetOverflowInterrupt_retainRegisterBitsOnClear)
{
    timerReg.timsk = 255;
    Timer_SetOverflowInterrupt(timer2, 0);
    CHECK_EQUAL(255 - (1 << 6), timerReg.timsk);
}

TEST(Timer, SetOverflowInterrupt_noChangeOnOutOfBounds)
{
    Timer_SetOverflowInterrupt(timer2, 23);
    CHECK_EQUAL(0, timerReg.timsk);
}

/* SetOverflowInterrupt - Timer specific -----------------------------*/

TEST(Timer, SetOverflowInterrupt_timer0)
{
    Timer_SetOverflowInterrupt(timer0, 1);
    CHECK_EQUAL((1 << 0), timerReg.timsk);
}

TEST(Timer, SetOverflowInterrupt_timer1)
{
    Timer_SetOverflowInterrupt(timer1, 1);
    CHECK_EQUAL((1 << 2), timerReg.timsk);
}

/* SetCompareMatchInterrupt ******************************************/

TEST(Timer, SetCompareMatchInterrupt_turnOn)
{
    Timer_SetCompareMatchInterrupt(timer2, 1, 0);
    CHECK_EQUAL((1 << 7), timerReg.timsk);
}

TEST(Timer, SetCompareMatchInterrupt_turnOff)
{
    timerReg.timsk = 255;
    Timer_SetCompareMatchInterrupt(timer2, 0, 0);
    CHECK_EQUAL(255 - (1 << 7), timerReg.timsk);
}

/* SetCompareMatchInterrupt - Timer specific ------------------------*/

TEST(Timer, SetCompareMatchInterrupt_timer0NoChange)
{
    Timer_SetCompareMatchInterrupt(timer0, 1, 0);
    CHECK_EQUAL(0, timerReg.timsk);
}

TEST(Timer, SetCompareMatchInterrupt_timer1_turnOnUnitA)
{
    Timer_SetCompareMatchInterrupt(timer1, 1, 0);
    CHECK_EQUAL((1 << 4), timerReg.timsk);
}

TEST(Timer, SetCompareMatchInterrupt_timer1_turnOnUnitB)
{
    Timer_SetCompareMatchInterrupt(timer1, 1, 1);
    CHECK_EQUAL((1 << 3), timerReg.timsk);
}

/* SetInputCaptureInterrupt ******************************************/

TEST(Timer, SetInputCaptureInterrupt_timer0_noChange)
{
    Timer_SetInputCaptureInterrupt(timer0, 1);
    CHECK_EQUAL(0, timerReg.timsk);
}

TEST(Timer, SetInputCaptureInterrupt_timer1)
{
    Timer_SetInputCaptureInterrupt(timer1, 1);
    CHECK_EQUAL((1 << 5), timerReg.timsk);
}

TEST(Timer, SetInputCaptureInterrupt_timer2_noChange)
{
    Timer_SetInputCaptureInterrupt(timer2, 1);
    CHECK_EQUAL(0, timerReg.timsk);
}

/* GetOverflowInterruptFlag  *****************************************/

TEST(Timer, GetOverflowInterruptFlag_timer0_isSet)
{
    uint8_t flag;
    timerReg.tifr = (1 << 0);
    flag = Timer_GetOverflowInterruptFlag(timer0);
    CHECK_EQUAL(1, flag);
}

TEST(Timer, GetOverflowInterruptFlag_timer0_isNotSet)
{
    uint8_t flag;
    flag = Timer_GetOverflowInterruptFlag(timer0);
    CHECK_EQUAL(0, flag);
}

TEST(Timer, GetOverflowInterruptFlag_timer1_isSet)
{
    uint8_t flag;
    timerReg.tifr = (1 << 2);
    flag = Timer_GetOverflowInterruptFlag(timer1);
    CHECK_EQUAL(1, flag);
}

TEST(Timer, GetOverflowInterruptFlag_timer1_isNotSet)
{
    uint8_t flag;
    flag = Timer_GetOverflowInterruptFlag(timer1);
    CHECK_EQUAL(0, flag);
}

TEST(Timer, GetOverflowInterruptFlag_timer2_isSet)
{
    timerReg.tifr = (1 << 6);
    CHECK_EQUAL(1, Timer_GetOverflowInterruptFlag(timer2));
}

TEST(Timer, GetOverflowInterruptFlag_timer2_isNotSet)
{
    CHECK_EQUAL(0, Timer_GetOverflowInterruptFlag(timer2));
}

/* GetCompareMatchInterruptFlag **************************************/

TEST(Timer, GetCompareMatchInterruptFlag_timer0_return)
{
    uint8_t flag;
    timerReg.tifr = 255;
    flag = Timer_GetCompareMatchInterruptFlag(timer0, 0);
    CHECK_EQUAL(0, flag);
}

TEST(Timer, GetCompareMatchInterruptFlag_timer1_unitA_isSet)
{
    uint8_t flag;
    timerReg.tifr = (1 << 4);
    flag = Timer_GetCompareMatchInterruptFlag(timer1, 0);
    CHECK_EQUAL(1, flag);
}

TEST(Timer, GetCompareMatchInterruptFlag_timer1_unitA_isNotSet)
{
    uint8_t flag;
    flag = Timer_GetCompareMatchInterruptFlag(timer1, 0);
    CHECK_EQUAL(0, flag);
}

TEST(Timer, GetCompareMatchInterruptFlag_timer1_unitB_isSet)
{
    uint8_t flag;
    timerReg.tifr = (1 << 3);
    flag = Timer_GetCompareMatchInterruptFlag(timer1, 1);
    CHECK_EQUAL(1, flag);
}

TEST(Timer, GetCompareMatchInterruptFlag_timer2_isSet)
{
    timerReg.tifr = (1 << 7);
    CHECK_EQUAL(1, Timer_GetCompareMatchInterruptFlag(timer2, 0));
}

TEST(Timer, GetCompareMatchInterruptFlag_timer2_isNotSet)
{
    CHECK_EQUAL(0, Timer_GetCompareMatchInterruptFlag(timer2, 0));
}

/* GetInputCaptureInterruptFlag **************************************/

TEST(Timer, GetInputCaptureInterruptFlag_timer0_return)
{
    timerReg.tifr = 255;
    CHECK_EQUAL(0, Timer_GetInputCaptureInterruptFlag(timer0));
}

TEST(Timer, GetInputCaptureInterruptFlag_timer1)
{
    timerReg.tifr = (1 << 5);
    CHECK_EQUAL(1, Timer_GetInputCaptureInterruptFlag(timer1));
}

TEST(Timer, GetInputCaptureInterruptFlag_timer2_return)
{
    timerReg.tifr = 255;
    CHECK_EQUAL(0, Timer_GetInputCaptureInterruptFlag(timer2));
}

/* ClearOverflowInterruptFlag ****************************************/

TEST(Timer, ClearOverflowInterruptFlag_timer0)
{
    Timer_ClearOverflowInterruptFlag(timer0);
    CHECK_EQUAL((1 << 0), timerReg.tifr);
}

TEST(Timer, ClearOverflowInterruptFlag_timer1)
{
    Timer_ClearOverflowInterruptFlag(timer1);
    CHECK_EQUAL((1 << 2), timerReg.tifr);
}

TEST(Timer, ClearOverflowInterruptFlag_timer2)
{
    Timer_ClearOverflowInterruptFlag(timer2);
    CHECK_EQUAL((1 << 6), timerReg.tifr);
}

TEST(Timer, ClearOverflowInterruptFlag_retainRegisterBits)
{
    timerReg.tifr = 255 - (1 << 0);
    Timer_ClearOverflowInterruptFlag(timer0);
    CHECK_EQUAL(255, timerReg.tifr);
}

/* ClearCompareMatchInterruptFlag ************************************/

TEST(Timer, ClearCompareMatchInterruptFlag_timer0_noChange)
{
    Timer_ClearCompareMatchInterruptFlag(timer0, 0);
    CHECK_EQUAL(0, timerReg.tifr);
}

TEST(Timer, ClearCompareMatchInterruptFlag_timer1_unitA)
{
    Timer_ClearCompareMatchInterruptFlag(timer1, 0);
    CHECK_EQUAL((1 << 4), timerReg.tifr);
}

TEST(Timer, ClearCompareMatchInterruptFlag_timer1_unitB)
{
    Timer_ClearCompareMatchInterruptFlag(timer1, 1);
    CHECK_EQUAL((1 << 3), timerReg.tifr);
}

TEST(Timer, ClearCompareMatchInterruptFlag_timer2)
{
    Timer_ClearCompareMatchInterruptFlag(timer2, 0);
    CHECK_EQUAL((1 << 7), timerReg.tifr);
}

TEST(Timer, ClearCompareMatchInterruptFlag_retainRegisterBits)
{
    timerReg.tifr = 255 - (1 << 7);
    Timer_ClearCompareMatchInterruptFlag(timer2, 0);
    CHECK_EQUAL(255, timerReg.tifr);
}

/* ClearInputCaptureInterruptFlag ************************************/

TEST(Timer, ClearInputCaptureInterruptFlag_timer0_noChange)
{
    Timer_ClearInputCaptureInterruptFlag(timer0);
    CHECK_EQUAL(0, timerReg.tifr);
}

TEST(Timer, ClearInputCaptureInterruptFlag_timer1)
{
    Timer_ClearInputCaptureInterruptFlag(timer1);
    CHECK_EQUAL((1 << 5), timerReg.tifr);
}

TEST(Timer, ClearInputCaptureInterruptFlag_timer2_noChange)
{
    Timer_ClearInputCaptureInterruptFlag(timer2);
    CHECK_EQUAL(0, timerReg.tifr);
}

TEST(Timer, ClearInputCaptureInterruptFlag_retainRegisterBits)
{
    timerReg.tifr = 255 - (1 << 5);
    Timer_ClearInputCaptureInterruptFlag(timer1);
    CHECK_EQUAL(255, timerReg.tifr);
}

/* Timer Feature - is16Bit *******************************************/

TEST(Timer, Is16Bit_timer0)
{
    CHECK_EQUAL(0, Timer_Is16Bit(timer0));
}

TEST(Timer, Is16Bit_timer1)
{
    CHECK_EQUAL(1, Timer_Is16Bit(timer1));
}

TEST(Timer, Is16Bit_timer2)
{
    CHECK_EQUAL(0, Timer_Is16Bit(timer2));
}

/* Timer Feature - hasWaveGenerationUnit *****************************/

TEST(Timer, HasWaveGenerationUnit_timer0)
{
    CHECK_EQUAL(0, Timer_HasWaveGenerationUnit(timer0));
}

TEST(Timer, HasWaveGenerationUnit_timer1)
{
    CHECK_EQUAL(1, Timer_HasWaveGenerationUnit(timer1));
}

TEST(Timer, HasWaveGenerationUnit_timer2)
{
    CHECK_EQUAL(1, Timer_HasWaveGenerationUnit(timer2));
}

/* Timer Feature - hasCompareMatchUnit *******************************/

TEST(Timer, HasCompareMatchUnit_timer0)
{
    CHECK_EQUAL(0, Timer_HasCompareMatchUnit(timer0));
}

TEST(Timer, HasCompareMatchUnit_timer1)
{
    CHECK_EQUAL(1, Timer_HasCompareMatchUnit(timer1));
}

TEST(Timer, HasCompareMatchUnit_timer2)
{
    CHECK_EQUAL(1, Timer_HasCompareMatchUnit(timer2));
}

/* Timer Feature - hasDualCompareMatchUnit ***************************/

TEST(Timer, HasDualCompareMatchUnit_timer0)
{
    CHECK_EQUAL(0, Timer_HasDualCompareMatchUnit(timer0));
}

TEST(Timer, HasDualCompareMatchUnit_timer1)
{
    CHECK_EQUAL(1, Timer_HasDualCompareMatchUnit(timer1));
}

TEST(Timer, HasDualCompareMatchUnit_timer2)
{
    CHECK_EQUAL(0, Timer_HasDualCompareMatchUnit(timer2));
}

/* Timer Feature - hasInputCaptureUnit *******************************/

TEST(Timer, HasInputCaptureUnit_timer0)
{
    CHECK_EQUAL(0, Timer_HasInputCaptureUnit(timer0));
}

TEST(Timer, HasInputCaptureUnit_timer1)
{
    CHECK_EQUAL(1, Timer_HasInputCaptureUnit(timer1));
}

TEST(Timer, HasInputCaptureUnit_timer2)
{
    CHECK_EQUAL(0, Timer_HasInputCaptureUnit(timer2));
}
