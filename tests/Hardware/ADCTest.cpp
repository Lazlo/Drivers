extern "C"
{
#include "ADC.h"
#include "IO.h"
#include "IOSpy.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(ADC)
{
    uint8_t adcReg_admux;
    uint8_t adcReg_adcsra;
    uint8_t adcReg_adch;
    uint8_t adcReg_adcl;

    void setup()
    {
      ADC_Create(&adcReg_admux, &adcReg_adcsra, &adcReg_adch, &adcReg_adcl);
    }

    void teardown()
    {
       ADC_Destroy();
    }
};

TEST(ADC, SetReferenceVoltage_AREF)
{
    adcReg_admux = (1 << 7)|(1 << 6);
    ADC_SetReferenceVoltage(0);
    CHECK_EQUAL(0, adcReg_admux);
}

TEST(ADC, SetReferenceVoltage_AVCC)
{
    adcReg_admux = (1 << 7);
    ADC_SetReferenceVoltage(1);
    CHECK_EQUAL((1 << 6), adcReg_admux);
}

TEST(ADC, SetReferenceVoltage_noChnageOnReserved)
{
    adcReg_admux = 0;
    ADC_SetReferenceVoltage(2);
    CHECK_EQUAL(0, adcReg_admux);
}

TEST(ADC, SetReferenceVoltage_Internal2V56)
{
    adcReg_admux = 0;
    ADC_SetReferenceVoltage(3);
    CHECK_EQUAL(((1 << 7)|(1 << 6)), adcReg_admux);
}

TEST(ADC, SetReferenceVoltage_noChangeOnOutOfBounds)
{
    adcReg_admux = 0;
    ADC_SetReferenceVoltage(7);
    CHECK_EQUAL(0, adcReg_admux);
}

TEST(ADC, SetLeftAdjustResult_turnOn)
{
    adcReg_admux = 0;
    ADC_SetLeftAdjustResult(1);
    CHECK_EQUAL((1 << 5), adcReg_admux);
}

TEST(ADC, SetLeftAdjustResult_turnOff)
{
    adcReg_admux = (1 << 5),
    ADC_SetLeftAdjustResult(0);
    CHECK_EQUAL(0, adcReg_admux);
}

TEST(ADC, SetChannel_Ch0)
{
    adcReg_admux = 0x0F;
    ADC_SetChannel(0);
    CHECK_EQUAL(0, adcReg_admux);
}

TEST(ADC, SetChannel_Ch7)
{
    adcReg_admux = (1 << 3);
    ADC_SetChannel(7);
    CHECK_EQUAL((1 << 2)|(1 << 1)|(1 << 0), adcReg_admux);
}

TEST(ADC, SetChannel_noChangeOnOutOfBounds)
{
    adcReg_admux = 0;
    ADC_SetChannel(255);
    CHECK_EQUAL(0, adcReg_admux);
}

TEST(ADC, SetChannel_noChangeOnReserved_8Min)
{
    adcReg_admux = 0;
    CHECK_EQUAL(0, adcReg_admux);
}

TEST(ADC, SetChannel_noChangeOnReserved_13Max)
{
    adcReg_admux = 0;
    ADC_SetChannel(13);
    CHECK_EQUAL(0, adcReg_admux);
}

TEST(ADC, SetChannel_1V3)
{
    adcReg_admux = 0;
    ADC_SetChannel(14);
    CHECK_EQUAL(14, adcReg_admux);
}

TEST(ADC, SetChannel_0V)
{
    adcReg_admux = 0;
    ADC_SetChannel(15);
    CHECK_EQUAL(15, adcReg_admux);
}

TEST(ADC, Enable_turnOn)
{
    adcReg_adcsra = 0;
    ADC_Enable(1);
    CHECK_EQUAL((1 << 7), adcReg_adcsra);
}

TEST(ADC, Enable_turnOff)
{
    adcReg_adcsra = (1 << 7);
    ADC_Enable(0);
    CHECK_EQUAL(0, adcReg_adcsra);
}

TEST(ADC, StartConversion)
{
    adcReg_adcsra = 0;
    ADC_StartConversion();
    CHECK_EQUAL((1 << 6), adcReg_adcsra);
}

TEST(ADC, IsConversionComplete_notComplete)
{
    adcReg_adcsra = (1 << 6);
    CHECK_EQUAL(0, ADC_IsConversionComplete());
}

TEST(ADC, IsConversionComplete_isComplete)
{
    adcReg_adcsra = 0;
    CHECK_EQUAL(1, ADC_IsConversionComplete());
}

TEST(ADC, SetFreeRunning_turnOn)
{
    adcReg_adcsra = 0;
    ADC_SetFreeRunning(1);
    CHECK_EQUAL((1 << 5), adcReg_adcsra);
}

TEST(ADC, SetFreeRunning_turnOff)
{
    adcReg_adcsra = (1 << 5);
    ADC_SetFreeRunning(0);
    CHECK_EQUAL(0, adcReg_adcsra);
}

TEST(ADC, GetInterruptFlag_isSet)
{
    adcReg_adcsra = (1 << 4);
    CHECK_EQUAL(1, ADC_GetInterruptFlag());
}

TEST(ADC, GetInterruptFlag_isNotSet)
{
    adcReg_adcsra = 0;
    CHECK_EQUAL(0, ADC_GetInterruptFlag());
}

TEST(ADC, SetInterruptEnable_turnOn)
{
    adcReg_adcsra = 0;
    ADC_SetInterruptEnable(1);
    CHECK_EQUAL((1 << 3), adcReg_adcsra);
}

TEST(ADC, SetInterruptEnable_turnOff)
{
    adcReg_adcsra = (1 << 3);
    ADC_SetInterruptEnable(0);
    CHECK_EQUAL(0, adcReg_adcsra);
}

TEST(ADC, SetPrescaler_to2)
{
    adcReg_adcsra = (1 << 2)|(1 << 1)|(1 << 0);
    ADC_SetPrescaler(2);
    CHECK_EQUAL(0, adcReg_adcsra);
}

TEST(ADC, SetPrescaler_to4)
{
    adcReg_adcsra = 0;
    ADC_SetPrescaler(4);
    CHECK_EQUAL(2, adcReg_adcsra);
}

TEST(ADC, SetPrescaler_to8)
{
    adcReg_adcsra = 0;
    ADC_SetPrescaler(8);
    CHECK_EQUAL(3, adcReg_adcsra);
}

TEST(ADC, SetPrescaler_to16)
{
    adcReg_adcsra = 0;
    ADC_SetPrescaler(16);
    CHECK_EQUAL(4, adcReg_adcsra);
}

TEST(ADC, SetPrescaler_to32)
{
    adcReg_adcsra = 0;
    ADC_SetPrescaler(32);
    CHECK_EQUAL(5, adcReg_adcsra);
}

TEST(ADC, SetPrescaler_to64)
{
    adcReg_adcsra = 0;
    ADC_SetPrescaler(64);
    CHECK_EQUAL(6, adcReg_adcsra);
}

TEST(ADC, SetPrescaler_to128)
{
    adcReg_adcsra = 0;
    ADC_SetPrescaler(128);
    CHECK_EQUAL(7, adcReg_adcsra);
}

TEST(ADC, SetPrescaler_noChangeOnOutOfBounds)
{
    adcReg_adcsra = 0;
    ADC_SetPrescaler(129);
    CHECK_EQUAL(0, adcReg_adcsra);
}

TEST(ADC, GetResult)
{
    adcReg_adch = 0x55;
    adcReg_adcl = 0xAA;
    CHECK_EQUAL(0x55AA, ADC_GetResult());
}

TEST(ADC, fp_IO_Read_initialized)
{
    POINTERS_EQUAL(IO_Read, adc_fp_IO_Read);
}

TEST(ADC, GetResult_readsLowBeforeHighReg)
{
    uint8_t (*saved_adc_fp_IO_Read)(const volatile uint8_t *);

    saved_adc_fp_IO_Read = adc_fp_IO_Read;
    adc_fp_IO_Read = IOSpy_Read;
    IOSpy_Create(2);
    ADC_GetResult();
    POINTERS_EQUAL(&adcReg_adcl, IOSpy_GetReadAddr());
    POINTERS_EQUAL(&adcReg_adch, IOSpy_GetReadAddr());
    IOSpy_Destroy();
    adc_fp_IO_Read = saved_adc_fp_IO_Read;
}
