extern "C"
{
#include "SPI_TrxSpy.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(SPI_TrxSpy)
{
    uint8_t nBytesToRecord;

    void setup()
    {
      nBytesToRecord = 5;
      SPI_TrxSpy_Create(nBytesToRecord);
    }

    void teardown()
    {
       SPI_TrxSpy_Destroy();
    }
};

TEST(SPI_TrxSpy, Create_byteWrittenIsZero)
{
    CHECK_EQUAL(0, SPI_TrxSpy_GetByteWritten());
}

TEST(SPI_TrxSpy, Create_lastByteWrittenIsZero)
{
    CHECK_EQUAL(0, SPI_TrxSpy_GetLastByteWritten());
}

TEST(SPI_TrxSpy, Create_bytesWrittenIsZero)
{
    CHECK_EQUAL(0, SPI_TrxSpy_GetBytesWrittenCount());
}

TEST(SPI_TrxSpy, Create_bytesWrittenReturnedisZero)
{
    CHECK_EQUAL(0, SPI_TrxSpy_GetBytesWrittenReturned());
}

TEST(SPI_TrxSpy, GetLastByteWritten)
{
    uint8_t byte;
    SPI_TrxSpy(0xAA);
    byte = SPI_TrxSpy_GetLastByteWritten();
    CHECK_EQUAL(0xAA, byte);
}

TEST(SPI_TrxSpy, incrementsBytesWrittenCount)
{
    const uint8_t nWritesExpected = nBytesToRecord;
    uint8_t i;
    for (i = 0; i < nWritesExpected; i++)
        SPI_TrxSpy(i);
    CHECK_EQUAL(nWritesExpected, SPI_TrxSpy_GetBytesWrittenCount());
}

TEST(SPI_TrxSpy, GetByteWritten)
{
    const uint8_t nWritesExpected = nBytesToRecord;
    uint8_t i;
    for (i = 0; i < nWritesExpected; i++)
        SPI_TrxSpy((uint8_t)(1 + i));

    CHECK_EQUAL(1, SPI_TrxSpy_GetByteWritten());
    CHECK_EQUAL(2, SPI_TrxSpy_GetByteWritten());
    CHECK_EQUAL(3, SPI_TrxSpy_GetByteWritten());
    CHECK_EQUAL(4, SPI_TrxSpy_GetByteWritten());
    CHECK_EQUAL(5, SPI_TrxSpy_GetByteWritten());
}

TEST(SPI_TrxSpy, GetByteWritten_incrementsReturnedCount)
{
    SPI_TrxSpy(0xAA);
    SPI_TrxSpy(0xFF);

    SPI_TrxSpy_GetByteWritten();
    CHECK_EQUAL(1, SPI_TrxSpy_GetBytesWrittenReturned());
    SPI_TrxSpy_GetByteWritten();
    CHECK_EQUAL(2, SPI_TrxSpy_GetBytesWrittenReturned());
}

TEST(SPI_TrxSpy, SetReturnByte)
{
    SPI_TrxSpy_SetReturnByte(0xAC);
    CHECK_EQUAL(0xAC, SPI_TrxSpy(0xFF));
}

TEST(SPI_TrxSpy, SetReturnByte_multiple)
{
    SPI_TrxSpy_SetReturnByte(0x01);
    SPI_TrxSpy_SetReturnByte(0x02);

    CHECK_EQUAL(0x01, SPI_TrxSpy(0xFF));
    CHECK_EQUAL(0x02, SPI_TrxSpy(0xFF));
}
