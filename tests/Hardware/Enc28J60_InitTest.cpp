extern "C"
{
#include "Enc28J60.h"
#include "Enc28J60_IO.h"
}

#include "CppUTest/TestHarness.h"
#include "CppUTest/TestPlugin.h"
#include "CppUTestExt/MockSupport.h"

/* Used by InitMac() for full- and half-duplex */

static void fakeEnc28J60_SetMacReceiveEnable(Enc28J60 self, const uint8_t enable)
{
    mock().actualCall("Enc28J60_SetMacReceiveEnable").withParameter("enable", enable);
}

static void fakeEnc28J60_SetPaddingMode(Enc28J60 self, const uint8_t mode)
{
    mock().actualCall("Enc28J60_SetPaddingMode").withParameter("mode", mode);
}

static void fakeEnc28J60_SetTransmitChecksumEnable(Enc28J60 self, const uint8_t enable)
{
    mock().actualCall("Enc28J60_SetTransmitChecksumEnable").withParameter("enable", enable);
}

static void fakeEnc28J60_SetDuplexMode(Enc28J60 self, const uint8_t full)
{
    mock().actualCall("Enc28J60_SetDuplexMode").withParameter("full", full);
}

static void fakeEnc28J60_SetFrameLengthCheckEnable(Enc28J60 self, const uint8_t enable)
{
    mock().actualCall("Enc28J60_SetFrameLengthCheckEnable").withParameter("enable", enable);
}

static void fakeEnc28J60_SetMaxFrameLen(Enc28J60 self, const uint16_t max)
{
    mock().actualCall("Enc28J60_SetMaxFrameLen").withParameter("max", max);
}

static void fakeEnc28J60_SetInterPacketGapDelayForBackToBack(Enc28J60 self, const uint8_t delay)
{
    mock().actualCall("Enc28J60_SetInterPacketGapDelayForBackToBack").withParameter("delay", delay);
}

static void fakeEnc28J60_SetInterPacketGapDelayForNonBackToBack(Enc28J60 self, const uint16_t delay)
{
    mock().actualCall("Enc28J60_SetInterPacketGapDelayForNonBackToBack").withParameter("delay", delay);
}

static void fakeEnc28J60_SetMacAddr(Enc28J60 self, const uint8_t *mac_addr)
{
    mock().actualCall("Enc28J60_SetMacAddr").withParameter("mac_addr", mac_addr);
}

/* Used by InitMac() for half-duplex */

static void fakeEnc28J60_SetMaxRetransmission(Enc28J60 self, const uint8_t retmax)
{
    mock().actualCall("Enc28J60_SetMaxRetransmission").withParameter("retmax", retmax);
}

static void fakeEnc28J60_SetCollisionWindow(Enc28J60 self, const uint8_t colwin)
{
    mock().actualCall("Enc28J60_SetCollisionWindow").withParameter("colwin", colwin);
}

static void fakeEnc28J60_SetDeferTransmissionEnable(Enc28J60 self, const uint8_t enable)
{
    mock().actualCall("Enc28J60_SetDeferTransmissionEnable").withParameter("enable", enable);
}

static void fakeEnc28J60_SetNoBackoffDuringBackpressureEnable(Enc28J60 self, const uint8_t enable)
{
    mock().actualCall("Enc28J60_SetNoBackoffDuringBackpressureEnable").withParameter("enable", enable);
}

static void fakeEnc28J60_SetNoBackoffEnable(Enc28J60 self, const uint8_t enable)
{
    mock().actualCall("Enc28J60_SetNoBackoffEnable").withParameter("enable", enable);
}

/* Used by Init() */

static void fakeEnc28J60_IO_Src(Enc28J60_IO self)
{
    mock().actualCall("Enc28J60_IO_Src");
}

static uint8_t fakeEnc28J60_IsClockReady(Enc28J60 self)
{
    mock().actualCall("Enc28J60_IsClockReady");
    return 1;
}

static void fakeEnc28J60_SetRxStart(Enc28J60 self, const uint16_t addr)
{
    mock().actualCall("Enc28J60_SetRxStart").withParameter("addr", addr);
}

static void fakeEnc28J60_SetRxEnd(Enc28J60 self, const uint16_t addr)
{
    mock().actualCall("Enc28J60_SetRxEnd").withParameter("addr", addr);
}

static void fakeEnc28J60_SetReceptionEnable(Enc28J60 self, const uint8_t enable)
{
    mock().actualCall("Enc28J60_SetReceptionEnable").withParameter("enable", enable);
}

TEST_GROUP(Enc28J60_Init)
{
	uint8_t csPin_offset;
	uint8_t csPin_mask;
	volatile uint8_t csPin_DDR;
	volatile uint8_t csPin_PORT;
	volatile uint8_t csPin_PIN;
	Pin csPin;
	Enc28J60 eth;

	void setup()
	{
		/* Used by InitMac() for full- and half-duplex */
		UT_PTR_SET(Enc28J60_SetMacReceiveEnable,	fakeEnc28J60_SetMacReceiveEnable);
		UT_PTR_SET(Enc28J60_SetPaddingMode,		fakeEnc28J60_SetPaddingMode);
		UT_PTR_SET(Enc28J60_SetTransmitChecksumEnable,	fakeEnc28J60_SetTransmitChecksumEnable);
		UT_PTR_SET(Enc28J60_SetDuplexMode,		fakeEnc28J60_SetDuplexMode);
		UT_PTR_SET(Enc28J60_SetFrameLengthCheckEnable,	fakeEnc28J60_SetFrameLengthCheckEnable);
		UT_PTR_SET(Enc28J60_SetMaxFrameLen,		fakeEnc28J60_SetMaxFrameLen);
		UT_PTR_SET(Enc28J60_SetInterPacketGapDelayForBackToBack,	fakeEnc28J60_SetInterPacketGapDelayForBackToBack);
		UT_PTR_SET(Enc28J60_SetInterPacketGapDelayForNonBackToBack,	fakeEnc28J60_SetInterPacketGapDelayForNonBackToBack);
		UT_PTR_SET(Enc28J60_SetMacAddr,			fakeEnc28J60_SetMacAddr);

		/* Used by InitMac() for half-duplex */
		UT_PTR_SET(Enc28J60_SetMaxRetransmission,	fakeEnc28J60_SetMaxRetransmission);
		UT_PTR_SET(Enc28J60_SetCollisionWindow,		fakeEnc28J60_SetCollisionWindow);
		UT_PTR_SET(Enc28J60_SetDeferTransmissionEnable,	fakeEnc28J60_SetDeferTransmissionEnable);
		UT_PTR_SET(Enc28J60_SetNoBackoffDuringBackpressureEnable,	fakeEnc28J60_SetNoBackoffDuringBackpressureEnable);
		UT_PTR_SET(Enc28J60_SetNoBackoffEnable,		fakeEnc28J60_SetNoBackoffEnable);

		/* Used by Init() */
		UT_PTR_SET(Enc28J60_IO_Src,			fakeEnc28J60_IO_Src);
		UT_PTR_SET(Enc28J60_IsClockReady,		fakeEnc28J60_IsClockReady);
		UT_PTR_SET(Enc28J60_SetRxStart,			fakeEnc28J60_SetRxStart);
		UT_PTR_SET(Enc28J60_SetRxEnd,			fakeEnc28J60_SetRxEnd);
		UT_PTR_SET(Enc28J60_SetReceptionEnable,		fakeEnc28J60_SetReceptionEnable);

		csPin_offset = 6;
		csPin_mask = (uint8_t)(1 << csPin_offset);
		csPin_DDR = csPin_PORT = csPin_PIN = 0;
		csPin = Pin_Create(csPin_offset, &csPin_DDR, &csPin_PORT, &csPin_PIN);
		eth = Enc28J60_Create(csPin);
	}

	void teardown()
	{
		Enc28J60_Destroy(eth);
		Pin_Destroy(csPin);
	}
};

TEST(Enc28J60_Init, InitMac_forFullDuplex)
{
    const uint8_t full_duplex = 1;
    const uint16_t maxFrameLen = 1518;
    const uint8_t ipg_delay_b2b = 0x15;
    const uint16_t ipg_delay_nb2b = 0x0012;
    const uint8_t macAddr[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06};

    mock().expectOneCall("Enc28J60_SetMacReceiveEnable").withParameter("enable", 1);		// 1.
    mock().expectOneCall("Enc28J60_SetPaddingMode").withParameter("mode", 7);			// 2.
    mock().expectOneCall("Enc28J60_SetTransmitChecksumEnable").withParameter("enable", 1);	// 2.
    mock().expectOneCall("Enc28J60_SetDuplexMode").withParameter("full", full_duplex);		// 2.
    mock().expectOneCall("Enc28J60_SetFrameLengthCheckEnable").withParameter("enable", 1);	// 2.
    mock().expectOneCall("Enc28J60_SetMaxFrameLen").withParameter("max", maxFrameLen);		// 4.
    mock().expectOneCall("Enc28J60_SetInterPacketGapDelayForBackToBack").withParameter("delay", ipg_delay_b2b);	// 5.
    mock().expectOneCall("Enc28J60_SetInterPacketGapDelayForNonBackToBack").withParameter("delay", ipg_delay_nb2b);	// 6./7.
    mock().expectOneCall("Enc28J60_SetMacAddr").withParameter("mac_addr", macAddr);		// 9.

    Enc28J60_InitMac(eth, full_duplex, macAddr, maxFrameLen);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60_Init, InitMac_forHalfDuplex)
{
    const uint8_t full_duplex = 0;
    const uint16_t maxFrameLen = 1518;
    const uint8_t ipg_delay_b2b = 0x12;
    const uint16_t ipg_delay_nb2b = 0x0C12;
    const uint8_t macAddr[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06};

    mock().expectOneCall("Enc28J60_SetMacReceiveEnable").withParameter("enable", 1);		// 1.
    mock().expectOneCall("Enc28J60_SetPaddingMode").withParameter("mode", 7);			// 2.
    mock().expectOneCall("Enc28J60_SetTransmitChecksumEnable").withParameter("enable", 1);	// 2.
    mock().expectOneCall("Enc28J60_SetDuplexMode").withParameter("full", full_duplex);		// 2.
    mock().expectOneCall("Enc28J60_SetFrameLengthCheckEnable").withParameter("enable", 1);	// 2.
    mock().expectOneCall("Enc28J60_SetDeferTransmissionEnable").withParameter("enable", 1);	// 3.
    mock().expectOneCall("Enc28J60_SetNoBackoffDuringBackpressureEnable").withParameter("enable", 1);	// 3.
    mock().expectOneCall("Enc28J60_SetNoBackoffEnable").withParameter("enable", 1);		// 3.
    mock().expectOneCall("Enc28J60_SetMaxFrameLen").withParameter("max", maxFrameLen);		// 4.
    mock().expectOneCall("Enc28J60_SetInterPacketGapDelayForBackToBack").withParameter("delay", ipg_delay_b2b);	// 5.
    mock().expectOneCall("Enc28J60_SetInterPacketGapDelayForNonBackToBack").withParameter("delay", ipg_delay_nb2b);	// 6./7.
    mock().expectOneCall("Enc28J60_SetMaxRetransmission").withParameter("retmax", 0x0F);	// 8.
    mock().expectOneCall("Enc28J60_SetCollisionWindow").withParameter("colwin", 0x37);		// 8.
    mock().expectOneCall("Enc28J60_SetMacAddr").withParameter("mac_addr", macAddr);		// 9.

    Enc28J60_InitMac(eth, full_duplex, macAddr, maxFrameLen);

    mock().checkExpectations();
    mock().clear();
}

static void fakeEnc28J60_InitMac(Enc28J60 self, const uint8_t full_duplex, const uint8_t *mac_addr, const uint16_t maxFrameLen)
{
    mock().actualCall("Enc28J60_InitMac").withParameter("full_duplex", full_duplex).withParameter("maxFrameLen", maxFrameLen);; // .withParameter("mac_addr", mac_addr);
}

TEST(Enc28J60_Init, Init)
{
    uint16_t maxFrameLen = 1518;
    uint16_t rxBufSize = (uint16_t)((maxFrameLen + 8) * 2);
    uint16_t rxStart = (uint16_t)(ENC28J60_RAM_END - rxBufSize);
    uint16_t rxEnd = ENC28J60_RAM_END;
    uint8_t full_duplex = 1;
    uint8_t mac_addr[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06};

    void (*fp_saved_Enc28J60_InitMac)(Enc28J60, const uint8_t, const uint8_t *, const uint16_t);

    fp_saved_Enc28J60_InitMac = Enc28J60_InitMac;

    Enc28J60_InitMac = fakeEnc28J60_InitMac;
    Enc28J60_SetReceptionEnable = fakeEnc28J60_SetReceptionEnable;

    mock().expectOneCall("Enc28J60_IO_Src");
    mock().expectOneCall("Enc28J60_IsClockReady"); /* TODO expect N calls */
    mock().expectOneCall("Enc28J60_SetRxStart").withParameter("addr", rxStart);
    mock().expectOneCall("Enc28J60_SetRxEnd").withParameter("addr", rxEnd);
    mock().expectOneCall("Enc28J60_InitMac").withParameter("full_duplex", full_duplex).withParameter("maxFrameLen", maxFrameLen); // .withParameter("mac_addr", mac_addr);
    // TODO InitPhy
    mock().expectOneCall("Enc28J60_SetReceptionEnable").withParameter("enable", 1);

    Enc28J60_Init(eth, rxBufSize, full_duplex, mac_addr, maxFrameLen);

    Enc28J60_InitMac = fp_saved_Enc28J60_InitMac;

    mock().checkExpectations();
    mock().clear();
}
