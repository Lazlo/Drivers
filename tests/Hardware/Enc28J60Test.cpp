extern "C"
{
#include "Enc28J60.h"
#include "Enc28J60_IO.h"
}

#include "CppUTest/TestHarness.h"
#include "CppUTest/TestPlugin.h"
#include "CppUTestExt/MockSupport.h"

static void FakeEnc28J60_IO_Bank(Enc28J60_IO self, const uint8_t bank)
{
    mock().actualCall("Enc28J60_IO_Bank").withParameter("bank", bank);
}

static uint8_t FakeEnc28J60_IO_Rcr(Enc28J60_IO self, const uint8_t addr)
{
    mock().actualCall("Enc28J60_IO_Rcr").withParameter("addr", addr);
    return (uint8_t)mock().getData("value").getIntValue();
}

static uint8_t FakeEnc28J60_IO_Rmr(Enc28J60_IO self, const uint8_t addr)
{
    mock().actualCall("Enc28J60_IO_Rmr").withParameter("addr", addr);
    return (uint8_t)mock().getData("value").getIntValue();
}

static uint16_t FakeEnc28J60_IO_Rpr(Enc28J60_IO self, const uint8_t addr)
{
    mock().actualCall("Enc28J60_IO_Rpr").withParameter("addr", addr);
    return (uint16_t)mock().getData("phyReg_value").getIntValue();
}

static void FakeEnc28J60_IO_Wcr(Enc28J60_IO self, const uint8_t addr, const uint8_t value)
{
    mock().actualCall("Enc28J60_IO_Wcr").withParameter("addr", addr).withParameter("value", value);
}

static void FakeEnc28J60_IO_Wpr(Enc28J60_IO self, const uint8_t addr, const uint16_t value)
{
    mock().actualCall("Enc28J60_IO_Wpr").withParameter("addr", addr).withParameter("value", value);
}

static void FakeEnc28J60_IO_Bfs(Enc28J60_IO self, const uint8_t addr, const uint8_t mask)
{
    mock().actualCall("Enc28J60_IO_Bfs").withParameter("addr", addr).withParameter("mask", mask);
}

static void FakeEnc28J60_IO_Bfc(Enc28J60_IO self, const uint8_t addr, const uint8_t mask)
{
    mock().actualCall("Enc28J60_IO_Bfc").withParameter("addr", addr).withParameter("mask", mask);
}

TEST_GROUP(Enc28J60)
{
    Enc28J60 eth;
    uint8_t csPin_offset;
    uint8_t csPin_mask;
    volatile uint8_t csPin_DDR;
    volatile uint8_t csPin_PORT;
    volatile uint8_t csPin_PIN;
    Pin csPin;

    void setup()
    {
        csPin_offset = 6;
        csPin_mask = (uint8_t)(1 << csPin_offset);
        csPin_DDR = csPin_PORT = csPin_PIN = 0;
        csPin = Pin_Create(csPin_offset, &csPin_DDR, &csPin_PORT, &csPin_PIN);

        UT_PTR_SET(Enc28J60_IO_Bank, FakeEnc28J60_IO_Bank);
        UT_PTR_SET(Enc28J60_IO_Rcr, FakeEnc28J60_IO_Rcr);
        UT_PTR_SET(Enc28J60_IO_Rmr, FakeEnc28J60_IO_Rmr);
        UT_PTR_SET(Enc28J60_IO_Rpr, FakeEnc28J60_IO_Rpr);
        UT_PTR_SET(Enc28J60_IO_Wcr, FakeEnc28J60_IO_Wcr);
        UT_PTR_SET(Enc28J60_IO_Wpr, FakeEnc28J60_IO_Wpr);
        UT_PTR_SET(Enc28J60_IO_Bfs, FakeEnc28J60_IO_Bfs);
        UT_PTR_SET(Enc28J60_IO_Bfc, FakeEnc28J60_IO_Bfc);

        eth = Enc28J60_Create(csPin);
    }

    void teardown()
    {
        Enc28J60_Destroy(eth);
        Pin_Destroy(csPin);
    }
};

/* Assertion Helpers ================================================*/

static void expectBitFieldOp(const uint8_t op, const uint8_t bank, const uint8_t addr, const uint8_t mask)
{
    const char *bfop_set = "Enc28J60_IO_Bfs";
    const char *bfop_clear = "Enc28J60_IO_Bfc";
    const char *bfop;

    if (op) /* set */
        bfop = bfop_set;
    else /* clear */
        bfop = bfop_clear;
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", bank);
    mock().expectOneCall(bfop).withParameter("addr", addr).withParameter("mask", mask);
}

static void expectBitFieldSet(const uint8_t bank, const uint8_t addr, const uint8_t mask)
{
    expectBitFieldOp(1, bank, addr, mask);
}

static void expectBitFieldClear(const uint8_t bank, const uint8_t addr, const uint8_t mask)
{
    expectBitFieldOp(0, bank, addr, mask);
}

/* Buffer Configuration **********************************************/

static void expectWriteBufferAddr(const uint8_t bank, const uint8_t base_addr,
				const uint16_t mask, const uint16_t value)
{
    uint8_t mask_l = (uint8_t)(mask & 0xFF);
    uint8_t mask_h = (uint8_t)((mask >> 8) & 0xFF);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", bank);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", base_addr).withParameter("value", value & mask_l);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", base_addr + 1).withParameter("value", (value >> 8) & mask_h);
}

static void expectReadBufferAddr(const uint8_t bank, const uint8_t base_addr,
				const uint8_t value)
{
    mock().setData("value", value);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", bank);
    mock().expectOneCall("Enc28J60_IO_Rcr").withParameter("addr", base_addr);
    mock().expectOneCall("Enc28J60_IO_Rcr").withParameter("addr", base_addr + 1);
}

TEST(Enc28J60, SetRxStartinBank0)
{
    const uint16_t rxStart = 0xFFFF;
    expectWriteBufferAddr(0, 0x08, 0x1FFF, rxStart);
    Enc28J60_SetRxStart(eth, rxStart);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetRxStart)
{
    uint16_t rxStart;
    expectReadBufferAddr(0, 0x08, 0x1F);
    rxStart = Enc28J60_GetRxStart(eth);
    mock().checkExpectations();
    mock().clear();
    CHECK_EQUAL(0x1F1F, rxStart);
}

TEST(Enc28J60, SetRxEndinBank0)
{
    const uint16_t rxEnd = 0xFFFF;
    expectWriteBufferAddr(0, 0x0A, 0x1FFF, rxEnd);
    Enc28J60_SetRxEnd(eth, rxEnd);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetRxEnd)
{
    uint16_t rxEnd;
    expectReadBufferAddr(0, 0x0A, 0x1E);
    rxEnd = Enc28J60_GetRxEnd(eth);
    mock().checkExpectations();
    mock().clear();
    CHECK_EQUAL(0x1E1E, rxEnd);
}

TEST(Enc28J60, SetRxReadPtrInBank0)
{
    const uint16_t rxReadPtr = 0xFFFF;
    expectWriteBufferAddr(0, 0x0C, 0x1FFF, rxReadPtr);
    Enc28J60_SetRxReadPtr(eth, rxReadPtr);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetRxReadPtr)
{
    uint16_t rxReadPtr;
    expectReadBufferAddr(0, 0x0C, 0x1A);
    rxReadPtr = Enc28J60_GetRxReadPtr(eth);
    mock().checkExpectations();
    mock().clear();
    CHECK_EQUAL(0x1A1A, rxReadPtr);
}

TEST(Enc28J60, SetTxStartInBank0)
{
    uint16_t badTxStart = 0xFFFF;
    expectWriteBufferAddr(0, 0x04, 0x1FFF, badTxStart);
    Enc28J60_SetTxStart(eth, badTxStart);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetTxStartFromBank0)
{
    uint16_t txStart;
    expectReadBufferAddr(0, 0x04, 0x15);
    txStart = Enc28J60_GetTxStart(eth);
    mock().checkExpectations();
    mock().clear();
    CHECK_EQUAL(0x1515, txStart);
}

TEST(Enc28J60, SetTxEndInBank0)
{
    uint16_t badTxEnd = 0xFFFF;
    expectWriteBufferAddr(0, 0x06, 0x1FFF, badTxEnd);
    Enc28J60_SetTxEnd(eth, badTxEnd);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetTxEndFromBank0)
{
    uint16_t rxEnd;
    expectReadBufferAddr(0, 0x06, 0x17);
    rxEnd = Enc28J60_GetTxEnd(eth);
    mock().checkExpectations();
    mock().clear();
    CHECK_EQUAL(0x1717, rxEnd);
}

TEST(Enc28J60, SetReadPtr)
{
    uint16_t badReadPtr = 0xFFFF;
    expectWriteBufferAddr(0, 0x00, 0x1FFF, badReadPtr);
    Enc28J60_SetReadPtr(eth, badReadPtr);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetReadPtr_readsERDPTLandERDPTHfromBank0)
{
    uint16_t readPtr;
    expectReadBufferAddr(0, 0x00, 0x13);
    readPtr = Enc28J60_GetReadPtr(eth);
    mock().checkExpectations();
    mock().clear();
    CHECK_EQUAL(0x1313, readPtr);
}

TEST(Enc28J60, SetWritePtr)
{
    uint16_t badWritePtr = 0xFFFF;
    expectWriteBufferAddr(0, 0x02, 0x1FFF, badWritePtr);
    Enc28J60_SetWritePtr(eth, badWritePtr);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetWritePtr_readsEWRPTLandEWRPTHfromBank0)
{
    uint16_t writePtr;
    expectReadBufferAddr(0, 0x02, 0x18);
    writePtr = Enc28J60_GetWritePtr(eth);
    mock().checkExpectations();
    mock().clear();
    CHECK_EQUAL(0x1818, writePtr);
}

/* Clock *************************************************************/

TEST(Enc28J60, IsClockReady_getBitInESTAT)
{
    mock().expectOneCall("Enc28J60_IO_Rcr").withParameter("addr", 0x1D);

    Enc28J60_IsClockReady(eth);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, IsClockReady_returnsZeroWhenBitIsNotSet)
{
    uint8_t clkrdy;

    mock().setData("value", 0xFE); /* fake ESTAT.CLKRDY bit state */
    mock().expectOneCall("Enc28J60_IO_Rcr").withParameter("addr", 0x1D);

    clkrdy = Enc28J60_IsClockReady(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(0, clkrdy);
}

TEST(Enc28J60, IsClockReady_returnOneWhenBitIsSet)
{
    uint8_t clkrdy;

    mock().setData("value", 0xFF); /* fake ESTAT.CLKRDY bit state */
    mock().expectOneCall("Enc28J60_IO_Rcr").withParameter("addr", 0x1D);

    clkrdy = Enc28J60_IsClockReady(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(1, clkrdy);
}

/* Ethernet Controller Configuration *********************************/

TEST(Enc28J60, SetReceptionEnable_setsReceptionEnableBitInECON1)
{
    mock().expectOneCall("Enc28J60_IO_Bfs").withParameter("addr", 0x1F).withParameter("mask", (1 << 2));

    Enc28J60_SetReceptionEnable(eth, 1);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetReceptionEnable_clearsReceptionEnableBitInECON1)
{
    mock().expectOneCall("Enc28J60_IO_Bfc").withParameter("addr", 0x1F).withParameter("mask", (1 << 2));

    Enc28J60_SetReceptionEnable(eth, 0);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetReceptionEnable)
{
    uint8_t enabled;

    mock().setData("value", (1 << 2));
    mock().expectOneCall("Enc28J60_IO_Rcr").withParameter("addr", 0x1F);

    enabled = Enc28J60_GetReceptionEnable(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(1, enabled);
}

TEST(Enc28J60, SetInterruptEnable_writesToEIE)
{
    uint8_t badIntEnableMask = 0xFF;

    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x1B).withParameter("value", badIntEnableMask & 0xFB);

    Enc28J60_SetInterruptEnable(eth, 0xFF);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetInterruptEnable_readsFromEIE)
{
    uint8_t enableMask;

    mock().setData("value", 0xFF);
    mock().expectOneCall("Enc28J60_IO_Rcr").withParameter("addr", 0x1B);

    enableMask = Enc28J60_GetInterruptEnable(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(0xFB, enableMask);
}

/* Ethernet Packet Filters ------------------------------------------*/

TEST(Enc28J60, SetBroadcastFilterEnable_setsBitInERXFCONinBank1)
{
    expectBitFieldSet(1, 0x18, (1 << 0));
    Enc28J60_SetBroadcastFilterEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetBroadcastFilterEnable_clearsBitInERXFCONinBank1)
{
    expectBitFieldClear(1, 0x18, (1 << 0));
    Enc28J60_SetBroadcastFilterEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetMulticastFilterEnable_setsBitInERXFCONinBank1)
{
    expectBitFieldSet(1, 0x18, (1 << 1));
    Enc28J60_SetMulticastFilterEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetMulticastFilterEnable_clearsBitInERXFCONinBank1)
{
    expectBitFieldClear(1, 0x18, (1 << 1));
    Enc28J60_SetMulticastFilterEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetHashTableFilterEnable_setsBitInERXFCONinBank1)
{
    expectBitFieldSet(1, 0x18, (1 << 2));
    Enc28J60_SetHashTableFilterEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetHashTableFilterEnable_clearsBitInERXFCONinBank1)
{
    expectBitFieldClear(1, 0x18, (1 << 2));
    Enc28J60_SetHashTableFilterEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetMagicPacketFilterEnable_setsBitInERXFCONinBank1)
{
    expectBitFieldSet(1, 0x18, (1 << 3));
    Enc28J60_SetMagicPacketFilterEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetMagicPacketFilterEnable_clearsBitInERXFCONinBank1)
{
    expectBitFieldClear(1, 0x18, (1 << 3));
    Enc28J60_SetMagicPacketFilterEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetPatternMatchFilterEnable_setsBitInERXFCONinBank1)
{
    expectBitFieldSet(1, 0x18, (1 << 4));
    Enc28J60_SetPatternMatchFilterEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetPatternMatchFilterEnable_clearsBitInERXFCONinBank1)
{
    expectBitFieldClear(1, 0x18, (1 << 4));
    Enc28J60_SetPatternMatchFilterEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetInvalidCrcFilterEnable_setsBitInERXFCONinBank1)
{
    expectBitFieldSet(1, 0x18, (1 << 5));
    Enc28J60_SetInvalidCrcFilterEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetInvalidCrcFilterEnable_clearsBitInERXFCONinBank1)
{
    expectBitFieldClear(1, 0x18, (1 << 5));
    Enc28J60_SetInvalidCrcFilterEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetAndOrFilterEnable_setsBitInERXFCONinBank1)
{
    expectBitFieldSet(1, 0x18, (1 << 6));
    Enc28J60_SetAndOrFilterEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetAndOrFilterEnable_clearsBitInERXFCONinBank1)
{
    expectBitFieldClear(1, 0x18, (1 << 6));
    Enc28J60_SetAndOrFilterEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetUnicastFilterEnable_setsBitInERXFCONinBank1)
{
    expectBitFieldSet(1, 0x18, (1 << 7));
    Enc28J60_SetUnicastFilterEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetUnicastFilterEnable_clearsBitInERXFCONinBank1)
{
    expectBitFieldClear(1, 0x18, (1 << 7));
    Enc28J60_SetUnicastFilterEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

/* Ethernet Controller Status ****************************************/

TEST(Enc28J60, GetInterruptFlags_readsEIR)
{
    uint8_t flags;

    mock().setData("value", 0xFF);
    mock().expectOneCall("Enc28J60_IO_Rcr").withParameter("addr", 0x1C);

    flags = Enc28J60_GetInterruptFlags(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(0x7B, flags);
}

TEST(Enc28J60, GetPacketCount_readsEPKTCNTinBank1)
{
    uint8_t pkt_count;

    mock().setData("value", 0xAA);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 1);
    mock().expectOneCall("Enc28J60_IO_Rcr").withParameter("addr", 0x19);

    pkt_count = Enc28J60_GetPacketCount(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(0xAA, pkt_count);
}

/* MAC Configuration *************************************************/

TEST(Enc28J60, SetMacReceiveEnable_setsReceiveEnableBitInMACON1inBank2)
{
    expectBitFieldSet(2, 0x00, (1 << 0));
    Enc28J60_SetMacReceiveEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetMacReceiveEnable_clearsReceiveEnableBitInMACON1inBank2)
{
    expectBitFieldClear(2, 0x00, (1 << 0));
    Enc28J60_SetMacReceiveEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetMacReceiveEnable)
{
    uint8_t enabled;

    mock().setData("value", (1 << 0));
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x00);

    enabled = Enc28J60_GetMacReceptionEnable(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(1, enabled);
}

TEST(Enc28J60, SetEnableTransmitChecksum_setsTXCRCEnableBitInMACON3inBank2)
{
    expectBitFieldSet(2, 0x02, (1 << 4));
    Enc28J60_SetTransmitChecksumEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetEnableTransmitChecksum_clearsTXCRCEnableBitInMACON3inBank2)
{
    expectBitFieldClear(2, 0x02, (1 << 4));
    Enc28J60_SetTransmitChecksumEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetEnableTransmitChecksum)
{
    uint8_t enabled;

    mock().setData("value", (1 << 4));
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x02);

    enabled = Enc28J60_GetTransmitChecksumEnable(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(1, enabled);
}

TEST(Enc28J60, SetDuplexMode_setsFullDuplexBitInMACON3inBank2AndInPHCON1)
{
    mock().setData("phyReg_value", 0xFEFF);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Bfs").withParameter("addr", 0x00).withParameter("mask", (1 << 3)|(1 << 2));
    mock().expectOneCall("Enc28J60_IO_Bfs").withParameter("addr", 0x02).withParameter("mask", (1 << 0));
    mock().expectOneCall("Enc28J60_IO_Rpr").withParameter("addr", 0);
    mock().expectOneCall("Enc28J60_IO_Wpr").withParameter("addr", 0).withParameter("value", (0xFEFF|(1 << 8)));

    Enc28J60_SetDuplexMode(eth, 1);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetDuplexMode_clearsFullDuplexBitInMACON3inBank2AndInPHCON1)
{
    mock().setData("phyReg_value", 0xFFFF);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Bfc").withParameter("addr", 0x00).withParameter("mask", (1 << 3)|(1 << 2));
    mock().expectOneCall("Enc28J60_IO_Bfc").withParameter("addr", 0x02).withParameter("mask", (1 << 0));
    mock().expectOneCall("Enc28J60_IO_Rpr").withParameter("addr", 0);
    mock().expectOneCall("Enc28J60_IO_Wpr").withParameter("addr", 0).withParameter("value", 0xFEFF);

    Enc28J60_SetDuplexMode(eth, 0);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetDuplexMode_returnsZeroForHalfDuplex)
{
    uint8_t dpxmode;

    mock().setData("value", 0);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x02);

    dpxmode = Enc28J60_GetDuplexMode(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(0, dpxmode);
}

TEST(Enc28J60, GetDuplexMode_returnsOneForFullDuplex)
{
    uint8_t dpxmode;

    mock().setData("value", 1);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x02);

    dpxmode = Enc28J60_GetDuplexMode(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(1, dpxmode);
}

TEST(Enc28J60, SetPaddingMode_setsBitsInMACON3inBank2)
{
    const uint8_t pad_mask = (1 << 7)|(1 << 6)|(1 << 5);

    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Bfc").withParameter("addr", 0x02).withParameter("mask", pad_mask);
    mock().expectOneCall("Enc28J60_IO_Bfs").withParameter("addr", 0x02).withParameter("mask", pad_mask);

    Enc28J60_SetPaddingMode(eth, 7);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetPaddingMode_readsBitsInMACON3inBank2)
{
    const uint8_t bad_pad_mode_value = 0xFF;
    uint8_t pad_mode;

    mock().setData("value", bad_pad_mode_value);

    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x02);

    pad_mode = Enc28J60_GetPaddingMode(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(7, pad_mode);
}

TEST(Enc28J60, SetFrameLengthCheckEnable_setsBitInMACON3inBank2)
{
    expectBitFieldSet(2, 0x02, (1 << 1));
    Enc28J60_SetFrameLengthCheckEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetFrameLengthCheckEnable_clearsBitInMACON3inBank2)
{
    expectBitFieldClear(2, 0x02, (1 << 1));
    Enc28J60_SetFrameLengthCheckEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetFrameLengthCheckEnable_readsBitInMACON3inBank2)
{
    uint8_t enabled;

    mock().setData("value", (1 << 1));
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x02);

    enabled = Enc28J60_GetFrameLengthCheckEnable(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(1, enabled);
}

TEST(Enc28J60, SetDeferTransmissionEnable_writesBitInMACON4inBank2)
{
    expectBitFieldSet(2, 0x03, (1 << 6));
    Enc28J60_SetDeferTransmissionEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetDeferTransmissionEnable_clearsBitInMACON4inBank2)
{
    expectBitFieldClear(2, 0x03, (1 << 6));
    Enc28J60_SetDeferTransmissionEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetDeferTransmissionEnable_readsBitFromMACON4inBank2)
{
    uint8_t defer;

    mock().setData("value", (1 << 6));
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x03);

    defer = Enc28J60_GetDeferTransmissionEnable(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(1, defer);
}

TEST(Enc28J60, SetNoBackoffDuringBackpressureEnable_writesBitInMACON4inBank2)
{
    expectBitFieldSet(2, 0x03, (1 << 5));
    Enc28J60_SetNoBackoffDuringBackpressureEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetNoBackoffDuringBackpressureEnable_clearsBitInMACON4inBank2)
{
    expectBitFieldClear(2, 0x03, (1 << 5));
    Enc28J60_SetNoBackoffDuringBackpressureEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetNoBackoffDuringBackpressureEnable_readsBitFromMACON4inBank2)
{
    uint8_t bpen;

    mock().setData("value", (1 << 5));
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x03);

    bpen = Enc28J60_GetNoBackoffDuringBackpressureEnable(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(1, bpen);
}

TEST(Enc28J60, SetNoBackoffEnable_writesBitInMACON4inBank2)
{
    expectBitFieldSet(2, 0x03, (1 << 4));
    Enc28J60_SetNoBackoffEnable(eth, 1);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, SetNoBackoffEnable_clearsBitInMACON4inBank2)
{
    expectBitFieldClear(2, 0x03, (1 << 4));
    Enc28J60_SetNoBackoffEnable(eth, 0);
    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetNoBackoffEnable_readsBitFromMACON4inBank2)
{
    uint8_t nobkoff;

    mock().setData("value", (1 << 4));
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x03);

    nobkoff = Enc28J60_GetNoBackoffEnable(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(1, nobkoff);
}

TEST(Enc28J60, SetMaxFrameLen_writesMAMXFLLandMAMXFLHinBank2)
{
    const uint16_t maxFrameLen = 1508;
    uint8_t mamxfll = maxFrameLen & 0xFF;
    uint8_t mamxflh = (maxFrameLen >> 8) & 0xFF;

    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x0A).withParameter("value", mamxfll);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x0B).withParameter("value", mamxflh);

    Enc28J60_SetMaxFrameLen(eth, maxFrameLen);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetMaxFrameLen_readsMAMXFLLandMAMXFLHinBank2)
{
    uint16_t maxFrameLen;

    mock().setData("value", 0xAA);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x0A);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x0B);

    maxFrameLen = Enc28J60_GetMaxFrameLen(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(0xAAAA, maxFrameLen);
}

TEST(Enc28J60, SetInterPacketGapDelayForBackToBack)
{
    const uint8_t ipg_bad_value = 0xFF;
    const uint8_t ipg_mask = 0x7F;

    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x04).withParameter("value", ipg_bad_value & ipg_mask);

    Enc28J60_SetInterPacketGapDelayForBackToBack(eth, ipg_bad_value);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetInterPacketGapDelayForBackToBack)
{
    uint8_t ipg;

    mock().setData("value", 127);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x04);

    ipg = Enc28J60_GetInterPacketGapDelayForBackToBack(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(127, ipg);
}

TEST(Enc28J60, SetInterPacketGapDelayForNonBackToBack)
{
    const uint16_t ipg_bad_value = 0xFFFF;

    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x06).withParameter("value", ipg_bad_value & 0xFF);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x07).withParameter("value", ipg_bad_value & 0xFF);

    Enc28J60_SetInterPacketGapDelayForNonBackToBack(eth, ipg_bad_value);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetInterPacketGapDelayForNonBackToBack)
{
    uint16_t ipg;

    mock().setData("value", 0x7F7F);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x06);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x07);

    ipg = Enc28J60_GetInterPacketGapDelayForNonBackToBack(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(0x7F7F, ipg);
}

TEST(Enc28J60, SetMaxRetransmission_writesToMACLCON1inBank2)
{
    const uint8_t badMaxRetransmissionValue = 0xFF;

    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x08).withParameter("value", 0xF);

    Enc28J60_SetMaxRetransmission(eth, badMaxRetransmissionValue);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetMaxRetransmission_readsFromMACLCON1inBank2)
{
    const uint8_t badMaxRetransmissionValue = 0xFF;
    uint8_t retmax;

    mock().setData("value", badMaxRetransmissionValue);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x08);

    retmax = Enc28J60_GetMaxRetransmission(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(0x0F, retmax);
}

TEST(Enc28J60, SetCollisionWindow_writesToMACLCON2inBank2)
{
    const uint8_t badCollisionWindowValue = 0xFF;

    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x09).withParameter("value", 0x3F);

    Enc28J60_SetCollisionWindow(eth, badCollisionWindowValue);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetCollisionWindow_readsFromMACLCON2inBank2)
{
    const uint8_t badCollisionWindowValue = 0xFF;
    uint8_t colwin;

    mock().setData("value", badCollisionWindowValue);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x09);

    colwin = Enc28J60_GetCollisionWindow(eth);

    mock().checkExpectations();
    mock().clear();

    CHECK_EQUAL(0x3F, colwin);
}

TEST(Enc28J60, SetMacAddr_writesMAADR1toMAADR6inBank3)
{
    const uint8_t mac_addr[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06};

    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 3);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x04).withParameter("value", 0x01);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x05).withParameter("value", 0x02);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x02).withParameter("value", 0x03);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x03).withParameter("value", 0x04);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x00).withParameter("value", 0x05);
    mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x01).withParameter("value", 0x06);

    Enc28J60_SetMacAddr(eth, mac_addr);

    mock().checkExpectations();
    mock().clear();
}

TEST(Enc28J60, GetMacAddr_readsMAADR1toMAADR6inBank3)
{
    const uint8_t mac_addr_expected[] = {0x55, 0x55, 0x55, 0x55, 0x55, 0x55};
    uint8_t mac_addr[6];

    mock().setData("value", 0x55);
    mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 3);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x04);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x05);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x02);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x03);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x00);
    mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x01);

    Enc28J60_GetMacAddr(eth, mac_addr);

    mock().checkExpectations();
    mock().clear();

    MEMCMP_EQUAL(mac_addr_expected, mac_addr, 6);
}

/* Ethernet Frame Transmission ***************************************/

static void fakeEnc28J60_SetTxStart(Enc28J60 self, const uint16_t addr)
{
    mock().actualCall("Enc28J60_SetTxStart").withParameter("addr", addr);
}

static void fakeEnc28J60_SetTxEnd(Enc28J60 self, const uint16_t addr)
{
    mock().actualCall("Enc28J60_SetTxEnd").withParameter("addr", addr);
}

static void fakeEnc28J60_IO_Wbm(Enc28J60_IO self, const uint16_t addr, const uint8_t *buf, const uint16_t len)
{
    mock().actualCall("Enc28J60_IO_Wbm").withParameter("addr", addr).withParameter("len", len); /* TODO pass buf as well */
}

TEST(Enc28J60, Transmit)
{
    const uint16_t len = 1518;
    uint8_t frame[len];

    uint16_t rxStart = 0;
    uint16_t rxEnd = 1 + len; /* + 1 because we write the per-packet control byte in front of the package */

    void (*fp_saved_Enc28J60_SetTxStart)(Enc28J60, const uint16_t);
    void (*fp_saved_Enc28J60_SetTxEnd)(Enc28J60, const uint16_t);
    void (*fp_saved_Enc28J60_IO_Wbm)(Enc28J60_IO, const uint16_t, const uint8_t *, const uint16_t);

    fp_saved_Enc28J60_SetTxStart = Enc28J60_SetTxStart;
    fp_saved_Enc28J60_SetTxEnd = Enc28J60_SetTxEnd;
    fp_saved_Enc28J60_IO_Wbm = Enc28J60_IO_Wbm;

    Enc28J60_SetTxStart = fakeEnc28J60_SetTxStart;
    Enc28J60_SetTxEnd = fakeEnc28J60_SetTxEnd;
    Enc28J60_IO_Wbm = fakeEnc28J60_IO_Wbm;

    mock().expectOneCall("Enc28J60_SetTxStart").withParameter("addr", rxStart);
    mock().expectOneCall("Enc28J60_IO_Wbm").withParameter("addr", rxStart).withParameter("len", len); /* TODO pass buf */
    mock().expectOneCall("Enc28J60_SetTxEnd").withParameter("addr", rxEnd);
    mock().expectOneCall("Enc28J60_IO_Bfc").withParameter("addr", 0x1C).withParameter("mask", (1 << 3)); /* clear EIR.TXIF */
    mock().expectOneCall("Enc28J60_IO_Bfs").withParameter("addr", 0x1B).withParameter("mask", (1 << 7)|(1 << 3)); /* set EIE.INTIE and EIE.TXIE */
    mock().expectOneCall("Enc28J60_IO_Bfs").withParameter("addr", 0x1F).withParameter("mask", (1 << 3)); /* set ECON1.TXRTS */

    Enc28J60_Transmit(eth, frame, len);

    Enc28J60_SetTxStart = fp_saved_Enc28J60_SetTxStart;
    Enc28J60_SetTxEnd = fp_saved_Enc28J60_SetTxEnd;
    Enc28J60_IO_Wbm = fp_saved_Enc28J60_IO_Wbm;

    mock().checkExpectations();
    mock().clear();
}
