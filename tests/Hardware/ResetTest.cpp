extern "C"
{
#include "Reset.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(Reset)
{
    uint8_t mcucr;

    void setup()
    {
    }

    void teardown()
    {
    }
};

TEST(Reset, Create_clearsRegister)
{
    mcucr = 0xFF;
    Reset_Create(&mcucr);
    CHECK_EQUAL(0, mcucr);
    Reset_Destroy();
}
TEST(Reset, WasCausedByWatchdog_isSet)
{
    mcucr = (1 << 3);
    Reset_Create(&mcucr);
    CHECK_EQUAL(1, Reset_WasCausedByWatchdog());
    Reset_Destroy();
}

TEST(Reset, WasCausedByWatchdog_isNotSet)
{
    mcucr = 0;
    Reset_Create(&mcucr);
    CHECK_EQUAL(0, Reset_WasCausedByWatchdog());
    Reset_Destroy();
}

TEST(Reset, WasCausedByBrownOut_isSet)
{
    mcucr = (1 << 2);
    Reset_Create(&mcucr);
    CHECK_EQUAL(1, Reset_WasCausedByBrownOut());
    Reset_Destroy();
}

TEST(Reset, WasCausedByBrownOut_isNotSet)
{
    mcucr = 0;
    Reset_Create(&mcucr);
    CHECK_EQUAL(0, Reset_WasCausedByBrownOut());
    Reset_Destroy();
}

TEST(Reset, WasCausedByExternalReset_isSet)
{
    mcucr = (1 << 1);
    Reset_Create(&mcucr);
    CHECK_EQUAL(1, Reset_WasCausedByExternalReset());
    Reset_Destroy();
}

TEST(Reset, WasCausedByExtrenalReset_isNotSet)
{
    mcucr = 0;
    Reset_Create(&mcucr);
    CHECK_EQUAL(0, Reset_WasCausedByExternalReset());
    Reset_Destroy();
}

TEST(Reset, WasCausedByPowerOnReset_isSet)
{
    mcucr = (1 << 0);
    Reset_Create(&mcucr);
    CHECK_EQUAL(1, Reset_WasCausedByPowerOnReset());
    Reset_Destroy();
}

TEST(Reset, WasCausedByPowerOnReset_isNotSet)
{
    mcucr = 0;
    Reset_Create(&mcucr);
    CHECK_EQUAL(0, Reset_WasCausedByPowerOnReset());
    Reset_Destroy();
}
