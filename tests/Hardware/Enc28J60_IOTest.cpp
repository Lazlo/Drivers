extern "C"
{
#include "Enc28J60_IO.h"
#include "Enc28J60_IOInternal.h"
#include "Pin.h"
#include "SPI.h"
#include "SPI_TrxSpy.h"
}

#include "CppUTest/TestHarness.h"
#include "CppUTest/TestPlugin.h"
#include "CppUTestExt/MockSupport.h"

static const uint8_t opcode_mask = (1 << 7)|(1 << 6)|(1 << 5);
static const uint8_t opcode_offset = 5;

TEST_GROUP(Enc28J60_IO)
{
	Enc28J60_IO io;
	uint8_t csPin_offset;
	uint8_t csPin_mask;
	volatile uint8_t csPin_DDR;
	volatile uint8_t csPin_PORT;
	volatile uint8_t csPin_PIN;
	Pin csPin;

	void setup()
	{
		csPin_offset = 7;
		csPin_mask = (uint8_t)(1 << csPin_offset);
		csPin_DDR = csPin_PORT = csPin_PIN = 0;
		csPin = Pin_Create(csPin_offset, &csPin_DDR, &csPin_PORT, &csPin_PIN);

		SPI_TrxSpy_Create(12);
		UT_PTR_SET(SPI_Trx, SPI_TrxSpy);

		io = Enc28J60_IO_Create(csPin);
	}

	void teardown()
	{
		Enc28J60_IO_Destroy(io);
		SPI_TrxSpy_Destroy();
		Pin_Destroy(csPin);
	}
};

/* Used to verify CS pin is pulled low before transfer and raised after transfer */
static void FakePin_Write(Pin self, const uint8_t level)
{
	mock().actualCall("Pin_Write").withParameter("pin", self).withParameter("level", level);
}

/* Used to verify CS pin is operated in the correct way */
static uint8_t FakeSPI_Trx(const uint8_t byte)
{
	mock().actualCall("SPI_Trx");
	return 0;
}

/* Create ************************************************************/

TEST(Enc28J60_IO, Create_setChipSelectPinDirectionToOutput)
{
	CHECK_EQUAL(csPin_mask, (csPin_DDR & csPin_mask));
}

TEST(Enc28J60_IO, Create_writeChipSelectPinHigh)
{
	CHECK_EQUAL(csPin_mask, (csPin_PORT & csPin_mask));
}

/* Select ************************************************************/

TEST(Enc28J60_IO, Select_setsCsPinLow)
{
	Enc28J60_IO_Select(io, 1);
	CHECK_EQUAL(0, (csPin_PORT & csPin_mask));
}

TEST(Enc28J60_IO, Select_setsCsPinHigh)
{
	Enc28J60_IO_Select(io, 0);
	CHECK_EQUAL(csPin_mask, (csPin_PORT & csPin_mask));
}

/* Read Control Register *********************************************/

TEST(Enc28J60_IO, Rcr_opCode_isZero)
{
	Enc28J60_IO_Rcr(io, 0xFF);
	CHECK_EQUAL(0, SPI_TrxSpy_GetByteWritten() & opcode_mask);
}

TEST(Enc28J60_IO, Rcr_masksRegisterAddress)
{
	uint8_t badRegisterAddress = 0xFF;
	uint8_t expectByteWritten = ADDR_MASK;

	Enc28J60_IO_Rcr(io, badRegisterAddress);
	CHECK_EQUAL(expectByteWritten, SPI_TrxSpy_GetByteWritten());
}

TEST(Enc28J60_IO, Rcr_returnsByteReceived)
{
	uint8_t rx;
	SPI_TrxSpy_SetReturnByte(0x00); /* the first byte shifted in while we shift the command out - should be ignored */
	SPI_TrxSpy_SetReturnByte(0xFA);
	rx = Enc28J60_IO_Rcr(io, 0x1F);
	CHECK_EQUAL(0xFA, rx);
}

TEST(Enc28J60_IO, Rcr_chipSelect)
{
	void (*fp_saved_Pin_Write)(Pin, const uint8_t) = Pin_Write;

	Pin_Write = FakePin_Write;
	SPI_Trx = FakeSPI_Trx;

	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 0);
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 1);

	Enc28J60_IO_Rcr(io, 0x1A);

	Pin_Write = fp_saved_Pin_Write;

	mock().checkExpectations();
	mock().clear();
}

/* Read MAC/MII Register *********************************************/

TEST(Enc28J60_IO, Rmr_opCode_isZero)
{
	Enc28J60_IO_Rmr(io, 0x20);
	CHECK_EQUAL(0, SPI_TrxSpy_GetByteWritten() & opcode_mask);
}

TEST(Enc28J60_IO, Rmr_masksRegisterAddress)
{
	uint8_t badRegisterAddress = 0xFF;
	uint8_t expectedByteWritten = ADDR_MASK;

	Enc28J60_IO_Rmr(io, badRegisterAddress);
	CHECK_EQUAL(expectedByteWritten, SPI_TrxSpy_GetByteWritten());
}

TEST(Enc28J60_IO, Rmr_writesThreeBytes)
{
	Enc28J60_IO_Rmr(io, 0x19);
	CHECK_EQUAL(3, SPI_TrxSpy_GetBytesWrittenCount());
}

TEST(Enc28J60_IO, Rmr_writesTwoDummyBytes)
{
	uint8_t byte2 = 0xFF;
	uint8_t byte3 = 0xFF;
	Enc28J60_IO_Rmr(io, 0x17);
	SPI_TrxSpy_GetByteWritten();
	byte2 = SPI_TrxSpy_GetByteWritten();
	byte3 = SPI_TrxSpy_GetByteWritten();
	CHECK_EQUAL(0, byte2);
	CHECK_EQUAL(0, byte3);
}

TEST(Enc28J60_IO, Rmr_returnsThirdByteReceived)
{
	SPI_TrxSpy_SetReturnByte(51);
	SPI_TrxSpy_SetReturnByte(52);
	SPI_TrxSpy_SetReturnByte(53);;
	CHECK_EQUAL(53, Enc28J60_IO_Rmr(io, 0x1A));
}

TEST(Enc28J60_IO, Rmr_chipSelect)
{
	void (*fp_saved_Pin_Write)(Pin, const uint8_t) = Pin_Write;

	Pin_Write = FakePin_Write;
	SPI_Trx = FakeSPI_Trx;

	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 0);
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 1);

	Enc28J60_IO_Rmr(io, 0x11);

	Pin_Write = fp_saved_Pin_Write;

	mock().checkExpectations();
	mock().clear();
}

/* Read PHY Register *************************************************/

static void FakeEnc28J60_IO_Wcr(Enc28J60_IO self, const uint8_t addr, const uint8_t value)
{
	mock().actualCall("Enc28J60_IO_Wcr").withParameter("addr", addr).withParameter("value", value);
}

static void FakeEnc28J60_IO_Bfs(Enc28J60_IO, const uint8_t, const uint8_t); /* defined next to tests for IO_Bank() */
static void FakeEnc28J60_IO_Bfc(Enc28J60_IO, const uint8_t, const uint8_t); /* defined next to tests for IO_Bank() */

static uint8_t FakeEnc28J60_IO_Rmr(Enc28J60_IO self, const uint8_t addr)
{
	mock().actualCall("Enc28J60_IO_Rmr").withParameter("addr", addr);
	/* TODO return (uint8_t)mock().getData("value").getIntValue(); */
	return 219;
}

static void FakeEnc28J60_IO_Bank(Enc28J60_IO self, const uint8_t bank)
{
	mock().actualCall("Enc28J60_IO_Bank").withParameter("bank", bank);
}

TEST(Enc28J60_IO, Rpr)
{
	void (*fp_saved_Enc28J60_IO_Wcr)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Wcr;
	uint8_t (*fp_saved_Enc28J60_IO_Rmr)(Enc28J60_IO, const uint8_t) = Enc28J60_IO_Rmr;
	void (*fp_saved_Enc28J60_IO_Bfs)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Bfs;
	void (*fp_saved_Enc28J60_IO_Bfc)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Bfc;
	void (*fp_saved_Enc28J60_IO_Bank)(Enc28J60_IO, const uint8_t) = Enc28J60_IO_Bank;

	const uint8_t phy_regAddr = 0xFF;

	Enc28J60_IO_Wcr = FakeEnc28J60_IO_Wcr;
	Enc28J60_IO_Rmr = FakeEnc28J60_IO_Rmr;
	Enc28J60_IO_Bfs = FakeEnc28J60_IO_Bfs;
	Enc28J60_IO_Bfc = FakeEnc28J60_IO_Bfc;
	Enc28J60_IO_Bank = FakeEnc28J60_IO_Bank;

	mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
	mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x14).withParameter("value", phy_regAddr & 0x1F);
	mock().expectOneCall("Enc28J60_IO_Bfs").withParameter("addr", 0x12).withParameter("mask", (1 << 0));
	mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 3);
	mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x0A); /* TODO check number of calls and return value */
	mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
	mock().expectOneCall("Enc28J60_IO_Bfc").withParameter("addr", 0x12).withParameter("mask", (1 << 0));
	mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x18);
	mock().expectOneCall("Enc28J60_IO_Rmr").withParameter("addr", 0x19);

	Enc28J60_IO_Rpr(io, phy_regAddr);

	Enc28J60_IO_Wcr = fp_saved_Enc28J60_IO_Wcr;
	Enc28J60_IO_Rmr = fp_saved_Enc28J60_IO_Rmr;
	Enc28J60_IO_Bfs = fp_saved_Enc28J60_IO_Bfs;
	Enc28J60_IO_Bfc = fp_saved_Enc28J60_IO_Bfc;
	Enc28J60_IO_Bank = fp_saved_Enc28J60_IO_Bank;

	mock().checkExpectations();
	mock().clear();
}

/* Read Buffer Memory ************************************************/

TEST(Enc28J60_IO, Rbm_opCode_isOne)
{
	Enc28J60_IO_Rbm(io, 0xFF, 0, 0);
	CHECK_EQUAL(1, ((SPI_TrxSpy_GetByteWritten() & opcode_mask) >> opcode_offset));
}

TEST(Enc28J60_IO, Rbm_firstByteArgumentIs26)
{
	Enc28J60_IO_Rbm(io, 0x11, 0, 0);
	CHECK_EQUAL(BUFFER_MEMORY_ARG, (SPI_TrxSpy_GetByteWritten() & ADDR_MASK));
}

TEST(Enc28J60_IO, Rbm_writesTwoBytes)
{
	Enc28J60_IO_Rbm(io, 0x23, 0, 0);
	CHECK_EQUAL(2, SPI_TrxSpy_GetBytesWrittenCount());
}

TEST(Enc28J60_IO, Rbm_sendsAddressByte)
{
	Enc28J60_IO_Rbm(io, 0x33, 0, 0);
	CHECK_EQUAL(0x33, SPI_TrxSpy_GetLastByteWritten());
}

/* TODO Test data returned by RBM */

TEST(Enc28J60_IO, Rbm_chipSelect)
{
	void (*fp_saved_Pin_Write)(Pin, const uint8_t) = Pin_Write;

	Pin_Write = FakePin_Write;
	SPI_Trx = FakeSPI_Trx;

	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 0);
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 1);

	Enc28J60_IO_Rbm(io, 0x10, 0, 0);

	Pin_Write = fp_saved_Pin_Write;

	mock().checkExpectations();
	mock().clear();
}

TEST(Enc28J60_IO, Rbm_receivesLenBytes)
{
	const uint8_t buf_len = 10;
	uint8_t buf[10];

	SPI_Trx = FakeSPI_Trx;

	/* TODO set data to be returned by FakeSPI_Trx() */
	mock().expectNCalls(2, "SPI_Trx");
	mock().expectNCalls(buf_len, "SPI_Trx"); // .withParameter("byte", 0);

	Enc28J60_IO_Rbm(io, 0x11, buf, buf_len);

	mock().checkExpectations();
	mock().clear();
}

/* Write Control Register ********************************************/

TEST(Enc28J60_IO, Wcr_opCode_isTwo)
{
	uint8_t cmd;
	uint8_t opcode;
	Enc28J60_IO_Wcr(io, 0x1F, (1 << 2));
	cmd = SPI_TrxSpy_GetByteWritten();
	opcode = (cmd & opcode_mask) >> opcode_offset;
	CHECK_EQUAL(2, opcode);
}

TEST(Enc28J60_IO, Wcr_masksRegisterAddress)
{
	uint8_t badRegisterAddress = 0xFF;
	uint8_t expectedAddress = ADDR_MASK;
	uint8_t actualAddress;

	Enc28J60_IO_Wcr(io, badRegisterAddress, 0x55);
	actualAddress = SPI_TrxSpy_GetByteWritten() & ADDR_MASK;
	CHECK_EQUAL(expectedAddress, actualAddress);
}

TEST(Enc28J60_IO, Wcr_writesTwoBytes)
{
	Enc28J60_IO_Wcr(io, 0x1F, (1 << 2));
	CHECK_EQUAL(2, SPI_TrxSpy_GetBytesWrittenCount());
}

TEST(Enc28J60_IO, Wcr_sendsValueByte)
{
	Enc28J60_IO_Wcr(io, 0x1F, 0xAA);
	CHECK_EQUAL(0xAA, SPI_TrxSpy_GetLastByteWritten());
}

TEST(Enc28J60_IO, Wcr_chipSelect)
{
	void (*fp_saved_Pin_Write)(Pin, const uint8_t) = Pin_Write;

	Pin_Write = FakePin_Write;
	SPI_Trx = FakeSPI_Trx;

	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 0);
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 1);

	Enc28J60_IO_Wcr(io, 0x1F, 0xFF);

	Pin_Write = fp_saved_Pin_Write;

	mock().checkExpectations();
	mock().clear();
}

/* Write PHY Register ************************************************/

TEST(Enc28J60_IO, Wpr)
{
	void (*fp_saved_Enc28J60_IO_Wcr)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Wcr;
	void (*fp_saved_Enc28J60_IO_Bank)(Enc28J60_IO, const uint8_t) = Enc28J60_IO_Bank;

	const uint8_t phy_regAddr = 0xFF;
	const uint16_t phy_regValue = 0xAA55;

	Enc28J60_IO_Wcr = FakeEnc28J60_IO_Wcr;
	Enc28J60_IO_Bank = FakeEnc28J60_IO_Bank;

	mock().expectOneCall("Enc28J60_IO_Bank").withParameter("bank", 2);
	mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x14).withParameter("value", phy_regAddr & 0x1F);
	mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x16).withParameter("value", phy_regValue & 0xFF);
	mock().expectOneCall("Enc28J60_IO_Wcr").withParameter("addr", 0x17).withParameter("value", ((phy_regValue >> 8) & 0xFF));

	Enc28J60_IO_Wpr(io, phy_regAddr, phy_regValue);

	Enc28J60_IO_Wcr = fp_saved_Enc28J60_IO_Wcr;
	Enc28J60_IO_Bank = fp_saved_Enc28J60_IO_Bank;

	mock().checkExpectations();
	mock().clear();
}

/* Write Buffer Memory ***********************************************/

TEST(Enc28J60_IO, Wbm_opCode_isThree)
{
	uint16_t len = 1;
	uint8_t buf[] = {0xFF};
	Enc28J60_IO_Wbm(io, 0x1F77, buf, len);
	CHECK_EQUAL(3, ((SPI_TrxSpy_GetByteWritten() & opcode_mask) >> opcode_offset));
}

TEST(Enc28J60_IO, Wbm_firstByteArgumentIs26)
{
	uint16_t len = 1;
	uint8_t buf[] = {0xFF};
	Enc28J60_IO_Wbm(io, 0x1F45, buf, len);
	CHECK_EQUAL(BUFFER_MEMORY_ARG, (SPI_TrxSpy_GetByteWritten() & ADDR_MASK));
}

TEST(Enc28J60_IO, Wbm_writesTwoBytesAtLeast)
{
	uint16_t len = 1;
	uint8_t buf[] = {0xFF};
	Enc28J60_IO_Wbm(io, 0x1F33, buf, len);
	CHECK(SPI_TrxSpy_GetBytesWrittenCount() > 2);
}

IGNORE_TEST(Enc28J60_IO, Wbm_sendsAddressByte)
{
	uint16_t len = 1;
	uint8_t buf[] = {0x23};
	Enc28J60_IO_Wbm(io, 0x1F42, buf, len);
	SPI_TrxSpy_GetByteWritten();
	CHECK_EQUAL(0x42, SPI_TrxSpy_GetByteWritten());
}

/* TODO Test data written using WBM */

TEST(Enc28J60_IO, Wbm_chipSelect)
{
	uint16_t len = 1;
	uint8_t buf[] = {0xAA};
	void (*fp_saved_Pin_Write)(Pin, const uint8_t) = Pin_Write;

	Pin_Write = FakePin_Write;
	SPI_Trx = FakeSPI_Trx;

	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 0);
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 1);

	Enc28J60_IO_Wbm(io, 0x1FFF, buf, len);

	Pin_Write = fp_saved_Pin_Write;

	mock().checkExpectations();
	mock().clear();
}

/* Bit Field Set *****************************************************/

TEST(Enc28J60_IO, Bfs_opCode_isFour)
{
	Enc28J60_IO_Bfs(io, 0x1F, 0x01);
	CHECK_EQUAL(4, ((SPI_TrxSpy_GetByteWritten() & opcode_mask) >> opcode_offset));
}

TEST(Enc28J60_IO, Bfs_masksRegisterAddress)
{
	uint8_t expectedAddress = 0x1A;
	uint8_t actualAddress;
	Enc28J60_IO_Bfs(io, expectedAddress, 0xCC);
	actualAddress = SPI_TrxSpy_GetByteWritten() & ADDR_MASK;
	CHECK_EQUAL(expectedAddress, actualAddress);
}

TEST(Enc28J60_IO, Bfs_writesTwoBytes)
{
	Enc28J60_IO_Bfs(io, 0x1F, 0xAA);
	CHECK_EQUAL(2, SPI_TrxSpy_GetBytesWrittenCount());
}

TEST(Enc28J60_IO, Bfs_sendsValueByte)
{
	Enc28J60_IO_Bfs(io, 0x1F, 0x55);
	CHECK_EQUAL(0x55, SPI_TrxSpy_GetLastByteWritten());
}

TEST(Enc28J60_IO, Bfs_chipSelect)
{
	void (*fp_saved_Pin_Write)(Pin, const uint8_t) = Pin_Write;

	Pin_Write = FakePin_Write;
	SPI_Trx = FakeSPI_Trx;

	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 0);
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 1);

	Enc28J60_IO_Bfs(io, 0x12, 0x01);

	Pin_Write = fp_saved_Pin_Write;

	mock().checkExpectations();
	mock().clear();
}

/* Bit Field Clear ***************************************************/

TEST(Enc28J60_IO, Bfc_opCode_isFive)
{
	Enc28J60_IO_Bfc(io, 0x1F, 0xFA);
	CHECK_EQUAL(5, ((SPI_TrxSpy_GetByteWritten() & opcode_mask) >> opcode_offset));
}

TEST(Enc28J60_IO, Bfc_masksRegisterAddress)
{
	uint8_t expectedAddress = 0x10;
	uint8_t actualAddress;
	Enc28J60_IO_Bfc(io, expectedAddress, 0xEE);
	actualAddress = SPI_TrxSpy_GetByteWritten() & ADDR_MASK;
	CHECK_EQUAL(expectedAddress, actualAddress);
}

TEST(Enc28J60_IO, Bfc_writesTwoBytes)
{
	Enc28J60_IO_Bfc(io, 0x1F, 0xFA);
	CHECK_EQUAL(2, SPI_TrxSpy_GetBytesWrittenCount());
}

TEST(Enc28J60_IO, Bfc_sendsValueByte)
{
	Enc28J60_IO_Bfc(io, 0x1F, 0x77);
	CHECK_EQUAL(0x77, SPI_TrxSpy_GetLastByteWritten());
}

TEST(Enc28J60_IO, Bfc_chipSelect)
{
	void (*fp_saved_Pin_Write)(Pin, const uint8_t) = Pin_Write;

	Pin_Write = FakePin_Write;
	SPI_Trx = FakeSPI_Trx;

	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 0);
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 1);

	Enc28J60_IO_Bfc(io, 0x03, 0xFF);

	Pin_Write = fp_saved_Pin_Write;

	mock().checkExpectations();
	mock().clear();
}

/* System Reset Command **********************************************/

TEST(Enc28J60_IO, Src)
{
	uint8_t systemResetCommand = 0xFF;

	Enc28J60_IO_Src(io);
	CHECK_EQUAL(systemResetCommand, SPI_TrxSpy_GetLastByteWritten());
}

TEST(Enc28J60_IO, Src_chipSelect)
{
	void (*fp_saved_Pin_Write)(Pin, const uint8_t) = Pin_Write;

	Pin_Write = FakePin_Write;
	SPI_Trx = FakeSPI_Trx;

	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 0);
	mock().expectOneCall("SPI_Trx");
	mock().expectOneCall("Pin_Write").withParameter("pin", csPin).withParameter("level", 1);

	Enc28J60_IO_Src(io);

	Pin_Write = fp_saved_Pin_Write;

	mock().checkExpectations();
	mock().clear();
}

/* Bank Select *******************************************************/

static void FakeEnc28J60_IO_Bfc(Enc28J60_IO self, const uint8_t addr, const uint8_t mask)
{
	mock().actualCall("Enc28J60_IO_Bfc").withParameter("addr", addr).withParameter("mask", mask);
}

static void FakeEnc28J60_IO_Bfs(Enc28J60_IO self, const uint8_t addr, const uint8_t mask)
{
	mock().actualCall("Enc28J60_IO_Bfs").withParameter("addr", addr).withParameter("mask", mask);
}

TEST(Enc28J60_IO, Bank_clearsBitsBeforeSet)
{
	enum {ECON1 = 0x1F};
	void (*fp_saved_Enc28J60_IO_Bfc)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Bfc;
	void (*fp_saved_Enc28J60_IO_Bfs)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Bfs;
	const uint8_t bank_mask = (1 << 1)|(1 << 0);
	const uint8_t bank = 2;

	Enc28J60_IO_Bfc = FakeEnc28J60_IO_Bfc;
	Enc28J60_IO_Bfs = FakeEnc28J60_IO_Bfs;

	mock().expectOneCall("Enc28J60_IO_Bfc").withParameter("addr", ECON1).withParameter("mask", bank_mask);
	mock().expectOneCall("Enc28J60_IO_Bfs").withParameter("addr", ECON1).withParameter("mask", bank);

	Enc28J60_IO_Bank(io, bank);

	Enc28J60_IO_Bfc = fp_saved_Enc28J60_IO_Bfc;
	Enc28J60_IO_Bfs = fp_saved_Enc28J60_IO_Bfs;

	mock().checkExpectations();
	mock().clear();
}

TEST(Enc28J60_IO, Bank_noChangeOnInvalidBank)
{
	void (*fp_saved_Enc28J60_IO_Bfc)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Bfc;
	void (*fp_saved_Enc28J60_IO_Bfs)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Bfs;

	Enc28J60_IO_Bfc = FakeEnc28J60_IO_Bfc;
	Enc28J60_IO_Bfs = FakeEnc28J60_IO_Bfs;

	mock().expectNoCall("Enc28J60_IO_Bfc");
	mock().expectNoCall("Enc28J60_IO_Bfs");

	Enc28J60_IO_Bank(io, 7);

	Enc28J60_IO_Bfc = fp_saved_Enc28J60_IO_Bfc;
	Enc28J60_IO_Bfs = fp_saved_Enc28J60_IO_Bfs;

	mock().checkExpectations();
	mock().clear();
}
