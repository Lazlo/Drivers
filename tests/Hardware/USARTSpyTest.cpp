extern "C"
{
#include "USARTSpy.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(USARTSpy)
{
    USART uSART;
    void setup()
    {
      USARTSpy_Create();
      USART_Create(0, 0, 0, 0, 0, 0);
    }

    void teardown()
    {
       USART_Destroy(uSART);
       USARTSpy_Destroy();
    }
};

TEST(USARTSpy, DataRegisterEmpty_SetReturnTrueOnCall)
{
    USARTSpy_DataRegisterEmpty_SetReturnTrueOnCall(20);
    CHECK_EQUAL(20, USARTSpy_DataRegisterEmpty_ReturnTrueOnCall);
}

TEST(USARTSpy, DataRegisterEmpty_GetCallsMade_isZeroOnCreate)
{
    CHECK_EQUAL(0, USARTSpy_DataRegisterEmpty_GetCallsMade());
}

TEST(USARTSpy, DataRegisterEmpty_incrmentsCallsMade)
{
    USARTSpy_DataRegisterEmpty(uSART);
    USARTSpy_DataRegisterEmpty(uSART);
    USARTSpy_DataRegisterEmpty(uSART);
    CHECK_EQUAL(3, USARTSpy_DataRegisterEmpty_GetCallsMade());
}

TEST(USARTSpy, DataRegisterEmpty_returnFalseByDefault)
{
    CHECK_EQUAL(0, USARTSpy_DataRegisterEmpty(uSART));
}

TEST(USARTSpy, DataRegisterEmpty_returnTrueOnNthCall)
{
    uint8_t i;
    USARTSpy_DataRegisterEmpty_SetReturnTrueOnCall(30);
    for (i = 0; i < 30 - 2; i++)
        USARTSpy_DataRegisterEmpty(uSART);
    CHECK_EQUAL(0, USARTSpy_DataRegisterEmpty(uSART));
    CHECK_EQUAL(1, USARTSpy_DataRegisterEmpty(uSART));
}

TEST(USARTSpy, Putc_GetCallsMade_isZeroOnCreate)
{
    CHECK_EQUAL(0, USARTSpy_Putc_GetCallsMade());
}

TEST(USARTSpy, Putc_incrementsCallsMade)
{
    USARTSpy_Putc(uSART, 't');
    USARTSpy_Putc(uSART, 'e');
    USARTSpy_Putc(uSART, 's');
    USARTSpy_Putc(uSART, 't');
    CHECK_EQUAL(4, USARTSpy_Putc_GetCallsMade());
}

TEST(USARTSpy, RxComplete_SetReturnTrueOnCall)
{
    USARTSpy_RxComplete_SetReturnTrueOnCall(10);
    CHECK_EQUAL(10, USARTSpy_RxComplete_ReturnTrueOnCall);
}

TEST(USARTSpy, RxComplete_GetCallsMade_isZeroOnCreate)
{
    CHECK_EQUAL(0, USARTSpy_RxComplete_GetCallsMade());
}

TEST(USARTSpy, RxComplete_incrementsCallsMade)
{
    USARTSpy_RxComplete(uSART);
    USARTSpy_RxComplete(uSART);
    CHECK_EQUAL(2, USARTSpy_RxComplete_GetCallsMade());
}

TEST(USARTSpy, RxComplete_returnFalseByDefault)
{
    CHECK_EQUAL(0, USARTSpy_RxComplete(uSART));
}

TEST(USARTSpy, RxComplete_returnTrueOnNthCall)
{
    uint8_t i;
    USARTSpy_RxComplete_SetReturnTrueOnCall(50);
    for (i = 0; i < 50 - 2; i++)
        USARTSpy_RxComplete(uSART);
    CHECK_EQUAL(0, USARTSpy_RxComplete(uSART));
    CHECK_EQUAL(1, USARTSpy_RxComplete(uSART));
}
