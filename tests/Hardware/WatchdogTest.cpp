extern "C"
{
#include "Watchdog.h"
#include "WatchdogInternal.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(Watchdog_m8)
{
    uint8_t wdReg_wdtcr;

    void setup()
    {
      Watchdog_Create(0, &wdReg_wdtcr);
    }

    void teardown()
    {
       Watchdog_Destroy();
    }
};

TEST_GROUP(Watchdog_m328p)
{
    uint8_t wdReg_wdtcsr;

    void setup()
    {
      Watchdog_Create(1, &wdReg_wdtcsr);
    }

    void teardown()
    {
       Watchdog_Destroy();
    }
};

/* Same for ATmega8 and ATmega328P */
TEST(Watchdog_m8, Enable_turnOn)
{
    uint8_t before = (1 << WDP2)|(1 << WDP1)|(1 << WDP0);
    uint8_t after = ((1 << WDCE)|(1 << WDE)|(1 << WDP2)|(1 << WDP1)|(1 << WDP0));
    wdReg_wdtcr = before;
    Watchdog_Enable(1);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

/* Same for ATmega8 and ATmega328P */
TEST(Watchdog_m8, Enable_turnOff)
{
    uint8_t before = (1 << WDE)|(1 << WDP2)|(1 << WDP1)|(1 << WDP0);
    uint8_t after = ((1 << WDCE)|(1 << WDP2)|(1 << WDP1)|(1 << WDP0));
    wdReg_wdtcr = before;
    Watchdog_Enable(0);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

/* SetPrescaler - for target ATmega8 *********************************/

TEST(Watchdog_m8, SetPrescaler_set16k)
{
    uint8_t before = (1 << WDP2)|(1 << WDP1)|(1 << WDP0);
    uint8_t after = 0;
    wdReg_wdtcr = before;
    Watchdog_SetPrescaler(16);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

TEST(Watchdog_m8, SetPrescaler_set32k)
{
    uint8_t before = (1 << WDP2)|(1 << WDP1);
    uint8_t after = (1 << WDP0);
    wdReg_wdtcr = before;
    Watchdog_SetPrescaler(32);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

TEST(Watchdog_m8, SetPrescaler_set64k)
{
    uint8_t before = (1 << WDP2)|(1 << WDP0);
    uint8_t after = (1 << WDP1);
    wdReg_wdtcr = before;
    Watchdog_SetPrescaler(64);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

TEST(Watchdog_m8, SetPrescaler_set128k)
{
    uint8_t before = (1 << WDP2);
    uint8_t after = ((1 << WDP1)|(1 << WDP0));
    wdReg_wdtcr = before;
    Watchdog_SetPrescaler(128);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

TEST(Watchdog_m8, SetPrescaler_set256k)
{
    uint8_t before = (1 << WDP1)|(1 << WDP0);
    uint8_t after = (1 << WDP2);
    wdReg_wdtcr = before;
    Watchdog_SetPrescaler(256);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

TEST(Watchdog_m8, SetPrescaler_set512k)
{
    uint8_t before = (1 << WDP1);
    uint8_t after = (1 << WDP2)|(1 << WDP0);
    wdReg_wdtcr = before;
    Watchdog_SetPrescaler(512);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

TEST(Watchdog_m8, SetPrescaler_set1024k)
{
    uint8_t before = (1 << WDP0);
    uint8_t after = ((1 << WDP2)|(1 << WDP1));
    wdReg_wdtcr = before;
    Watchdog_SetPrescaler(1024);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

TEST(Watchdog_m8, SetPrescaler_set2048k)
{
    uint8_t before = 0;
    uint8_t after = ((1 << WDP2)|(1 << WDP1)|(1 << WDP0));
    wdReg_wdtcr = before;
    Watchdog_SetPrescaler(2048);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

TEST(Watchdog_m8, SetPrescaler_noChangeOnOutOfBounds)
{
    uint8_t before = 0xFF;
    uint8_t after = 0xFF;
    wdReg_wdtcr = before;
    Watchdog_SetPrescaler(7);
    CHECK_EQUAL(after, wdReg_wdtcr);
}

/* SetPrescaler - for ATmega328P *************************************/

TEST(Watchdog_m328p, SetPrescaler_set2k)
{
    uint8_t before = (1 << WDP3)|(1 << WDP2)|(1 << WDP1)|(1 << WDP0);
    uint8_t after = 0;
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(2);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}

TEST(Watchdog_m328p, SetPrescaler_set4k)
{
    uint8_t before = (1 << WDP3)|(1 << WDP2)|(1 << WDP1);
    uint8_t after = (1 << WDP0);
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(4);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}

TEST(Watchdog_m328p, SetPrescaler_set8k)
{
    uint8_t before = (1 << WDP3)|(1 << WDP2)|(1 << WDP0);
    uint8_t after = (1 << WDP1);
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(8);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}

TEST(Watchdog_m328p, SetPrescaler_set16k)
{
    uint8_t before = (1 << WDP3)|(1 << WDP2);
    uint8_t after = (1 << WDP1)|(1 << WDP0);
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(16);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}

TEST(Watchdog_m328p, SetPrescaler_set32k)
{
    uint8_t before = (1 << WDP3)|(1 << WDP1)|(1 << WDP0);
    uint8_t after = (1 << WDP2);
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(32);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}

TEST(Watchdog_m328p, SetPrescaler_set64k)
{
    uint8_t before = (1 << WDP3)|(1 << WDP1);
    uint8_t after = (1 << WDP2)|(1 << WDP0);
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(64);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}

TEST(Watchdog_m328p, SetPrescaler_set128k)
{
    uint8_t before = (1 << WDP3)|(1 << WDP0);
    uint8_t after = (1 << WDP2)|(1 << WDP1);
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(128);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}

TEST(Watchdog_m328p, SetPrescaler_set256k)
{
    uint8_t before = (1 << WDP3);
    uint8_t after = (1 << WDP2)|(1 << WDP1)|(1 << WDP0);
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(256);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}

TEST(Watchdog_m328p, SetPrescaler_set512k)
{
    uint8_t before = (1 << WDP2)|(1 << WDP1)|(1 << WDP0);
    uint8_t after = (1 << WDP3);
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(512);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}

TEST(Watchdog_m328p, SetPrescaler_set1024k)
{
    uint8_t before = (1 << WDP2)|(1 << WDP1);
    uint8_t after = (1 << WDP3)|(1 << WDP0);
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(1024);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}

TEST(Watchdog_m328p, SetPrescaler_noChangeOnOutOfBounds)
{
    uint8_t before = 0xFF;
    uint8_t after = 0xFF;
    wdReg_wdtcsr = before;
    Watchdog_SetPrescaler(2048);
    CHECK_EQUAL(after, wdReg_wdtcsr);
}
