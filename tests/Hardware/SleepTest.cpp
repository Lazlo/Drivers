extern "C"
{
#include "Sleep.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(Sleep_m8)
{
    uint8_t mcucr;

    void setup()
    {
      Sleep_Create(0, &mcucr);
    }

    void teardown()
    {
       Sleep_Destroy();
    }
};

TEST_GROUP(Sleep_m328p)
{
    uint8_t smcr;

    void setup()
    {
      Sleep_Create(1, &smcr);
    }

    void teardown()
    {
      Sleep_Destroy();
    }
};

TEST(Sleep_m8, SetEnable_turnOn)
{
    mcucr = 0;
    Sleep_SetEnable(1);
    CHECK_EQUAL((1 << 7), mcucr);
}

TEST(Sleep_m328p, SetEnable_turnOn)
{
    smcr = 0;
    Sleep_SetEnable(1);
    CHECK_EQUAL((1 << 0), smcr);
}

TEST(Sleep_m8, SetMode_idle)
{
    mcucr = (1 << 6)|(1 << 5)|(1 << 4);
    Sleep_SetMode(0);
    CHECK_EQUAL(0, mcucr);
}

TEST(Sleep_m328p, SetMode_idle)
{
    smcr = (1 << 3)|(1 << 2)|(1 << 1);
    Sleep_SetMode(0);
    CHECK_EQUAL(0, smcr);
}

TEST(Sleep_m8, SetMode_adcNoiseReduction)
{
    mcucr = (1 << 6)|(1 << 5);
    Sleep_SetMode(1);
    CHECK_EQUAL((1 << 4), mcucr);
}

TEST(Sleep_m328p, SetMode_adcNoiseReduction)
{
    smcr = (1 << 3)|(1 << 2);
    Sleep_SetMode(1);
    CHECK_EQUAL((1 << 1), smcr);
}

TEST(Sleep_m8, SetMode_powerDown)
{
    mcucr = (1 << 6)|(1 << 4);
    Sleep_SetMode(2);
    CHECK_EQUAL((1 << 5), mcucr);
}

TEST(Sleep_m328p, SetMode_powerDown)
{
    smcr = (1 << 3)|(1 << 1);
    Sleep_SetMode(2);
    CHECK_EQUAL((1 << 2), smcr);
}

TEST(Sleep_m8, SetMode_powerSave)
{
    mcucr = (1 << 6);
    Sleep_SetMode(3);
    CHECK_EQUAL((1 << 5)|(1 << 4), mcucr);
}

TEST(Sleep_m328p, SetMode_powerSave)
{
    smcr = (1 << 3);
    Sleep_SetMode(3);
    CHECK_EQUAL((1 << 2)|(1 << 1), smcr);
}

TEST(Sleep_m8, SetMode_noChangeOnReserved4)
{
    mcucr = 0;
    Sleep_SetMode(4);
    CHECK_EQUAL(0, mcucr);
}

TEST(Sleep_m328p, SetMode_noChangeOnReserved4)
{
    smcr = 0;
    Sleep_SetMode(4);
    CHECK_EQUAL(0, smcr);
}

TEST(Sleep_m8, SetMode_noChangeOnReserver5)
{
    mcucr = 0;
    Sleep_SetMode(5);
    CHECK_EQUAL(0, mcucr);
}

TEST(Sleep_m328p, SetMode_noChangeOnReserved5)
{
    smcr = 0;
    Sleep_SetMode(5);
    CHECK_EQUAL(0, smcr);
}

TEST(Sleep_m8, SetMode_standby)
{
    mcucr = (1 << 4);
    Sleep_SetMode(6);
    CHECK_EQUAL((1 << 6)|(1 << 5), mcucr);
}

TEST(Sleep_m328p, SetMode_standby)
{
    smcr = (1 << 1);
    Sleep_SetMode(6);
    CHECK_EQUAL((1 << 3)|(1 << 2), smcr);
}

TEST(Sleep_m8, SetMode_noChangeOnOutOfBounds)
{
    mcucr = 0;
    Sleep_SetMode(7);
    CHECK_EQUAL(0, mcucr);
}

TEST(Sleep_m328p, SetMode_externalStandby)
{
    smcr = 0;
    Sleep_SetMode(7);
    CHECK_EQUAL((1 << 3)|(1 << 2)|(1 << 1), smcr);
}
