extern "C"
{
#include "I2C.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(I2C)
{
    uint8_t i2cReg_twbr;
    uint8_t i2cReg_twcr;
    uint8_t i2cReg_twsr;
    uint8_t i2cReg_twdr;
    uint8_t i2cReg_twar;

    void setup()
    {
      I2C_Create(&i2cReg_twbr, &i2cReg_twcr, &i2cReg_twsr, &i2cReg_twdr, &i2cReg_twar);
    }

    void teardown()
    {
       I2C_Destroy();
    }
};

TEST(I2C, SetSlaveAddress)
{
    i2cReg_twar = 0;
    I2C_SetSlaveAddress(127);
    CHECK_EQUAL(254, i2cReg_twar);
}

TEST(I2C, SetSlaveAddress_noChangeOnOutOfBounds)
{
    i2cReg_twar = 0;
    I2C_SetSlaveAddress(255);
    CHECK_EQUAL(0, i2cReg_twar);
}

TEST(I2C, SetGeneralCallEnable_turnOn)
{
    i2cReg_twar = 0;
    I2C_SetGeneralCallEnable(1);
    CHECK_EQUAL(1, i2cReg_twar);
}

TEST(I2C, SetGeneralCallEnable_turnOff)
{
    i2cReg_twar = 1;
    I2C_SetGeneralCallEnable(0);
    CHECK_EQUAL(0, i2cReg_twar);
}

TEST(I2C, GetInterruptFlag_isSet)
{
    i2cReg_twcr = (1 << 7);
    CHECK_EQUAL(1, I2C_GetInterruptFlag());
}

TEST(I2C, GetInterruptFlag_isNotSet)
{
    i2cReg_twcr = 0;
    CHECK_EQUAL(0, I2C_GetInterruptFlag());
}

TEST(I2C, ClearInterruptFlag)
{
    /* Clear interrupt flag by writing it 1 */
    i2cReg_twcr = 0;
    I2C_ClearInterruptFlag();
    CHECK_EQUAL((1 << 7), i2cReg_twcr);
}

TEST(I2C, SetAcknowledgeEnable_turnOn)
{
    i2cReg_twcr = 0;
    I2C_SetAcknowledgeEnable(1);
    CHECK_EQUAL((1 << 6), i2cReg_twcr);
}

TEST(I2C, SetAcknowledgeEnable_turnOff)
{
    i2cReg_twcr = (1 << 6);
    I2C_SetAcknowledgeEnable(0);
    CHECK_EQUAL(0, i2cReg_twcr);
}

TEST(I2C, Start)
{
    i2cReg_twcr = 0;
    I2C_Start();
    CHECK_EQUAL((1 << 5), i2cReg_twcr);
}

TEST(I2C, Stop)
{
    i2cReg_twcr = 0;
    I2C_Stop();
    CHECK_EQUAL((1 << 4), i2cReg_twcr);
}

TEST(I2C, GetWriteCollisionFlag_isSet)
{
    i2cReg_twcr = (1 << 3);
    CHECK_EQUAL(1, I2C_GetWriteCollisionFlag());
}

TEST(I2C, GetWriteCollisionFlag_isNotSet)
{
    i2cReg_twcr = 0;
    CHECK_EQUAL(0, I2C_GetWriteCollisionFlag());
}

TEST(I2C, Enable_turnOn)
{
    i2cReg_twcr = 0;
    I2C_Enable(1);
    CHECK_EQUAL((1 << 1), i2cReg_twcr);
}

TEST(I2C, Enable_turnOff)
{
    i2cReg_twcr = (1 << 1);
    I2C_Enable(0);
    CHECK_EQUAL(0, i2cReg_twcr);
}

TEST(I2C, SetInterruptEnable_turnOn)
{
    i2cReg_twcr = 0;
    I2C_SetInterruptEnable(1);
    CHECK_EQUAL((1 << 0), i2cReg_twcr);
}

TEST(I2C, SetInterruptEnable_turnOff)
{
    i2cReg_twcr = (1 << 0);
    I2C_SetInterruptEnable(0);
    CHECK_EQUAL(0, i2cReg_twcr);
}

TEST(I2C, SetPrescaler_setTo1)
{
    i2cReg_twsr = (1 << 1)|(1 << 0);
    I2C_SetPrescaler(1);
    CHECK_EQUAL(0, i2cReg_twsr);
}

TEST(I2C, SetPrescaler_setTo4)
{
    i2cReg_twsr = (1 << 1);
    I2C_SetPrescaler(4);
    CHECK_EQUAL((1 << 0), i2cReg_twsr);
}

TEST(I2C, SetPrescaler_setTo16)
{
    i2cReg_twsr = (1 << 0);
    I2C_SetPrescaler(16);
    CHECK_EQUAL((1 << 1), i2cReg_twsr);
}

TEST(I2C, SetPrescaler_setTo64)
{
    i2cReg_twsr = 0;
    I2C_SetPrescaler(64);
    CHECK_EQUAL((1 << 1)|(1 << 0), i2cReg_twsr);
}

TEST(I2C, SetPrescaler_noChangeOnOutOfBounds0)
{
    i2cReg_twsr = 255;
    I2C_SetPrescaler(0);
    CHECK_EQUAL(255, i2cReg_twsr);
}

TEST(I2C, SetPrescaler_noChangeOnOutOfBounds128)
{
    i2cReg_twsr = 255;
    I2C_SetPrescaler(128);
    CHECK_EQUAL(255, i2cReg_twsr);
}

TEST(I2C, GetStatus)
{
    i2cReg_twsr = (1 << 7)|(1 << 3)|(1 << 2)|(1 << 1)|(1 << 0);
    CHECK_EQUAL((1 << 7)|(1 << 3), I2C_GetStatus());
}

TEST(I2C, Read)
{
    i2cReg_twdr = 0x55;
    CHECK_EQUAL(0x55, I2C_Read());
}

TEST(I2C, Write)
{
    i2cReg_twdr = 0;
    I2C_Write(0xAA);
    CHECK_EQUAL(0xAA, i2cReg_twdr);
}

TEST(I2C, SetClockFrequency_400kHzAt16MHz)
{
    I2C_SetClockFrequency(400, 16000000);
    CHECK_EQUAL(12, i2cReg_twbr);
}

TEST(I2C, SetClockFrequency_100kHzAt16MHz)
{
    I2C_SetClockFrequency(100, 16000000);
    CHECK_EQUAL(72, i2cReg_twbr);
}
