extern "C"
{
#include "USART.h"
#include "USARTSpy.h"
}

#include "CppUTest/TestHarness.h"

#define USART_CREATE() USART_Create(&uSARTReg.ucsra, &uSARTReg.ucsrb, &uSARTReg.ucsrc, \
                                        &uSARTReg.ubrrl, &uSARTReg.ubrrh, &uSARTReg.udr)

typedef struct USARTRegisterStruct
{
    uint8_t ucsra;
    uint8_t ucsrb;
    uint8_t ucsrc;
    uint8_t ubrrl;
    uint8_t ubrrh;
    uint8_t udr;
} USARTRegister;

TEST_GROUP(USART)
{
    USART uSART;
    USARTRegister uSARTReg;

    void setup()
    {
        uSART = USART_CREATE();
    }

    void teardown()
    {
        USART_Destroy(uSART);
    }
};

TEST(USART, Create_Singleton)
{
    USART a, b;

    a = USART_CREATE();
    b = USART_CREATE();
    POINTERS_EQUAL(a, b);
}

TEST(USART, SetBaudRate_9600at16MHz)
{
    uint16_t ubrr;
    USART_SetBaudRate(uSART, 9600, 16000000);
    ubrr = uSARTReg.ubrrl;
    ubrr |= (uint16_t)(uSARTReg.ubrrh << 8);
    CHECK_EQUAL(103, ubrr);
}

TEST(USART, SetBaudRate_38400at16MHz)
{
    uint16_t ubrr;
    USART_SetBaudRate(uSART, 38400, 16000000);
    ubrr = uSARTReg.ubrrl;
    ubrr |= (uint16_t)(uSARTReg.ubrrh << 8);
    CHECK_EQUAL(25, ubrr);
}

TEST(USART, SetMode_synchronous)
{
    USART_SetMode(uSART, 1);
    CHECK_EQUAL((1 << 7)|(1 << 6), uSARTReg.ucsrc);
}

TEST(USART, SetMode_asynchronous)
{
    uSARTReg.ucsrc = (1 << 6);
    USART_SetMode(uSART, 0);
    CHECK_EQUAL((1 << 7), uSARTReg.ucsrc);
}

TEST(USART, SetMode_retainRegisterBits)
{
    uSARTReg.ucsrc = 255;
    USART_SetMode(uSART, 0);
    CHECK_EQUAL(255 - (1 << 6), uSARTReg.ucsrc);
}

TEST(USART, SetParity_setNone)
{
    uSARTReg.ucsrc = 255;
    USART_SetParity(uSART, 0);
    CHECK_EQUAL(255 - ((1 << 5)|(1 << 4)), uSARTReg.ucsrc);
}

TEST(USART, SetParity_noChangeOnSetReserved)
{
    uSARTReg.ucsrc = 255 - ((1 << 5)|(1 << 4));
    USART_SetParity(uSART, 1);
    CHECK_EQUAL(255 - ((1 << 5)|(1 << 4)), uSARTReg.ucsrc);
}

TEST(USART, SetParity_setEven)
{
    uSARTReg.ucsrc = 255 - ((1 << 5)|(1 << 4));
    USART_SetParity(uSART, 2);
    CHECK_EQUAL(255 - (1 << 4), uSARTReg.ucsrc);
}

TEST(USART, SetParity_setOdd)
{
    uSARTReg.ucsrc = 255 - ((1 << 5)|(1 << 4));
    USART_SetParity(uSART, 3);
    CHECK_EQUAL(255, uSARTReg.ucsrc);
}

TEST(USART, SetStopBits_set1StopBit)
{
    /* Make sure URSEL bit is written by implementation when writing to UCSRC (on ATmega8 only). */
    uSARTReg.ucsrc = 127;
    USART_SetStopBits(uSART, 1);
    CHECK_EQUAL(255 - (1 << 3), uSARTReg.ucsrc);
}

TEST(USART, SetStopBits_set2StopBits)
{
    uSARTReg.ucsrc = 127 - (1 << 3);
    USART_SetStopBits(uSART, 2);
    CHECK_EQUAL(255, uSARTReg.ucsrc);
}

TEST(USART, SetStopBits_noChangeOnOutOfBounds_value0)
{
    USART_SetStopBits(uSART, 0);
    CHECK_EQUAL(0, uSARTReg.ucsrc);
}

TEST(USART, SetStopBits_noChangeOnOutOfBounds_value3)
{
    USART_SetStopBits(uSART, 3);
    CHECK_EQUAL(0, uSARTReg.ucsrc);
}

TEST(USART, SetCharacterSize_set5Bit)
{
    uSARTReg.ucsrb = (1 << 2); /* UCSZ2 */
    uSARTReg.ucsrc = (1 << 2)|(1 << 1); /* UCSZ1 UCSZ0 */
    USART_SetCharacterSize(uSART, 5);
    CHECK_EQUAL(0, uSARTReg.ucsrb);
    CHECK_EQUAL(128, uSARTReg.ucsrc); /* Make sure URSEL has been set */
}

TEST(USART, SetCharacterSize_set6Bits)
{
    uSARTReg.ucsrb = (1 << 2);
    uSARTReg.ucsrc = (1 << 2);
    USART_SetCharacterSize(uSART, 6);
    CHECK_EQUAL(0, uSARTReg.ucsrb);
    CHECK_EQUAL(128 + (1 << 1), uSARTReg.ucsrc);
}

TEST(USART, SetCharacterSize_set7Bits)
{
    uSARTReg.ucsrb = (1 << 2);
    uSARTReg.ucsrc = (1 << 1);
    USART_SetCharacterSize(uSART, 7);
    CHECK_EQUAL(0, uSARTReg.ucsrb);
    CHECK_EQUAL(128 + (1 << 2), uSARTReg.ucsrc);
}

TEST(USART, SetCharacterSize_set8Bits)
{
    uSARTReg.ucsrb = (1 << 2);
    uSARTReg.ucsrc = 0;
    USART_SetCharacterSize(uSART, 8);
    CHECK_EQUAL(0, uSARTReg.ucsrb);
    CHECK_EQUAL(128 + ((1 << 2)|(1 << 1)), uSARTReg.ucsrc);
}

TEST(USART, SetCharacterSize_set9Bits)
{
    uSARTReg.ucsrb = 0;
    uSARTReg.ucsrc = 0;
    USART_SetCharacterSize(uSART, 9);
    CHECK_EQUAL((1 << 2), uSARTReg.ucsrb);
    CHECK_EQUAL(128 + ((1 << 2)|(1 << 1)), uSARTReg.ucsrc);
}

TEST(USART, SetCharacterSize_noChangeOnOutOfBounds_4)
{
    uSARTReg.ucsrb = 0;
    uSARTReg.ucsrc = 0;
    USART_SetCharacterSize(uSART, 4);
    CHECK_EQUAL(0, uSARTReg.ucsrb);
    CHECK_EQUAL(0, uSARTReg.ucsrc);
}

TEST(USART, SetCharacterSize_noChangeOnOutOfBounds_10)
{
    uSARTReg.ucsrb = 0;
    uSARTReg.ucsrc = 0;
    USART_SetCharacterSize(uSART, 10);
    CHECK_EQUAL(0, uSARTReg.ucsrb);
    CHECK_EQUAL(0, uSARTReg.ucsrc);
}

TEST(USART, SetClockPolarity_set0)
{
    uSARTReg.ucsrc = (1 << 0);
    USART_SetClockPolarity(uSART, 0);
    CHECK_EQUAL(128, uSARTReg.ucsrc);
}

TEST(USART, SetClockPolarity_set1)
{
    uSARTReg.ucsrc = 0;
    USART_SetClockPolarity(uSART, 1);
    CHECK_EQUAL(128 + (1 << 0), uSARTReg.ucsrc);
}

TEST(USART, SetRxCompleteInterruptEnable_turnOn)
{
    USART_SetRxCompleteInterruptEnable(uSART, 1);
    CHECK_EQUAL((1 << 7), uSARTReg.ucsrb);
}

TEST(USART, SetRxCompleteInterruptEnable_turnOff)
{
    uSARTReg.ucsrb = 255;
    USART_SetRxCompleteInterruptEnable(uSART, 0);
    CHECK_EQUAL(255 - (1 << 7), uSARTReg.ucsrb);
}

TEST(USART, SetTxCompleteInterruptEnable_turnOn)
{
    USART_SetTxCompleteInterruptEnable(uSART, 1);
    CHECK_EQUAL((1 << 6), uSARTReg.ucsrb);
}

TEST(USART, SetTxCompleteInterruptEnable_turnOff)
{
    uSARTReg.ucsrb = 255;
    USART_SetTxCompleteInterruptEnable(uSART, 0);
    CHECK_EQUAL(255 - (1 << 6), uSARTReg.ucsrb);
}

TEST(USART, SetDataRegisterEmptyInterruptEnable_turnOn)
{
    USART_SetDataRegisterEmptyInterruptEnable(uSART, 1);
    CHECK_EQUAL((1 << 5), uSARTReg.ucsrb);
}

TEST(USART, SetDataRegisterEmptyInterruptEnable_turnOff)
{
    uSARTReg.ucsrb = 255;
    USART_SetDataRegisterEmptyInterruptEnable(uSART, 0);
    CHECK_EQUAL(255 - (1 << 5), uSARTReg.ucsrb);
}

TEST(USART, Enable_turnOn)
{
    USART_Enable(uSART, 1);
    CHECK_EQUAL((1 << 4)|(1 << 3), uSARTReg.ucsrb);
}

TEST(USART, Enable_turnOff)
{
    uSARTReg.ucsrb = 0xFF;
    USART_Enable(uSART, 0);
    CHECK_EQUAL(0xFF - ((1 << 4)|(1 << 3)), uSARTReg.ucsrb);
}

TEST(USART, Write)
{
    USART_Write(uSART, 0x55);
    CHECK_EQUAL(0x55, uSARTReg.udr);
}

TEST(USART, Read)
{
    uSARTReg.udr = 0xAA;
    CHECK_EQUAL(0xAA, USART_Read(uSART));
}

TEST(USART, RxComplete_isSet)
{
    uSARTReg.ucsra = (1 << 7);
    CHECK_EQUAL(1, USART_RxComplete(uSART));
}

TEST(USART, DataRegisterEmpty)
{
    uSARTReg.ucsra = (1 << 5);
    CHECK_EQUAL(1, USART_DataRegisterEmpty(uSART));
}

TEST(USART, HasFrameError)
{
    uSARTReg.ucsra = (1 << 4);
    CHECK_EQUAL(1, USART_HasFrameError(uSART));
}

TEST(USART, HasDataOverrun)
{
    uSARTReg.ucsra = (1 << 3);
    CHECK_EQUAL(1, USART_HasDataOverrun(uSART));
}

TEST(USART, HasParityError)
{
    uSARTReg.ucsra = (1 << 2);
    CHECK_EQUAL(1, USART_HasParityError(uSART));
}

TEST(USART, fp_DataRegisterEmpty_initialized)
{
    POINTERS_EQUAL(USART_DataRegisterEmpty, fp_USART_DataRegisterEmpty);
}

TEST(USART, Putc_waitsForDataRegisterEmpty)
{
    uint8_t (*fp_saved_USART_DataRegisterEmpty)(USART);

    USARTSpy_Create();

    /* save original function pointer */
    fp_saved_USART_DataRegisterEmpty = fp_USART_DataRegisterEmpty;

    /* install our spy */
    fp_USART_DataRegisterEmpty = USARTSpy_DataRegisterEmpty;

    USARTSpy_DataRegisterEmpty_SetReturnTrueOnCall(10);

    USART_Putc(uSART, 'z');

    CHECK_EQUAL(10, USARTSpy_DataRegisterEmpty_GetCallsMade());
    CHECK_EQUAL('z', uSARTReg.udr);

    /* restore original function pointer */
    fp_USART_DataRegisterEmpty = fp_saved_USART_DataRegisterEmpty;

    USARTSpy_Destroy();
}

TEST(USART, fp_Putc_initialized)
{
    POINTERS_EQUAL(USART_Putc, fp_USART_Putc);
}

TEST(USART, Puts_callsPutc)
{
    void (*fp_saved_USART_Putc)(USART, const char);

    USARTSpy_Create();

    /* save original function pointer */
    fp_saved_USART_Putc = fp_USART_Putc;

    /* install our spy */
    fp_USART_Putc = USARTSpy_Putc;

    USART_Puts(uSART, "test");

    CHECK_EQUAL(4, USARTSpy_Putc_GetCallsMade());

    /* restore original function pointer */
    fp_USART_Putc = fp_saved_USART_Putc;

    USARTSpy_Destroy();
}

TEST(USART, fp_RxComplete_initialized)
{
    POINTERS_EQUAL(USART_RxComplete, fp_USART_RxComplete);
}

TEST(USART, Getc_callsRxComplete)
{
    uint8_t (*fp_saved_USART_RxComplete)(USART);

    USARTSpy_Create();

    /* save original function pointer */
    fp_saved_USART_RxComplete = fp_USART_RxComplete;

    /* install our spy */
    fp_USART_RxComplete = USARTSpy_RxComplete;

    USARTSpy_RxComplete_SetReturnTrueOnCall(55);

    uSARTReg.udr = 'k';
    CHECK_EQUAL('k', USART_Getc(uSART));
    CHECK_EQUAL(55, USARTSpy_RxComplete_GetCallsMade());

    /* restore original function pointer */
    fp_USART_RxComplete = fp_saved_USART_RxComplete;

    USARTSpy_Destroy();
}
