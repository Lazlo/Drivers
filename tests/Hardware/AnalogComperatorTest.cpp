extern "C"
{
#include "AnalogComperator.h"
}

#include "CppUTest/TestHarness.h"

TEST_GROUP(AnalogComperator)
{
    uint8_t acReg_sfior;
    uint8_t acReg_acsr;

    void setup()
    {
      AnalogComperator_Create(&acReg_sfior, &acReg_acsr);
    }

    void teardown()
    {
       AnalogComperator_Destroy();
    }
};

TEST(AnalogComperator, MultiplexerEnable_turnOn)
{
    acReg_sfior = 0;
    AnalogComperator_MultiplexerEnable(1);
    CHECK_EQUAL((1 << 3), acReg_sfior);
}

TEST(AnalogComperator, MultiplexerEnable_turnOff)
{
    acReg_sfior = (1 << 3);
    AnalogComperator_MultiplexerEnable(0);
    CHECK_EQUAL(0, acReg_sfior);
}

TEST(AnalogComperator, Disable_set)
{
    acReg_acsr = 0;
    AnalogComperator_Disable(1);
    CHECK_EQUAL((1 << 7), acReg_acsr);
}

TEST(AnalogComperator, Disable_clear)
{
    acReg_acsr = (1 << 7);
    AnalogComperator_Disable(0);
    CHECK_EQUAL(0, acReg_acsr);
}

TEST(AnalogComperator, SetBandgapSelect_set)
{
    acReg_acsr = 0;
    AnalogComperator_SetBandgapSelect(1);
    CHECK_EQUAL((1 << 6), acReg_acsr);
}

TEST(AnalogComperator, SetBandgapSelect_clear)
{
    acReg_acsr = (1 << 6);
    AnalogComperator_SetBandgapSelect(0);
    CHECK_EQUAL(0, acReg_acsr);
}

TEST(AnalogComperator, GetOutput_isSet)
{
    acReg_acsr = (1 << 5);
    CHECK_EQUAL(1, AnalogComperator_GetOutput());
}

TEST(AnalogComperator, GetOutput_isNotSet)
{
    acReg_acsr = 0;
    CHECK_EQUAL(0, AnalogComperator_GetOutput());
}

TEST(AnalogComperator, GetInterruptFlag_isSet)
{
    acReg_acsr = (1 << 4);
    CHECK_EQUAL(1, AnalogComperator_GetInterruptFlag());
}

TEST(AnalogComperator, GetInterrptFlag_isNotSet)
{
    acReg_acsr = 0;
    CHECK_EQUAL(0, AnalogComperator_GetInterruptFlag());
}

TEST(AnalogComperator, SetInterruptEnable_set)
{
    acReg_acsr = 0;
    AnalogComperator_SetInterruptEnable(1);
    CHECK_EQUAL((1 << 3), acReg_acsr);
}

TEST(AnalogComperator, SetInterruptEnable_clear)
{
    acReg_acsr = (1 << 3);
    AnalogComperator_SetInterruptEnable(0);
    CHECK_EQUAL(0, acReg_acsr);
}

TEST(AnalogComperator, SetInputCaptureEnable_set)
{
    acReg_acsr = 0;
    AnalogComperator_SetInputCaptureEnable(1);
    CHECK_EQUAL((1 << 2), acReg_acsr);
}

TEST(AnalogComperator, SetInputCaptureEnable_clear)
{
    acReg_acsr = (1 << 2);
    AnalogComperator_SetInputCaptureEnable(0);
    CHECK_EQUAL(0, acReg_acsr);
}

TEST(AnalogComperator, SetInterruptMode_onToggle)
{
    acReg_acsr = (1 << 1)|(1 << 0);
    AnalogComperator_SetInterruptMode(0);
    CHECK_EQUAL(0, acReg_acsr);
}

TEST(AnalogComperator, SetInterruptMode_noChangeOnReserved)
{
    acReg_acsr = 0;
    AnalogComperator_SetInterruptMode(1);
    CHECK_EQUAL(0, acReg_acsr);
}

TEST(AnalogComperator, SetInterruptMode_fallingEdge)
{
    acReg_acsr = (1 << 0);
    AnalogComperator_SetInterruptMode(2);
    CHECK_EQUAL((1 << 1), acReg_acsr);
}

TEST(AnalogComperator, SetInterruptMode_risingEdge)
{
    acReg_acsr = 0;
    AnalogComperator_SetInterruptMode(3);
    CHECK_EQUAL((1 << 1)|(1 << 0), acReg_acsr);
}

TEST(AnalogComperator, SetInterruptMode_noChangeOnOutOfBounds)
{
    acReg_acsr = 0;
    AnalogComperator_SetInterruptMode(7);
    CHECK_EQUAL(0, acReg_acsr);
}
