#include "CppUTest/TestHarness.h"
#include "DriversBuildTime.h"

TEST_GROUP(DriversBuildTime)
{
  DriversBuildTime* projectBuildTime;

  void setup()
  {
    projectBuildTime = new DriversBuildTime();
  }
  void teardown()
  {
    delete projectBuildTime;
  }
};

TEST(DriversBuildTime, Create)
{
  CHECK(0 != projectBuildTime->GetDateTime());
}

