#include "IOSpy.h"
#include <stdlib.h>

static volatile uint8_t **lastReadAddr;
static uint8_t callsMadeToRead;
static uint8_t callsMadeToRead_passedOn;

static volatile uint8_t *lastWriteAddr;
static uint8_t lastWriteValue;

void IOSpy_Create(const uint8_t recordReadCalls)
{
    lastReadAddr = calloc(recordReadCalls, sizeof(volatile uint8_t *));
    callsMadeToRead = 0;
    callsMadeToRead_passedOn = 0;

    lastWriteAddr = 0;
    lastWriteValue = 0;
}

void IOSpy_Destroy(void)
{
    free(lastReadAddr);
}

volatile uint8_t *IOSpy_GetLastReadAddr(void)
{
    if (!callsMadeToRead)
        return 0;
    return lastReadAddr[callsMadeToRead - 1];
}

uint8_t IOSpy_Read(const volatile uint8_t *addr)
{
    lastReadAddr[callsMadeToRead] = (volatile uint8_t *)addr;
    callsMadeToRead++;
    return 0;
}

uint8_t IOSpy_GetCallsMadeToRead(void)
{
    return callsMadeToRead;
}

volatile uint8_t *IOSpy_GetReadAddr(void)
{
    volatile uint8_t *addr;
    addr = lastReadAddr[callsMadeToRead_passedOn++];
    return addr;
}

volatile uint8_t *IOSpy_GetLastWriteAddr(void)
{
    return lastWriteAddr;
}

uint8_t IOSpy_GetLastWriteValue(void)
{
    return lastWriteValue;
}

void IOSpy_Write(volatile uint8_t *addr, const uint8_t data)
{
    lastWriteAddr = addr;
    lastWriteValue = data;
}
