#include "IO.h"

void IO_Create(void)
{
}

void IO_Destroy(void)
{
}

uint8_t IO_Read(const volatile uint8_t *addr)
{
    return *addr;
}

void IO_Write(volatile uint8_t *addr, const uint8_t data)
{
    *addr = data;
}
