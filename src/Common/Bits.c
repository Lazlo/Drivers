#include "Bits.h"

uint8_t BitIsSet(volatile const uint8_t *var, const uint8_t offset)
{
    return *var & (1 << offset) ? 1 : 0;
}

void SetBit(volatile uint8_t *var, const uint8_t offset, const uint8_t set)
{
    if (set)
        *var |= (uint8_t)(1 << offset);
    else
        *var &= (uint8_t)~(1 << offset);
}

void UpdateBits(volatile uint8_t *var, const uint8_t mask, const uint8_t value)
{
    *var &= (uint8_t)~mask;
    *var |= value & mask;
}
