#include "DriversBuildTime.h"

DriversBuildTime::DriversBuildTime()
: dateTime(__DATE__ " " __TIME__)
{
}

DriversBuildTime::~DriversBuildTime()
{
}

const char* DriversBuildTime::GetDateTime()
{
    return dateTime;
}

