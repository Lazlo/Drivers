#include "USARTSpy.h"

uint8_t USARTSpy_DataRegisterEmpty_ReturnTrueOnCall;
uint8_t USARTSpy_RxComplete_ReturnTrueOnCall;

static uint8_t USARTSpy_DataRegisterEmpty_CallsMade;
static uint8_t USARTSpy_Putc_CallsMade;
static uint8_t USARTSpy_RxComplete_CallsMade;

void USARTSpy_Create(void)
{
    USARTSpy_DataRegisterEmpty_CallsMade = 0;
    USARTSpy_Putc_CallsMade = 0;
    USARTSpy_RxComplete_CallsMade = 0;
}

void USARTSpy_Destroy(void)
{
}

void USARTSpy_DataRegisterEmpty_SetReturnTrueOnCall(const uint8_t nthCall)
{
    USARTSpy_DataRegisterEmpty_ReturnTrueOnCall = nthCall;
}

uint8_t USARTSpy_DataRegisterEmpty_GetCallsMade(void)
{
    return USARTSpy_DataRegisterEmpty_CallsMade;
}

uint8_t USARTSpy_DataRegisterEmpty(USART self)
{
    uint8_t *callsMade = &USARTSpy_DataRegisterEmpty_CallsMade;
    uint8_t *returnTrueOnCall = &USARTSpy_DataRegisterEmpty_ReturnTrueOnCall;

    if (++(*callsMade) == *returnTrueOnCall)
        return 1;
    return 0;
}

uint8_t USARTSpy_Putc_GetCallsMade(void)
{
    return USARTSpy_Putc_CallsMade;
}

void USARTSpy_Putc(USART self, const char c)
{
    uint8_t *callsMade = &USARTSpy_Putc_CallsMade;
    ++(*callsMade);
}

void USARTSpy_RxComplete_SetReturnTrueOnCall(const uint8_t nthCall)
{
    USARTSpy_RxComplete_ReturnTrueOnCall = nthCall;
}

uint8_t USARTSpy_RxComplete_GetCallsMade(void)
{
    return USARTSpy_RxComplete_CallsMade;
}

uint8_t USARTSpy_RxComplete(USART self)
{
    uint8_t *callsMade = &USARTSpy_RxComplete_CallsMade;
    uint8_t *returnTrueOnCall = &USARTSpy_RxComplete_ReturnTrueOnCall;
    if (++(*callsMade) == *returnTrueOnCall)
        return 1;
    return 0;
}
