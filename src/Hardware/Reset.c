#include "Reset.h"
#include "Bits.h"

/* ATmega8 MCUCR register bits */
enum {WDRF = 3, BORF = 2, EXTRF = 1, PORF = 0};

static volatile uint8_t *rsReg_mcucr;
static uint8_t rsRegCached_mcucr;

void Reset_Create(volatile uint8_t *mcucr)
{
    rsReg_mcucr = mcucr;
    rsRegCached_mcucr = *rsReg_mcucr;
    *rsReg_mcucr = 0;
}

void Reset_Destroy(void)
{
}

uint8_t Reset_WasCausedByWatchdog(void)
{
    return BitIsSet(&rsRegCached_mcucr, WDRF);
}

uint8_t Reset_WasCausedByBrownOut(void)
{
    return BitIsSet(&rsRegCached_mcucr, BORF);
}

uint8_t Reset_WasCausedByExternalReset(void)
{
    return BitIsSet(&rsRegCached_mcucr, EXTRF);
}

uint8_t Reset_WasCausedByPowerOnReset(void)
{
    return BitIsSet(&rsRegCached_mcucr, PORF);
}
