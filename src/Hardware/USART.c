#include "USART.h"
#include "Bits.h"

#define USART_UNITS 1

/* ATmega8 UCSRC bits
 *
 * NOTE: On ATmega8 we need to write URSEL bit 1 in order to access UCSRC.
 */
enum {URSEL = 7, UMSEL = 6, UPM1 = 5, UPM0 = 4, USBS = 3, UCSZ1 = 2, UCSZ0 = 1, UCPOL = 0};

/* ATmega8 UCSRB bits */
enum {RXCIE = 7, TXCIE = 6, UDRIE = 5, RXEN = 4, TXEN = 3, UCSZ2 = 2};

/* ATmega8 USART UCSRA bits */
enum {RXC = 7, UDRE = 5, FE = 4, DOR = 3, PE = 2};

typedef struct USARTStruct
{
    volatile uint8_t *ucsra;
    volatile uint8_t *ucsrb;
    volatile uint8_t *ucsrc;
    volatile uint8_t *ubrrl;
    volatile uint8_t *ubrrh;
    volatile uint8_t *udr;
} USARTStruct;

static USARTStruct usart[USART_UNITS];

USART USART_Create(volatile uint8_t *ucsra, volatile uint8_t *ucsrb, volatile uint8_t *ucsrc,
                        volatile uint8_t *ubrrl, volatile uint8_t *ubrrh, volatile uint8_t *udr)
{
     USART self = &usart[0];
     self->ucsra = ucsra;
     self->ucsrb = ucsrb;
     self->ucsrc = ucsrc;
     self->ubrrl = ubrrl;
     self->ubrrh = ubrrh;
     self->udr = udr;
     return self;
}

void USART_Destroy(USART self)
{
}

void USART_SetBaudRate(USART self, const uint32_t baud, const uint32_t f_cpu)
{
    uint16_t ubrr = (uint16_t)((f_cpu / (baud * 16)) - 1);

    *self->ubrrl = (uint8_t)ubrr;
    *self->ubrrh = (uint8_t)(ubrr >> 8);
}

/* For accessing UCSRC on ATmega8 we need to write the URSEL bit along with whatever else
 * we change in the register. In order to do so, independent if we clear or set a bit,
 * we created this variation of UpdateBits(). */
static void UpdateBits2(volatile uint8_t *var, const uint8_t mask, const uint8_t value)
{
    uint8_t tmp;
    tmp = *var;
    tmp &= (uint8_t)~mask;
    tmp |= value;
    *var = tmp;
}

void USART_SetMode(USART self, const uint8_t sync)
{
    const uint8_t mask = (1 << URSEL)|(1 << UMSEL);
    uint8_t tmp = (1 << URSEL);
    if (sync)
        tmp |= (1 << UMSEL);
    UpdateBits2(self->ucsrc, mask, tmp);
}

void USART_SetParity(USART self, const uint8_t mode)
{
    const uint8_t mask = (1 << UPM1)|(1 << UPM0);
    const uint8_t offset = UPM0;
    uint8_t tmp = (1 << URSEL);
    if (mode == 1)
        return;
    tmp = (uint8_t)(mode << offset);
    UpdateBits2(self->ucsrc, mask, tmp);
}

void USART_SetStopBits(USART self, const uint8_t bits)
{
    const uint8_t mask = (1 << USBS);
    uint8_t tmp = (1 << URSEL);
    if (bits != 1 && bits != 2)
        return;
    if (bits == 2)
        tmp |= mask;
    UpdateBits2(self->ucsrc, mask, tmp);
}

void USART_SetCharacterSize(USART self, const uint8_t bits)
{
    const uint8_t mask_b = (1 << UCSZ2);
    const uint8_t mask_c = (1 << UCSZ1)|(1 << UCSZ0);
    uint8_t tmp_b = 0;
    uint8_t tmp_c = (1 << URSEL);
    if (bits < 5 || bits > 9)
        return;
    if (bits == 6)
        tmp_c |= (1 << UCSZ0);
    else if (bits == 7)
        tmp_c |= (1 << UCSZ1);
    else if (bits == 8)
        tmp_c |= mask_c;
    else if (bits == 9)
    {
        tmp_b |= mask_b;
        tmp_c |= mask_c;
    }
    UpdateBits(self->ucsrb, mask_b, tmp_b);
    UpdateBits2(self->ucsrc, mask_c, tmp_c);
}

void USART_SetClockPolarity(USART self, const uint8_t mode)
{
    const uint8_t mask = (1 << UCPOL);
    uint8_t tmp = (1 << URSEL);
    if (mode)
        tmp |= mask;
    UpdateBits2(self->ucsrc, mask, tmp);
}

void USART_SetRxCompleteInterruptEnable(USART self, const uint8_t enable)
{
    SetBit(self->ucsrb, RXCIE, enable);
}

void USART_SetTxCompleteInterruptEnable(USART self, const uint8_t enable)
{
    SetBit(self->ucsrb, TXCIE, enable);
}

void USART_SetDataRegisterEmptyInterruptEnable(USART self, const uint8_t enable)
{
    SetBit(self->ucsrb, UDRIE, enable);
}

void USART_Enable(USART self, const uint8_t enable)
{
    const uint8_t mask = (1 << RXEN)|(1 << TXEN);
    UpdateBits(self->ucsrb, mask, (uint8_t)(enable ? mask : 0));
}

void USART_Write(USART self, const uint8_t byte)
{
    *self->udr = byte;
}

uint8_t USART_Read(USART self)
{
    return *self->udr;
}

uint8_t USART_RxComplete(USART self)
{
    return BitIsSet(self->ucsra, RXC);
}

uint8_t USART_DataRegisterEmpty(USART self)
{
    return BitIsSet(self->ucsra, UDRE);
}

uint8_t USART_HasFrameError(USART self)
{
    return BitIsSet(self->ucsra, FE);
}

uint8_t USART_HasDataOverrun(USART self)
{
    return BitIsSet(self->ucsra, DOR);
}

uint8_t USART_HasParityError(USART self)
{
    return BitIsSet(self->ucsra, PE);
}

uint8_t (*fp_USART_DataRegisterEmpty)(USART) = USART_DataRegisterEmpty;

void USART_Putc(USART self, const char c)
{
    while (!(*fp_USART_DataRegisterEmpty)(self))
        ;
    USART_Write(self, (uint8_t)c);
}

void (*fp_USART_Putc)(USART, const char c) = USART_Putc;

void USART_Puts(USART self, const char *s)
{
    while (*s)
        (*fp_USART_Putc)(self, *s++);
}

uint8_t (*fp_USART_RxComplete)(USART) = USART_RxComplete;

char USART_Getc(USART self)
{
    while (!(*fp_USART_RxComplete)(self))
        ;
    return (char)USART_Read(self);
}
