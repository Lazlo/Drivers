#include "SPI_TrxSpy.h"
#include <stdlib.h>

static uint8_t *bytesWritten;
static uint8_t nBytesWritten;
static uint8_t nBytesWritten_returned;

static uint8_t *returnBytes;
static uint8_t returnBytesSet;

void SPI_TrxSpy_Create(const uint8_t bytesToRecord)
{
    bytesWritten = calloc(bytesToRecord, sizeof(uint8_t));
    returnBytes = calloc(bytesToRecord, sizeof(uint8_t));
    nBytesWritten = 0;
    nBytesWritten_returned = 0;
    returnBytesSet = 0;
}

void SPI_TrxSpy_Destroy(void)
{
    free(bytesWritten);
    free(returnBytes);
}

uint8_t SPI_TrxSpy(const uint8_t byte)
{
    uint8_t rx;
    bytesWritten[nBytesWritten] = byte;
    rx = returnBytes[nBytesWritten];
    nBytesWritten++;
    return rx;
}

uint8_t SPI_TrxSpy_GetBytesWrittenCount(void)
{
    return nBytesWritten;
}

uint8_t SPI_TrxSpy_GetBytesWrittenReturned(void)
{
    return nBytesWritten_returned;
}

uint8_t SPI_TrxSpy_GetLastByteWritten(void)
{
    return bytesWritten[nBytesWritten - 1];
}

uint8_t SPI_TrxSpy_GetByteWritten(void)
{
    if (!nBytesWritten)
        return 0;
    return bytesWritten[nBytesWritten_returned++];
}

void SPI_TrxSpy_SetReturnByte(const uint8_t byte)
{
    returnBytes[returnBytesSet++] = byte;
}
