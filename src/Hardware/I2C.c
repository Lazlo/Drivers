#include "I2C.h"

/* ATmega8 TWCR register bits */
enum {TWINT = 7, TWEA = 6, TWSTA = 5, TWSTO = 4, TWWC = 3, TWEN = 1, TWIE = 0};

/* ATmega8 TWSR register bits */
enum {TWS7 = 7, TWS6 = 6, TWS5 = 5, TWS4 = 4, TWS3 = 3, TWPS1 = 1, TWPS0 = 0};

/* ATmega8 TWAR register bits */
enum {TWGCE = 0};

static volatile uint8_t *i2cReg_twbr;
static volatile uint8_t *i2cReg_twcr;
static volatile uint8_t *i2cReg_twsr;
static volatile uint8_t *i2cReg_twdr;
static volatile uint8_t *i2cReg_twar;

void I2C_Create(volatile uint8_t *twbr, volatile uint8_t *twcr, volatile uint8_t *twsr, volatile uint8_t *twdr, volatile uint8_t *twar)
{
    i2cReg_twbr = twbr;
    i2cReg_twcr = twcr;
    i2cReg_twsr = twsr;
    i2cReg_twdr = twdr;
    i2cReg_twar = twar;
}

void I2C_Destroy(void)
{
}

void I2C_SetSlaveAddress(const uint8_t sla)
{
    const uint8_t max = 127;
    const uint8_t offset = 1;
    if (sla > max)
        return;
    *i2cReg_twar = (uint8_t)(sla << offset);
}

void I2C_SetGeneralCallEnable(const uint8_t enable)
{
    if (enable)
        *i2cReg_twar |= (1 << TWGCE);
    else
        *i2cReg_twar &= (uint8_t)~(1 << TWGCE);
}

uint8_t I2C_GetInterruptFlag(void)
{
    return *i2cReg_twcr & (1 << TWINT) ? 1 : 0;
}

void I2C_ClearInterruptFlag(void)
{
    *i2cReg_twcr |= (1 << TWINT);
}

void I2C_SetAcknowledgeEnable(const uint8_t enable)
{
    if (enable)
        *i2cReg_twcr |= (1 << TWEA);
    else
        *i2cReg_twcr &= (uint8_t)~(1 << TWEA);
}

void I2C_Start(void)
{
    /* NOTE Bit must be cleared after START condition has been
     * transmited by hardware. */
    *i2cReg_twcr |= (1 << TWSTA);
}

void I2C_Stop(void)
{
    *i2cReg_twcr |= (1 << TWSTO);
}

uint8_t I2C_GetWriteCollisionFlag(void)
{
    return *i2cReg_twcr & (1 << TWWC) ? 1 : 0;
}

void I2C_Enable(const uint8_t enable)
{
    if (enable)
        *i2cReg_twcr |= (1 << TWEN);
    else
        *i2cReg_twcr &= (uint8_t)~(1 << TWEN);
}

void I2C_SetInterruptEnable(const uint8_t enable)
{
    if (enable)
        *i2cReg_twcr |= (1 << TWIE);
    else
        *i2cReg_twcr &= (uint8_t)~(1 << TWIE);
}

void I2C_SetPrescaler(const uint8_t prescaler)
{
    const uint8_t mask = (1 << TWPS1)|(1 << TWPS0);
    uint8_t ps;
    if (prescaler == 1)
        ps = 0;
    else if (prescaler == 4)
        ps = (1 << TWPS0);
    else if (prescaler == 16)
        ps = (1 << TWPS1);
    else if (prescaler == 64)
        ps = mask;
    else
        return;
    *i2cReg_twsr &= (uint8_t)~mask;
    *i2cReg_twsr |= ps & mask;
}

uint8_t I2C_GetStatus(void)
{
    const uint8_t mask = (1 << TWS7)|(1 << TWS6)|(1 << TWS5)|(1 << TWS4)|(1 << TWS3);
    return *i2cReg_twsr & mask;
}

uint8_t I2C_Read(void)
{
    return *i2cReg_twdr;
}

void I2C_Write(const uint8_t byte)
{
    *i2cReg_twdr = byte;
}

void I2C_SetClockFrequency(const uint16_t freq_khz, const uint32_t f_cpu)
{
    *i2cReg_twbr = (uint8_t)((f_cpu / (uint32_t)(freq_khz * 1000)) - 16) / 2;
}
