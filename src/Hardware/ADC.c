#include "ADC.h"
#include "Bits.h"
#include "IO.h"

/* ATmega8 ADMUX register bits */
enum {REFS1 = 7, REFS0 = 6, ADLAR = 5, MUX3 = 3, MUX2 = 2, MUX1 = 1, MUX0 = 0};

/* ATmega8 ADCSRA register bits */
enum {ADEN = 7, ADSC = 6, ADFR = 5, ADIF = 4, ADIE = 3, ADPS2 = 2, ADPS1 = 1, ADPS0 = 0};

typedef struct ADCRegisterStruct
{
    volatile uint8_t *admux;
    volatile uint8_t *adcsra;
    volatile uint8_t *adch;
    volatile uint8_t *adcl;
} ADCRegisterStruct;

static ADCRegisterStruct adcReg;

void ADC_Create(volatile uint8_t *admux, volatile uint8_t *adcsra, volatile uint8_t *adch, volatile uint8_t *adcl)
{
    adcReg.admux = admux;
    adcReg.adcsra = adcsra;
    adcReg.adch = adch;
    adcReg.adcl = adcl;
}

void ADC_Destroy(void)
{
}

void ADC_SetReferenceVoltage(const uint8_t ref)
{
    const uint8_t mask = (1 << REFS1)|(1 << REFS0);
    const uint8_t offset = REFS0;
    const uint8_t max = 3;
    if (ref == 2 || ref > max)
        return;
    UpdateBits(adcReg.admux, mask, (uint8_t)(ref << offset));
}

void ADC_SetLeftAdjustResult(const uint8_t enable)
{
    SetBit(adcReg.admux, ADLAR, enable);
}

void ADC_SetChannel(const uint8_t channel)
{
    const uint8_t mask = (1 << MUX3)|(1 << MUX2)|(1 << MUX1)|(1 << MUX0);
    const uint8_t max = 15;
    if (channel > max || (channel > 7 && channel < 14))
        return;
    UpdateBits(adcReg.admux, mask, channel);
}

void ADC_Enable(const uint8_t enable)
{
    SetBit(adcReg.adcsra, ADEN, enable);
}

void ADC_StartConversion(void)
{
    *adcReg.adcsra |= (1 << ADSC);
}

uint8_t ADC_IsConversionComplete(void)
{
    return !BitIsSet(adcReg.adcsra, ADSC);
}

void ADC_SetFreeRunning(const uint8_t enable)
{
    SetBit(adcReg.adcsra, ADFR, enable);
}

uint8_t ADC_GetInterruptFlag(void)
{
    return BitIsSet(adcReg.adcsra, ADIF);
}

void ADC_SetInterruptEnable(const uint8_t enable)
{
    SetBit(adcReg.adcsra, ADIE, enable);
}

void ADC_SetPrescaler(const uint8_t prescaler)
{
    const uint8_t mask = (1 << ADPS2)|(1 << ADPS1)|(1 << ADPS0);
    uint8_t adps = 0;
    if (prescaler != 2 && prescaler != 4 && prescaler != 8 && prescaler != 16 && prescaler != 32 && prescaler != 64 && prescaler != 128)
        return;
    if (prescaler == 4)
        adps = (1 << ADPS1);
    if (prescaler == 8)
        adps = (1 << ADPS1)|(1 << ADPS0);
    if (prescaler == 16)
        adps = (1 << ADPS2);
    if (prescaler == 32)
        adps = (1 << ADPS2)|(1 << ADPS0);
    if (prescaler == 64)
        adps = (1 << ADPS2)|(1 << ADPS1);
    if (prescaler == 128)
        adps = mask;
    UpdateBits(adcReg.adcsra, mask, adps);
}

uint8_t (*adc_fp_IO_Read)(const volatile uint8_t *) = IO_Read;

uint16_t ADC_GetResult(void)
{
    uint16_t res = 0;
    /* read ADCL before ADCH */
    res = (uint16_t)(adc_fp_IO_Read)(adcReg.adcl);
    res |= (uint16_t)(adc_fp_IO_Read(adcReg.adch) << 8);
    return res;
}
