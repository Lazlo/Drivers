#include "Enc28J60.h"
#include "Enc28J60_IO.h"
#include "Enc28J60_Registers.h"
#include <stdlib.h>
#include <string.h>

typedef struct Enc28J60Struct
{
    Enc28J60_IO io;
} Enc28J60Struct;

static void Enc28J60_IO_SetBit(Enc28J60_IO, const uint8_t, const uint8_t, const uint8_t, const uint8_t);

Enc28J60 Enc28J60_Create(const Pin csPin)
{
     Enc28J60 self = calloc(1, sizeof(Enc28J60Struct));
     self->io = Enc28J60_IO_Create(csPin);
     return self;
}

void Enc28J60_Destroy(Enc28J60 self)
{
    Enc28J60_IO_Destroy(self->io);
    free(self);
}

/* Buffer Configuration **********************************************/

static void Enc28J60_IO_SetBufferAddr(Enc28J60_IO self, const uint8_t addr, const uint16_t value)
{
    const uint16_t mask = ENC28J60_RAM_END;
    Enc28J60_IO_Bank(self, BANK0);
    Enc28J60_IO_Wcr(self, addr, (uint8_t)(value & (mask & 0xFF)));
    Enc28J60_IO_Wcr(self, (uint8_t)(addr + 1), (uint8_t)((value >> 8) & ((mask >> 8) & 0xFF)));
}

static uint16_t Enc28J60_IO_GetBufferAddr(Enc28J60_IO self, const uint8_t addr)
{
    const uint16_t mask = ENC28J60_RAM_END;
    uint16_t value;
    Enc28J60_IO_Bank(self, BANK0);
    value = Enc28J60_IO_Rcr(self, addr);
    value |= (uint16_t)(Enc28J60_IO_Rcr(self, (uint8_t)(addr + 1)) << 8);
    return value & mask;
}

static void Enc28J60_SetRxStart_impl(Enc28J60 self, const uint16_t addr)
{
    Enc28J60_IO_SetBufferAddr(self->io, ERXSTL, addr);
}

void (*Enc28J60_SetRxStart)(Enc28J60, const uint16_t) = Enc28J60_SetRxStart_impl;

uint16_t Enc28J60_GetRxStart(Enc28J60 self)
{
    return Enc28J60_IO_GetBufferAddr(self->io, ERXSTL);
}

static void Enc28J60_SetRxEnd_impl(Enc28J60 self, const uint16_t addr)
{
    Enc28J60_IO_SetBufferAddr(self->io, ERXNDL, addr);
}

void (*Enc28J60_SetRxEnd)(Enc28J60, const uint16_t) = Enc28J60_SetRxEnd_impl;

uint16_t Enc28J60_GetRxEnd(Enc28J60 self)
{
    return Enc28J60_IO_GetBufferAddr(self->io, ERXNDL);
}

void Enc28J60_SetRxReadPtr(Enc28J60 self, const uint16_t addr)
{
    Enc28J60_IO_SetBufferAddr(self->io, ERXRDPTL, addr);
}

uint16_t Enc28J60_GetRxReadPtr(Enc28J60 self)
{
    return Enc28J60_IO_GetBufferAddr(self->io, ERXRDPTL);
}

static void Enc28J60_SetTxStart_impl(Enc28J60 self, const uint16_t addr)
{
    Enc28J60_IO_SetBufferAddr(self->io, ETXSTL, addr);
}

void (*Enc28J60_SetTxStart)(Enc28J60, const uint16_t) = Enc28J60_SetTxStart_impl;

uint16_t Enc28J60_GetTxStart(Enc28J60 self)
{
    return Enc28J60_IO_GetBufferAddr(self->io, ETXSTL);
}

static void Enc28J60_SetTxEnd_impl(Enc28J60 self, const uint16_t addr)
{
    Enc28J60_IO_SetBufferAddr(self->io, ETXNDL, addr);
}

void (*Enc28J60_SetTxEnd)(Enc28J60, const uint16_t) = Enc28J60_SetTxEnd_impl;

uint16_t Enc28J60_GetTxEnd(Enc28J60 self)
{
    return Enc28J60_IO_GetBufferAddr(self->io, ETXNDL);
}

static void Enc28J60_SetReadPtr_impl(Enc28J60 self, const uint16_t addr)
{
    Enc28J60_IO_SetBufferAddr(self->io, ERDPTL, addr);
}

void (*Enc28J60_SetReadPtr)(Enc28J60, const uint16_t) = Enc28J60_SetReadPtr_impl;

uint16_t Enc28J60_GetReadPtr(Enc28J60 self)
{
    return Enc28J60_IO_GetBufferAddr(self->io, ERDPTL);
}

static void Enc28J60_SetWritePtr_impl(Enc28J60 self, const uint16_t addr)
{
    Enc28J60_IO_SetBufferAddr(self->io, EWRPTL, addr);
}

void (*Enc28J60_SetWritePtr)(Enc28J60, const uint16_t) = Enc28J60_SetWritePtr_impl;

uint16_t Enc28J60_GetWritePtr(Enc28J60 self)
{
    return Enc28J60_IO_GetBufferAddr(self->io, EWRPTL);
}

/* Clock *************************************************************/

static uint8_t Enc28J60_IsClockReady_impl(Enc28J60 self)
{
    const uint8_t mask_clkrdy = (1 << CLKRDY);
    uint8_t clkrdy;
    clkrdy = Enc28J60_IO_Rcr(self->io, ESTAT) & mask_clkrdy;
    return clkrdy;
}

uint8_t (*Enc28J60_IsClockReady)(Enc28J60) = Enc28J60_IsClockReady_impl;

/* Ethernet Controller Configuration *********************************/

static void Enc28J60_SetReceptionEnable_impl(Enc28J60 self, const uint8_t enable)
{
    void (*fp_bfop)(Enc28J60_IO, const uint8_t, const uint8_t);

    if (enable)
        fp_bfop = Enc28J60_IO_Bfs;
    else
        fp_bfop = Enc28J60_IO_Bfc;

    (fp_bfop)(self->io, ECON1, (1 << RXEN));
}

void (*Enc28J60_SetReceptionEnable)(Enc28J60, const uint8_t) = Enc28J60_SetReceptionEnable_impl;

uint8_t Enc28J60_GetReceptionEnable(Enc28J60 self)
{
    return Enc28J60_IO_Rcr(self->io, ECON1) & (1 << RXEN) ? 1 : 0;
}

void Enc28J60_SetInterruptEnable(Enc28J60 self, const uint8_t enable_mask)
{
    Enc28J60_IO_Wcr(self->io, EIE, enable_mask & 0xFB);
}

uint8_t Enc28J60_GetInterruptEnable(Enc28J60 self)
{
    return Enc28J60_IO_Rcr(self->io, EIE) & 0xFB;
}

/* Ethernet Packet Filters ------------------------------------------*/

void Enc28J60_SetBroadcastFilterEnable(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK1, ERXFCON, (1 << BCEN), enable);
}

void Enc28J60_SetMulticastFilterEnable(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK1, ERXFCON, (1 << MCEN), enable);
}

void Enc28J60_SetHashTableFilterEnable(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK1, ERXFCON, (1 << HTEN), enable);
}

void Enc28J60_SetMagicPacketFilterEnable(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK1, ERXFCON, (1 << MPEN), enable);
}

void Enc28J60_SetPatternMatchFilterEnable(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK1, ERXFCON, (1 << PMEN), enable);
}

void Enc28J60_SetInvalidCrcFilterEnable(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK1, ERXFCON, (1 << CRCEN), enable);
}

void Enc28J60_SetAndOrFilterEnable(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK1, ERXFCON, (1 << ANDOR), enable);
}

void Enc28J60_SetUnicastFilterEnable(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK1, ERXFCON, (1 << UCEN), enable);
}

/* Ethernet Controller Status ****************************************/

uint8_t Enc28J60_GetInterruptFlags(Enc28J60 self)
{
    return Enc28J60_IO_Rcr(self->io, EIR) & 0x7B;
}

uint8_t Enc28J60_GetPacketCount(Enc28J60 self)
{
    Enc28J60_IO_Bank(self->io, BANK1);
    return Enc28J60_IO_Rcr(self->io, EPKTCNT);
}

/* MAC Configuration *************************************************/

static void Enc28J60_IO_SetBit(Enc28J60_IO self, const uint8_t bank, const uint8_t addr, const uint8_t mask, const uint8_t value)
{
    void (*fp_bfop)(Enc28J60_IO, const uint8_t, const uint8_t);
    if (value)
        fp_bfop = Enc28J60_IO_Bfs;
    else
        fp_bfop = Enc28J60_IO_Bfc;
    Enc28J60_IO_Bank(self, bank);
    (fp_bfop)(self, addr, mask);
}

static uint8_t Enc28J60_IO_GetMacBit(Enc28J60_IO self, const uint8_t bank, const uint8_t addr, const uint8_t mask)
{
    Enc28J60_IO_Bank(self, bank);
    return Enc28J60_IO_Rmr(self, addr) & mask ? 1 : 0;
}

static void Enc28J60_SetMacReceiveEnable_impl(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK2, MACON1, (1 << MARXEN), enable);
}

void (*Enc28J60_SetMacReceiveEnable)(Enc28J60, const uint8_t) = Enc28J60_SetMacReceiveEnable_impl;

uint8_t Enc28J60_GetMacReceptionEnable(Enc28J60 self)
{
    return Enc28J60_IO_GetMacBit(self->io, BANK2, MACON1, (1 << MARXEN));
}

static void Enc28J60_SetTransmitChecksumEnable_impl(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK2, MACON3, (1 << TXCRCEN), enable);
}

void (*Enc28J60_SetTransmitChecksumEnable)(Enc28J60, const uint8_t) = Enc28J60_SetTransmitChecksumEnable_impl;

uint8_t Enc28J60_GetTransmitChecksumEnable(Enc28J60 self)
{
    return Enc28J60_IO_GetMacBit(self->io, BANK2, MACON3, (1 << TXCRCEN));
}

static void Enc28J60_SetDuplexMode_impl(Enc28J60 self, const uint8_t full)
{
    const uint8_t macon1_mask = (1 << TXPAUS)|(1 << RXPAUS);
    const uint8_t macon3_mask = (1 << FULDPX);
    uint16_t phcon1;
    void (*fp_bfop)(Enc28J60_IO, const uint8_t, const uint8_t);

    if (full)
        fp_bfop = Enc28J60_IO_Bfs;
    else
        fp_bfop = Enc28J60_IO_Bfc;

    /* Update MAC registers */
    Enc28J60_IO_Bank(self->io, BANK2);
    (fp_bfop)(self->io, MACON1, macon1_mask);
    (fp_bfop)(self->io, MACON3, macon3_mask);
    /* Update PHY registers */
    phcon1 = Enc28J60_IO_Rpr(self->io, PHCON1);
    if (full)
        phcon1 |= (1 << PDPXMD);
    else
        phcon1 &= (uint16_t)~(1 << PDPXMD);
    Enc28J60_IO_Wpr(self->io, PHCON1, phcon1);
}

void (*Enc28J60_SetDuplexMode)(Enc28J60, const uint8_t) = Enc28J60_SetDuplexMode_impl;

uint8_t Enc28J60_GetDuplexMode(Enc28J60 self)
{
    return Enc28J60_IO_GetMacBit(self->io, BANK2, MACON3, (1 << FULDPX));
}

static void Enc28J60_SetPaddingMode_impl(Enc28J60 self, const uint8_t mode)
{
    const uint8_t mode_mask = (1 << PADCFG2)|(1 << PADCFG1)|(1 << PADCFG0);
    const uint8_t mode_offset = PADCFG0;

    Enc28J60_IO_Bank(self->io, BANK2);
    Enc28J60_IO_Bfc(self->io, MACON3, mode_mask);
    Enc28J60_IO_Bfs(self->io, MACON3, (uint8_t)(mode << mode_offset));
}

void (*Enc28J60_SetPaddingMode)(Enc28J60, const uint8_t) = Enc28J60_SetPaddingMode_impl;

uint8_t Enc28J60_GetPaddingMode(Enc28J60 self)
{
    const uint8_t offset = PADCFG0;
    uint8_t pad_mode;

    Enc28J60_IO_Bank(self->io, BANK2);
    pad_mode = (uint8_t)(Enc28J60_IO_Rmr(self->io, MACON3) >> offset);
    return pad_mode;
}

static void Enc28J60_SetFrameLengthCheckEnable_impl(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK2, MACON3, (1 << FRMLNEN), enable);
}

void (*Enc28J60_SetFrameLengthCheckEnable)(Enc28J60, const uint8_t) = Enc28J60_SetFrameLengthCheckEnable_impl;

uint8_t Enc28J60_GetFrameLengthCheckEnable(Enc28J60 self)
{
    return Enc28J60_IO_GetMacBit(self->io, BANK2, MACON3, (1 << FRMLNEN));
}

static void Enc28J60_SetDeferTransmissionEnable_impl(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK2, MACON4, (1 << DEFER), enable);
}

void (*Enc28J60_SetDeferTransmissionEnable)(Enc28J60, const uint8_t) = Enc28J60_SetDeferTransmissionEnable_impl;

uint8_t Enc28J60_GetDeferTransmissionEnable(Enc28J60 self)
{
    return Enc28J60_IO_GetMacBit(self->io, BANK2, MACON4, (1 << DEFER));
}

static void Enc28J60_SetNoBackoffDuringBackpressureEnable_impl(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK2, MACON4, (1 << BPEN), enable);
}

void (*Enc28J60_SetNoBackoffDuringBackpressureEnable)(Enc28J60, const uint8_t) = Enc28J60_SetNoBackoffDuringBackpressureEnable_impl;

uint8_t Enc28J60_GetNoBackoffDuringBackpressureEnable(Enc28J60 self)
{
    return Enc28J60_IO_GetMacBit(self->io, BANK2, MACON4, (1 << BPEN));
}

static void Enc28J60_SetNoBackoffEnable_impl(Enc28J60 self, const uint8_t enable)
{
    Enc28J60_IO_SetBit(self->io, BANK2, MACON4, (1 << NOBKOFF), enable);
}

void (*Enc28J60_SetNoBackoffEnable)(Enc28J60, const uint8_t) = Enc28J60_SetNoBackoffEnable_impl;

uint8_t Enc28J60_GetNoBackoffEnable(Enc28J60 self)
{
    return Enc28J60_IO_GetMacBit(self->io, BANK2, MACON4, (1 << NOBKOFF));
}

static void Enc28J60_SetMaxFrameLen_impl(Enc28J60 self, const uint16_t max)
{
    Enc28J60_IO_Bank(self->io, BANK2);
    Enc28J60_IO_Wcr(self->io, MAMXFLL, (uint8_t)(max & 0xFF));
    Enc28J60_IO_Wcr(self->io, MAMXFLH, (uint8_t)((max >> 8) & 0xFF));
}

void (*Enc28J60_SetMaxFrameLen)(Enc28J60, const uint16_t) = Enc28J60_SetMaxFrameLen_impl;

uint16_t Enc28J60_GetMaxFrameLen(Enc28J60 self)
{
    uint16_t len;
    Enc28J60_IO_Bank(self->io, BANK2);
    len = (uint16_t)Enc28J60_IO_Rmr(self->io, MAMXFLL);
    len |= (uint16_t)(Enc28J60_IO_Rmr(self->io, MAMXFLH) << 8);
    return len;
}

static void Enc28J60_SetInterPacketGapDelayForBackToBack_impl(Enc28J60 self, const uint8_t delay)
{
    const uint8_t mask_ipg = 0x7F;
    Enc28J60_IO_Bank(self->io, BANK2);
    Enc28J60_IO_Wcr(self->io, MABBIPG, delay & mask_ipg);
}

void (*Enc28J60_SetInterPacketGapDelayForBackToBack)(Enc28J60, const uint8_t) = Enc28J60_SetInterPacketGapDelayForBackToBack_impl;

uint8_t Enc28J60_GetInterPacketGapDelayForBackToBack(Enc28J60 self)
{
    uint8_t ipg;
    Enc28J60_IO_Bank(self->io, BANK2);
    ipg = Enc28J60_IO_Rmr(self->io, MABBIPG);
    return ipg;
}

static void Enc28J60_SetInterPacketGapDelayForNonBackToBack_impl(Enc28J60 self, const uint16_t delay)
{
    Enc28J60_IO_Bank(self->io, BANK2);
    Enc28J60_IO_Wcr(self->io, MAIPGL, (uint8_t)(delay & 0xFF));
    Enc28J60_IO_Wcr(self->io, MAIPGH, (uint8_t)((delay >> 8) & 0xFF));
}

void (*Enc28J60_SetInterPacketGapDelayForNonBackToBack)(Enc28J60, const uint16_t) = Enc28J60_SetInterPacketGapDelayForNonBackToBack_impl;

uint16_t Enc28J60_GetInterPacketGapDelayForNonBackToBack(Enc28J60 self)
{
    uint16_t ipg;
    Enc28J60_IO_Bank(self->io, BANK2);
    ipg = (uint16_t)Enc28J60_IO_Rmr(self->io, MAIPGL);
    ipg |= (uint16_t)(Enc28J60_IO_Rmr(self->io, MAIPGH) << 8);
    return ipg;
}

static void Enc28J60_SetMaxRetransmission_impl(Enc28J60 self, const uint8_t retmax)
{
    const uint8_t retmax_mask = 0x0F;
    Enc28J60_IO_Bank(self->io, BANK2);
    Enc28J60_IO_Wcr(self->io, MACLCON1, retmax & retmax_mask);
}

void (*Enc28J60_SetMaxRetransmission)(Enc28J60, const uint8_t) = Enc28J60_SetMaxRetransmission_impl;

uint8_t Enc28J60_GetMaxRetransmission(Enc28J60 self)
{
    const uint8_t retmax_mask = 0x0F;
    Enc28J60_IO_Bank(self->io, BANK2);
    return Enc28J60_IO_Rmr(self->io, MACLCON1) & retmax_mask;
}

static void Enc28J60_SetCollisionWindow_impl(Enc28J60 self, const uint8_t colwin)
{
    const uint8_t colwin_mask = 0x3F;
    Enc28J60_IO_Bank(self->io, BANK2);
    Enc28J60_IO_Wcr(self->io, MACLCON2, colwin & colwin_mask);
}

void (*Enc28J60_SetCollisionWindow)(Enc28J60, const uint8_t) = Enc28J60_SetCollisionWindow_impl;

uint8_t Enc28J60_GetCollisionWindow(Enc28J60 self)
{
    const uint8_t colwin_mask = 0x3F;
    Enc28J60_IO_Bank(self->io, BANK2);
    return Enc28J60_IO_Rmr(self->io, MACLCON2) & colwin_mask;
}

static const uint8_t mac_reg[] = {MAADR1, MAADR2, MAADR3, MAADR4, MAADR5, MAADR6};

static void Enc28J60_SetMacAddr_impl(Enc28J60 self, const uint8_t *mac_addr)
{
    uint8_t i;

    Enc28J60_IO_Bank(self->io, BANK3);
    for (i = 0; i < 6; i++)
        Enc28J60_IO_Wcr(self->io, mac_reg[i], mac_addr[i]);
}

void (*Enc28J60_SetMacAddr)(Enc28J60, const uint8_t *) = Enc28J60_SetMacAddr_impl;

void Enc28J60_GetMacAddr(Enc28J60 self, uint8_t *mac_addr)
{
    uint8_t i;

    Enc28J60_IO_Bank(self->io, BANK3);
    for (i = 0; i < 6; i++)
        mac_addr[i] = Enc28J60_IO_Rmr(self->io, mac_reg[i]);
}

/* Initialization Wrappers *******************************************/

static void Enc28J60_InitMac_impl(Enc28J60 self, const uint8_t full_duplex, const uint8_t *mac_addr, const uint16_t maxFrameLen)
{
    const uint8_t ipg_delay_b2b[] = {0x12, 0x15}; /* HD value, FD value */
    const uint16_t ipg_delay_nb2b[] = {0x0C12, 0x0012}; /* HD value, FD value */

    Enc28J60_SetMacReceiveEnable(self, 1);
    Enc28J60_SetPaddingMode(self, 7); /* TODO make padding mode adjustable */
    Enc28J60_SetTransmitChecksumEnable(self, 1);
    Enc28J60_SetDuplexMode(self, full_duplex);
    if (full_duplex == 0)
    {
        Enc28J60_SetDeferTransmissionEnable(self, 1);
        Enc28J60_SetNoBackoffDuringBackpressureEnable(self, 1);
        Enc28J60_SetNoBackoffEnable(self, 1);
    }
    Enc28J60_SetFrameLengthCheckEnable(self, 1);
    Enc28J60_SetMaxFrameLen(self, maxFrameLen);
    Enc28J60_SetInterPacketGapDelayForBackToBack(self, ipg_delay_b2b[full_duplex]);
    Enc28J60_SetInterPacketGapDelayForNonBackToBack(self, ipg_delay_nb2b[full_duplex]);
    if (full_duplex == 0)
    {
        Enc28J60_SetMaxRetransmission(self, 0x0F);
        Enc28J60_SetCollisionWindow(self, 0x37);
    }
    Enc28J60_SetMacAddr(self, mac_addr);
}

void (*Enc28J60_InitMac)(Enc28J60, const uint8_t, const uint8_t *, const uint16_t) = Enc28J60_InitMac_impl;

void Enc28J60_Init(Enc28J60 self, const uint16_t rxBufSize, const uint8_t full_duplex, const uint8_t *mac_addr, const uint16_t maxFrameLen)
{
    Enc28J60_IO_Src(self->io);
    Enc28J60_SetRxStart(self, (uint16_t)(ENC28J60_RAM_END - rxBufSize));
    Enc28J60_SetRxEnd(self, ENC28J60_RAM_END);
    /* TODO Set receive filters */
    while (!Enc28J60_IsClockReady(self))
        ;
    Enc28J60_InitMac(self, full_duplex, mac_addr, maxFrameLen);
    Enc28J60_SetReceptionEnable(self, 1);
}

/* Ethernet Frame Transmission ***************************************/

void Enc28J60_Transmit(Enc28J60 self, const uint8_t *buf, const uint16_t len)
{
    uint16_t txStart = 0;
    uint16_t txEnd = (uint16_t)(txStart + 1 + len);

    Enc28J60_SetTxStart(self, txStart);
    /* FIXME write control byte before frame */
    Enc28J60_IO_Wbm(self->io, txStart, buf, len);
    Enc28J60_SetTxEnd(self, txEnd);
    Enc28J60_IO_Bfc(self->io, EIR, (1 << TXIF));
    Enc28J60_IO_Bfs(self->io, EIE, (1 << INTIE)|(1 << TXIE));
    Enc28J60_IO_Bfs(self->io, ECON1, (1 << TXRTS));
}
