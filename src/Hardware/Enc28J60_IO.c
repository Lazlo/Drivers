#include "Enc28J60_IO.h"
#include "Enc28J60_IOInternal.h"
#include "Enc28J60_Registers.h" /* only used by Enc28J60_IO_Bank() */
#include "SPI.h"
#include <stdlib.h>
#include <string.h>

typedef struct Enc28J60_IOStruct
{
	Pin csPin;
} Enc28J60_IOStruct;

Enc28J60_IO Enc28J60_IO_Create(const Pin csPin)
{
	Enc28J60_IO self = calloc(1, sizeof(Enc28J60_IOStruct));
	self->csPin = csPin;
	Pin_SetDirection(self->csPin, 1);
	Pin_Write(self->csPin, 1);
	return self;
}

void Enc28J60_IO_Destroy(Enc28J60_IO self)
{
	free(self);
}

void Enc28J60_IO_Select(Enc28J60_IO self, const uint8_t select)
{
	uint8_t lineLevel = 1;
	if (select)
		lineLevel = 0;
	Pin_Write(self->csPin, lineLevel);
}

static uint8_t Enc28J60_IO_Transfer(Enc28J60_IO self, const uint8_t *tx_data, const uint8_t tx_data_len, uint8_t *rx_data, const uint8_t rx_data_len)
{
	uint8_t i;
	Enc28J60_IO_Select(self, 1);
	for (i = 0; i < tx_data_len; i++) {
		if (rx_data_len)
			rx_data[i] = SPI_Trx(tx_data[i]);
		else
			SPI_Trx(tx_data[i]);
	}
	Enc28J60_IO_Select(self, 0);
	return (uint8_t)(1 + i);
}

static uint8_t Enc28J60_IO_Rcr_impl(Enc28J60_IO self, const uint8_t addr)
{
	uint8_t msg[] = {OP_RCR|(addr & ADDR_MASK), 0};
	uint8_t rx[2];
	Enc28J60_IO_Transfer(self, msg, 2, rx, 2);
	return rx[1];
}

uint8_t (*Enc28J60_IO_Rcr)(Enc28J60_IO, const uint8_t) = Enc28J60_IO_Rcr_impl;

static uint8_t Enc28J60_IO_Rmr_impl(Enc28J60_IO self, const uint8_t addr)
{
	uint8_t msg[] = {OP_RCR|(addr & ADDR_MASK), 0, 0};
	uint8_t rx[3];
	Enc28J60_IO_Transfer(self, msg, 3, rx, 3);
	return rx[2];
}

uint8_t (*Enc28J60_IO_Rmr)(Enc28J60_IO, const uint8_t) = Enc28J60_IO_Rmr_impl;

static uint16_t Enc28J60_IO_Rpr_impl(Enc28J60_IO self, const uint8_t addr)
{
	const uint8_t mask_phy_regAddr = 0x1F;
	uint16_t rv;
	/* Prepare PHY Register Read command */
	Enc28J60_IO_Bank(self, BANK2);
	Enc28J60_IO_Wcr(self, MIREGADR, addr & mask_phy_regAddr);
	Enc28J60_IO_Bfs(self, MICMD, (1 << MIIRD));
	/* Wait for operation to complete */
	Enc28J60_IO_Bank(self, BANK3);
	Enc28J60_IO_Rmr(self, MISTAT); /* TODO poll bit/loop */
	/* Clean-up PHY Register Read command */
	Enc28J60_IO_Bank(self, BANK2);
	Enc28J60_IO_Bfc(self, MICMD, (1 << MIIRD));
	/* Get return value of PHY Register Read command */
	rv = 0;
	rv = Enc28J60_IO_Rmr(self, MIRDL);
	rv |= (uint16_t)(Enc28J60_IO_Rmr(self, MIRDH) << 8);
	return rv;
}

uint16_t (*Enc28J60_IO_Rpr)(Enc28J60_IO, const uint8_t) = Enc28J60_IO_Rpr_impl;

static uint8_t Enc28J60_IO_Rbm_impl(Enc28J60_IO self, const uint8_t addr, uint8_t *buf, const uint16_t len)
{
	uint8_t cmd = OP_RBM|BUFFER_MEMORY_ARG;
	uint16_t i;
	Enc28J60_IO_Select(self, 1);
	SPI_Trx(cmd);
	SPI_Trx(addr);
	for (i = 0; i < len; i++) {
		buf[i] = SPI_Trx(0);
	}
	Enc28J60_IO_Select(self, 0);
	return 23;
}

uint8_t (*Enc28J60_IO_Rbm)(Enc28J60_IO, const uint8_t, uint8_t *, const uint16_t) = Enc28J60_IO_Rbm_impl;

static void Enc28J60_IO_Wcr_impl(Enc28J60_IO self, const uint8_t addr, const uint8_t value)
{
	uint8_t msg[] = {(uint8_t)(OP_WCR|(addr & ADDR_MASK)), value};
	Enc28J60_IO_Transfer(self, msg, 2, 0, 0);
}

void (*Enc28J60_IO_Wcr)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Wcr_impl;

void static Enc28J60_IO_Wpr_impl(Enc28J60_IO self, const uint8_t addr, const uint16_t value)
{
	const uint8_t mask_phy_regAddr = 0x1F;
	Enc28J60_IO_Bank(self, BANK2);
	Enc28J60_IO_Wcr(self, MIREGADR, addr & mask_phy_regAddr);
	Enc28J60_IO_Wcr(self, MIWRL, (uint8_t)(value & 0xFF));
	Enc28J60_IO_Wcr(self, MIWRH, (uint8_t)((value >> 8) & 0xFF));
}

void (*Enc28J60_IO_Wpr)(Enc28J60_IO, const uint8_t, const uint16_t) = Enc28J60_IO_Wpr_impl;

static void Enc28J60_IO_Wbm_impl(Enc28J60_IO self, const uint16_t addr, const uint8_t *buf, const uint16_t len)
{
	uint8_t cmd = OP_WBM|BUFFER_MEMORY_ARG;
	Enc28J60_IO_Select(self, 1);
	SPI_Trx(cmd);
	SPI_Trx(0); // addr);
	SPI_Trx(0);
	Enc28J60_IO_Select(self, 0);
}

void (*Enc28J60_IO_Wbm)(Enc28J60_IO, const uint16_t, const uint8_t *, const uint16_t) = Enc28J60_IO_Wbm_impl;

static void Enc28J60_IO_Bfs_impl(Enc28J60_IO self, const uint8_t addr, const uint8_t mask)
{
	uint8_t msg[] = {(uint8_t)(OP_BFS|(addr & ADDR_MASK)), mask};
	Enc28J60_IO_Transfer(self, msg, 2, 0, 0);
}

void (*Enc28J60_IO_Bfs)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Bfs_impl;

static void Enc28J60_IO_Bfc_impl(Enc28J60_IO self, const uint8_t addr, const uint8_t mask)
{
	uint8_t msg[] = {(uint8_t)(OP_BFC|(addr & ADDR_MASK)), mask};
	Enc28J60_IO_Transfer(self, msg, 2, 0, 0);
}

void (*Enc28J60_IO_Bfc)(Enc28J60_IO, const uint8_t, const uint8_t) = Enc28J60_IO_Bfc_impl;

static void Enc28J60_IO_Src_impl(Enc28J60_IO self)
{
	const uint8_t msg = OP_SRC|ADDR_MASK;
	Enc28J60_IO_Transfer(self, &msg, 1, 0, 0);
}

void (*Enc28J60_IO_Src)(Enc28J60_IO) = Enc28J60_IO_Src_impl;

static void Enc28J60_IO_Bank_impl(Enc28J60_IO self, const uint8_t bank)
{
	const uint8_t bank_mask = (1 << BSEL1)|(1 << BSEL0);
	if (bank > bank_mask)
		return;
	Enc28J60_IO_Bfc(self, ECON1, bank_mask);
	Enc28J60_IO_Bfs(self, ECON1, bank);
}

void (*Enc28J60_IO_Bank)(Enc28J60_IO, const uint8_t) = Enc28J60_IO_Bank_impl;
