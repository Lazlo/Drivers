/* TODO Add function to reset/update the watchdog (executes WDR instruction)
 * TODO Make it work for ATmega328P
 *  - add support for watchdog interrupt
 */

#include "Watchdog.h"
#include "WatchdogInternal.h"
#include "IO.h"

static const uint16_t wd_ps_m8[] = {16, 32, 64, 128, 256, 512, 1024, 2048};
static const uint16_t wd_ps_m328p[] = {2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};
static const uint16_t *wd_ps;
static uint8_t wd_ps_count;
static uint8_t wd_target;
static volatile uint8_t *wdReg_wdtcr;

void Watchdog_Create(const uint8_t target, volatile uint8_t *wdtcr)
{
    wd_target = target;
    wdReg_wdtcr = wdtcr;
    if (wd_target == 0) {
        wd_ps = wd_ps_m8;
        wd_ps_count = 8;
    } else {
        wd_ps = wd_ps_m328p;
        wd_ps_count = 10;
    }
}

void Watchdog_Destroy(void)
{
}

void Watchdog_Enable(const uint8_t enable)
{
    uint8_t tmp;
    tmp = IO_Read(wdReg_wdtcr);
    tmp |= (1 << WDCE);
    if (enable)
        tmp |= (1 << WDE);
    else
        tmp &= (uint8_t)~(1 << WDE);
    IO_Write(wdReg_wdtcr, tmp);
}

void Watchdog_SetPrescaler(const uint16_t ps)
{
    uint8_t max = wd_ps_count;
    uint8_t i;

    uint8_t tmp;
    tmp = IO_Read(wdReg_wdtcr);
    tmp &= (uint8_t)~(1 << WDCE); /* make sure not to write WDCE in 2nd step */
    tmp &= (uint8_t)~((1 << WDP2)|(1 << WDP1)|(1 << WDP0));

    if (wd_target == 1)
       tmp &= (uint8_t)~(1 << WDP3);

    for (i = 0; i < max; i++) {
        if (ps != wd_ps[i])
            continue;
        /* handle special case for when WDP3 needs to be set, but the
         * bit in the register is not consecutive to WDP[2:0]. */
        if (wd_target == 1 && (i & (1 << 3)))
            tmp |= (uint8_t)((1 << WDP3)|(i & 7));
        else
            tmp |= i;
        break;
    }
    if (i == max)
        return;
    /* Make sure to write the new prescaler within the next 4 clock cycles!
     * NOTE: Using IO_Write() will be too slow and hence not set the prescaler. */
    /* THIS WORKS! */
    *wdReg_wdtcr |= (1 << WDCE)|(1 << WDE);
    *wdReg_wdtcr = tmp;
}
