#include "AnalogComperator.h"
#include "Bits.h"

/* ATmega8 SFIOR register bits */
enum {ACME = 3};

/* ATmega8 ACSR register bits */
enum {ACD = 7, ACBG = 6, ACO = 5, ACI = 4, ACIE = 3, ACIC = 2, ACIS1 = 1, ACIS0 = 0};

static volatile uint8_t *acReg_sfior;
static volatile uint8_t *acReg_acsr;

void AnalogComperator_Create(volatile uint8_t *sfior, volatile uint8_t *acsr)
{
    acReg_sfior = sfior;
    acReg_acsr = acsr;
}

void AnalogComperator_Destroy(void)
{
}

void AnalogComperator_MultiplexerEnable(const uint8_t enable)
{
    SetBit(acReg_sfior, ACME, enable);
}

void AnalogComperator_Disable(const uint8_t disable)
{
    SetBit(acReg_acsr, ACD, disable);
}

void AnalogComperator_SetBandgapSelect(const uint8_t set)
{
    SetBit(acReg_acsr, ACBG, set);
}

uint8_t AnalogComperator_GetOutput(void)
{
    return BitIsSet(acReg_acsr, ACO);
}

uint8_t AnalogComperator_GetInterruptFlag(void)
{
    return BitIsSet(acReg_acsr, ACI);
}

void AnalogComperator_SetInterruptEnable(const uint8_t enable)
{
    SetBit(acReg_acsr, ACIE, enable);
}

void AnalogComperator_SetInputCaptureEnable(const uint8_t enable)
{
    SetBit(acReg_acsr, ACIC, enable);
}

void AnalogComperator_SetInterruptMode(const uint8_t mode)
{
    const uint8_t mask = (1 << ACIS1)|(1 << ACIS0);
    const uint8_t max = 3;
    if (mode == 1 || mode > max)
        return;
    UpdateBits(acReg_acsr, mask, mode);
}
