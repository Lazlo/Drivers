#include "SPI.h"
#include "SPIInternal.h"
#include "Bits.h"

typedef struct SPIRegStruct
{
	volatile uint8_t *spcr;
	volatile uint8_t *spsr;
	volatile uint8_t *spdr;
} SPIRegStruct;

static SPIRegStruct spiReg;

void SPI_Create(volatile uint8_t *spcr, volatile uint8_t *spsr, volatile uint8_t *spdr)
{
    spiReg.spcr = spcr;
    spiReg.spsr = spsr;
    spiReg.spdr = spdr;
}

void SPI_Destroy(void)
{
}

void SPI_SetInterruptEnable(const uint8_t enable)
{
    SetBit(spiReg.spcr, SPIE, enable);
}

void SPI_Enable(const uint8_t enable)
{
    SetBit(spiReg.spcr, SPE, enable);
}

void SPI_SetDataOrder(const uint8_t lsb_first)
{
    SetBit(spiReg.spcr, DORD, lsb_first);
}

void SPI_SetMasterMode(const uint8_t master_mode)
{
    SetBit(spiReg.spcr, MSTR, master_mode);
}

void SPI_SetClockPolarity(const uint8_t mode)
{
    SetBit(spiReg.spcr, CPOL, mode);
}

void SPI_SetClockPhase(const uint8_t mode)
{
    SetBit(spiReg.spcr, CPHA, mode);
}

/* NOTE: We didn't implement setting clock rate 64 when SPI2X is set. */
void SPI_SetClockRate(const uint8_t rate)
{
    const uint8_t rate_values[] = {4, 16, 64, 128, 2, 8, 32};
    const uint8_t max = 7;
    uint8_t mask_cr = (1 << SPR1)|(1 << SPR0);
    uint8_t mask_sr = (1 << SPI2X);
    uint8_t val_cr = 0;
    uint8_t val_sr = 0;
    uint8_t i;

    for (i = 0; i < max; i++) {
        if (rate != rate_values[i])
            continue;
        val_cr = i & mask_cr;
        if (i & (1 << 2))
            val_sr = mask_sr;
        break;
    }
    if (i == max)
        return;
    UpdateBits(spiReg.spcr, mask_cr, val_cr);
    UpdateBits(spiReg.spsr, mask_sr, val_sr);
}

static uint8_t SPI_GetInterruptFlag_impl(void)
{
    return BitIsSet(spiReg.spsr, SPIF);
}

uint8_t (*SPI_GetInterruptFlag)(void) = SPI_GetInterruptFlag_impl;

uint8_t SPI_GetWriteCollisionFlag(void)
{
    return BitIsSet(spiReg.spsr, WCOL);
}

static uint8_t SPI_Read_impl(void)
{
    return *spiReg.spdr;
}

uint8_t (*SPI_Read)(void) = SPI_Read_impl;

static void SPI_Write_impl(const uint8_t byte)
{
    *spiReg.spdr = byte;
}

void (*SPI_Write)(const uint8_t byte) = SPI_Write_impl;

static uint8_t SPI_Trx_impl(const uint8_t byte)
{
    SPI_Write(byte);
    while (!SPI_GetInterruptFlag())
        ;
    return SPI_Read();
}

uint8_t (*SPI_Trx)(const uint8_t byte) = SPI_Trx_impl;
