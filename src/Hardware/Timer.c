/* TODO allow register overflow interrupt function pointer */
/* TODO allow register compare match interrupt function pointer */
/* TODO prescaler bits not accounted for yet: PSR10 in SFIOR */
/* TODO timer1 bits not accounted for yet: FOC1A(3) and FOC1B(2) in TCCR1A */
/* TODO timer1 bits not accounted for yet: ICNC1(7) and ICES(6) in TCCR1B */
/* TODO timer2 bits not accounted for yet: FOC2(7) in TCCR2 */
/* TODO timer2 bits not accounted for yet: AS2(3), TCN2UB(2), OCR2UB(1) and TCR2UB(0) in ASSR */

#include "Timer.h"
#include "Bits.h"

/* Used by assignFeatures() via Timer_Create() */
enum {WGM_UNIT = 0, OC_UNIT = 1, DUAL_OC_UNIT = 2, IC_UNIT = 3, IS_16BIT = 4};

/* ATmega8 Clock Selection bits for Timers 0, 1 and 2
 * timer0 CS02 CS01 CS00 in TCCR0  (offset in register: 2, 1, 0)
 * timer1 CS12 CS11 CS10 in TCCR1B (offset in register: 2, 1, 0)
 * timer2 CS22 CS21 CS20 in TCCR2  (offset in register: 2, 1, 0)
 *
 * Used by Timer_SetClockSource()
 */
enum { CS0 = 0, CS1 = 1, CS2 = 2};

/* ATmega8 Wave Generation Mode bits for Timer 0, 1 and 2
 * timer0 n/a
 * timer1 WGM13 WGM12 in TCCR1B (offset in register: 4, 3)
 * timer1 WGM11 WGM10 in TCCR1A (offset in register: 1, 0)
 * timer2 WGM20 WGM21 in TCCR2  (offset in reigster: 6, 3)
 *
 * Used by TimerSetWaveGenerationMode()
 */
enum {WGM10 = 0, WGM11 = 1, WGM12 = 3, WGM13 = 4};
enum { WGM1 = 3, WGM0 = 6};

/* ATmega8 Overflow Interrupt Enable bits for Timers 0, 1 and 2
 * timer0 TOIE0 in TIMSK (offset in register: 0)
 * timer1 TOIE1 in TIMSK (offset in reigster: 2)
 * timer2 TOIE2 in TIMSK (offset in reigster: 6)
 *
 * Used by Timer_SetOverflowInterrupt()
 */
enum {TOIE0 = 0, TOIE1 = 2, TOIE2 = 6};

/* ATmega8 Compare Match Interrupt Enable bits for Timer 0, 1 and 2
 * timer0 n/a
 * timer1 OCIE1A in TIMSK (offset in register: 4)
 * timer1 OCIE1B in TIMSK (offset in register: 3)
 * timer2 OCIE2 in  TIMSK (offset in register: 7)
 *
 * Used by Timer_SetCompareMatchInterrupt()
 */
enum {OCIE1A = 4, OCIE1B = 3, OCIE2 = 7};

/* Used by Timer_SetInputCaptureInterrupt() */
enum {TICIE1 = 5};

/* ATmega8 Overflow Interrupt flags for Timer 0, 1 and 2
 * timer0 TOV0 in TIFR (offset in register: 0)
 * timer1 TOV1 in TIFR (offset in register: 2)
 * timer2 TOV2 in TIFR (offset in register: 6)
 *
 * Used by Timer_GetOverflowInterruptFlag() and Timer_ClearOverflowInterruptFlag()
 */
enum {TOV0 = 0, TOV1 = 2, TOV2 = 6};

/* Used by Timer_GetCompareMatchInterruptFlag() and Timer_ClearCompareMatchInterruptFlag() */
enum {OCF1A = 4, OCF1B = 3, OCF2 = 7};

/* Used by Timer_GetInputCaptureInterruptFlag() and Timer_ClearInputCaptureInterruptFlag() */
enum {ICF1 = 5};

typedef struct TimerRegStruct
{
    volatile uint8_t *tccr;
    volatile uint8_t *tccrb;
    volatile uint8_t *tcnt;
    volatile uint8_t *tcnth;
    volatile uint8_t *ocr;
    volatile uint8_t *ocrh;
    volatile uint8_t *ocrb;
    volatile uint8_t *ocrbh;
    volatile uint8_t *icr;
    volatile uint8_t *icrh;
    volatile uint8_t *timsk;
    volatile uint8_t *tifr;
} TimerRegStruct;

typedef struct TimerStruct
{
    uint8_t id;
    uint8_t features;
    TimerRegStruct reg;
} TimerStruct;

#define TIMERS_MAX 3

static TimerStruct timer[TIMERS_MAX];
static uint8_t timerTarget;
static void assignFeatures(Timer self)
{
     switch(self->id)
     {
     case 1:
         self->features = (1 << WGM_UNIT)|(1 << OC_UNIT)|(1 << DUAL_OC_UNIT)|(1 << IC_UNIT)|(1 << IS_16BIT);
         break;
     case 2:
         self->features = (1 << WGM_UNIT)|(1 << OC_UNIT);
         break;
     default:
         self->features = 0;
     }
}

static void assignRegisters(Timer self, volatile uint8_t *tccr, volatile uint8_t *tccrb,
                        volatile uint8_t *tcnt, volatile uint8_t *tcnth,
                        volatile uint8_t *ocr, volatile uint8_t *ocrh,
                        volatile uint8_t *ocrb, volatile uint8_t *ocrbh,
                        volatile uint8_t *icr, volatile uint8_t *icrh,
                        volatile uint8_t *timsk, volatile uint8_t *tifr)
{
     self->reg.tccr = tccr;
     self->reg.tccrb = tccrb;
     self->reg.tcnt = tcnt;
     self->reg.tcnth = tcnth;
     self->reg.ocr = ocr;
     self->reg.ocrh = ocrh;
     self->reg.ocrb = ocrb;
     self->reg.ocrbh = ocrbh;
     self->reg.icr = icr;
     self->reg.icrh = icrh;
     self->reg.timsk = timsk;
     self->reg.tifr = tifr;
}

Timer Timer_Create(const uint8_t target, const uint8_t id, volatile uint8_t *tccr, volatile uint8_t *tccrb,
                        volatile uint8_t *tcnt, volatile uint8_t *tcnth,
                        volatile uint8_t *ocr, volatile uint8_t *ocrh,
                        volatile uint8_t *ocrb, volatile uint8_t *ocrbh,
                        volatile uint8_t *icr, volatile uint8_t *icrh,
                        volatile uint8_t *timsk, volatile uint8_t *tifr)
{
     timerTarget = target;
     Timer self = 0;
     if (id > TIMERS_MAX - 1)
         return self;
     self = &timer[id];
     self->id = id;
     assignFeatures(self);
     assignRegisters(self, tccr, tccrb,
                        tcnt, tcnth,
                        ocr, ocrh,
                        ocrb, ocrbh,
                        icr, icrh,
                        timsk, tifr);
     return self;
}

void Timer_Destroy(Timer self)
{
}


uint8_t Timer_GetId(Timer self)
{
    return self->id;
}

#define TIMER_HAS_FEATURE(t, f) BitIsSet(&t->features, f)

uint8_t Timer_Is16Bit(Timer self)
{
    return TIMER_HAS_FEATURE(self, IS_16BIT);
}

uint8_t Timer_HasWaveGenerationUnit(Timer self)
{
    return TIMER_HAS_FEATURE(self, WGM_UNIT);
}

uint8_t Timer_HasCompareMatchUnit(Timer self)
{
    return TIMER_HAS_FEATURE(self, OC_UNIT);
}

uint8_t Timer_HasDualCompareMatchUnit(Timer self)
{
    return TIMER_HAS_FEATURE(self, DUAL_OC_UNIT);
}

uint8_t Timer_HasInputCaptureUnit(Timer self)
{
    return TIMER_HAS_FEATURE(self, IC_UNIT);
}

void Timer_SetClockSource(Timer self, const uint8_t cs)
{
    const uint8_t cs_max = (1 << CS2)|(1 << CS1)|(1 << CS0);
    const uint8_t offset = 0;
    if (cs > (cs_max >> offset))
        return;
    UpdateBits(self->reg.tccrb, cs_max, (uint8_t)(cs << offset));
}

void (*fp_Timer_SetClockSource)(Timer, const uint8_t) = Timer_SetClockSource;

void Timer_SetPrescaler(Timer self, const uint16_t ps)
{
	uint8_t cs = 0;

	if (ps == 1)					cs = 1;
	else if (ps == 8)				cs = 2;
	else
	{
		if (self->id == 0 || self->id == 1)
		{
			if (ps == 64)			cs = 3;
			else if (ps == 256)		cs = 4;
			else if (ps == 1024)	cs = 5;
		}
		else if (self->id == 2)
		{
			if (ps == 32)			cs = 3;
			else if (ps == 64)		cs = 4;
			else if (ps == 128)		cs = 5;
			else if (ps == 256)		cs = 6;
			else if (ps == 1024)	cs = 7;
		}
	}
	(fp_Timer_SetClockSource)(self, cs);
}

void Timer_SetCompareMode(Timer self, const uint8_t com, const uint8_t unit)
{
    /* ATmega8 Compare Mode bits for Timers 0, 1 and 2
     * timer0 n/a
     * timer1 COM1A1 COM1A0 in TCCR1A (offset in register: 7, 6)
     * timer1 COM1B1 COM1B0 in TCCR1A (offset in register: 5, 4)
     * timer2 COM21  COM20  in TCCR2  (offset in register: 5, 4)
     */
    const uint8_t com_max = 3;
    uint8_t offset;
    if (!Timer_HasCompareMatchUnit(self))
        return;
    if (com > com_max)
        return;
    if (self->id == 1 && unit == 0)
        offset = 6;
    else
        offset = 4;
    UpdateBits(self->reg.tccr, (uint8_t)(com_max << offset), (uint8_t)(com << offset));
}

void Timer_SetWaveGenerationMode(Timer self, const uint8_t wgm)
{
    uint8_t wgm_max;
    uint8_t real_wgm = 0;
    uint8_t real_wgmb = 0;
    uint8_t mask;
    if (!Timer_HasWaveGenerationUnit(self))
        return;
    else if (self->id == 1)
        wgm_max = 15;
    else
        wgm_max = 3;
    if (wgm > wgm_max)
        return;
    switch(self->id)
    {
    case 1:
        if (wgm & (1 << 0))
            real_wgm |= (1 << WGM10);
        if (wgm & (1 << 1))
            real_wgm |= (1 << WGM11);
        if (wgm & (1 << 2))
            real_wgmb |= (1 << WGM12);
        if (wgm & (1 << 3))
            real_wgmb |= (1 << WGM13);
        mask = (1 << WGM11)|(1 << WGM10);
        UpdateBits(self->reg.tccr, mask, real_wgm & mask);
        mask = (1 << WGM13)|(1 << WGM12);
        UpdateBits(self->reg.tccrb, mask, real_wgmb & mask);
        break;
    case 2:
        /* translate consecutive wgm argument to non-consecutive wgm bit mask */
        if (wgm & (1 << 0))
            real_wgm |= (1 << WGM0);
        if (wgm & (1 << 1))
            real_wgm |= (1 << WGM1);
        mask = (1 << WGM1)|(1 << WGM0);
        UpdateBits(self->reg.tccr, mask, real_wgm);
        break;
    default:
        return;
    }
}

void Timer_SetCounter(Timer self, const uint16_t cnt)
{
    /* ATmega8 Counter Registers for Timer 0, 1, and 2
     * timer0 TCNT0
     * timer1 TCNT1H TCNT1L
     * timer2 TCNT2
     */
    *self->reg.tcnt = (uint8_t)cnt;
    if (Timer_Is16Bit(self))
        *self->reg.tcnth = (uint8_t)(cnt >> 8);
}

void Timer_SetCompareMatch(Timer self, const uint16_t cnt, const uint8_t unit)
{
    /* ATmega8 Compare Match registers for Timer 0, 1 and 2
     * timer0 n/a
     * timer1 OCR1AH OCR1AL
     * timer1 OCR1BH OCR1BL
     * timer2 OCR2
     */
    volatile uint8_t *reg;
    volatile uint8_t *regh;
    if (!Timer_HasCompareMatchUnit(self))
        return;
    if (self->id == 1 && unit == 0)
    {
        reg = self->reg.ocr;
        regh = self->reg.ocrh;
    }
    else if (self->id == 1 && unit == 1)
    {
        reg = self->reg.ocrb;
        regh = self->reg.ocrbh;
    }
    else if (self->id == 2)
    {
        reg = self->reg.ocr;
    }

    *reg = (uint8_t)cnt;
    if (Timer_Is16Bit(self))
        *regh = (uint8_t)(cnt >> 8);
}

uint16_t Timer_GetInputCaptureValue(Timer self)
{
    uint16_t val = 0;
    if (!Timer_HasInputCaptureUnit(self))
        return 0;
    val = *self->reg.icr;
    val |= (uint16_t)(*self->reg.icrh << 8);
    return val;
}

void Timer_SetInputCaptureValue(Timer self, const uint16_t val)
{
    if (!Timer_HasInputCaptureUnit(self))
        return;
    *self->reg.icr = (uint8_t)val;
    *self->reg.icrh = (uint8_t)(val >> 8);
}

void Timer_SetOverflowInterrupt(Timer self, const uint8_t enable)
{
    const uint8_t bit_position[] = {TOIE0, TOIE1, TOIE2};
    uint8_t offset;
    if (enable > 1)
        return;
    if (timerTarget == 0)   offset = bit_position[self->id]; /* ATmega8 */
    else                    offset = 0;                      /* ATmega328p */
    UpdateBits(self->reg.timsk, (uint8_t)(1 << offset), (uint8_t)(enable << offset));
}

void Timer_SetCompareMatchInterrupt(Timer self, const uint8_t enable, const uint8_t unit)
{
    uint8_t offset;
    if (!Timer_HasCompareMatchUnit(self))
        return;
    if (self->id == 1 && unit == 0)
        offset = OCIE1A;
    if (self->id == 1 && unit == 1)
        offset = OCIE1B;
    if (self->id == 2)
        offset = OCIE2;
    UpdateBits(self->reg.timsk, (uint8_t)(1 << offset), (uint8_t)(enable << offset));
}

void Timer_SetInputCaptureInterrupt(Timer self, const uint8_t enable)
{
    uint8_t offset = TICIE1;
    if (!Timer_HasInputCaptureUnit(self))
        return;
    UpdateBits(self->reg.timsk, (uint8_t)(1 << offset), (uint8_t)(enable << offset));
}

uint8_t Timer_GetOverflowInterruptFlag(Timer self)
{
    uint8_t offset;
    if (self->id == 0)
        offset = TOV0;
    if (self->id == 1)
        offset = TOV1;
    if (self->id == 2)
        offset = TOV2;
    return BitIsSet(self->reg.tifr, offset);
}

uint8_t Timer_GetCompareMatchInterruptFlag(Timer self, const uint8_t unit)
{
    uint8_t offset;
    if (!Timer_HasCompareMatchUnit(self))
        return 0;
    if (self->id == 1 && unit == 0)
        offset = OCF1A;
    if (self->id == 1 && unit == 1)
        offset = OCF1B;
    if (self->id == 2)
        offset = OCF2;
    return BitIsSet(self->reg.tifr, offset);
}

uint8_t Timer_GetInputCaptureInterruptFlag(Timer self)
{
    uint8_t offset = ICF1;
    if (!Timer_HasInputCaptureUnit(self))
        return 0;
    return BitIsSet(self->reg.tifr, offset);
}

void Timer_ClearOverflowInterruptFlag(Timer self)
{
    uint8_t offset;
    if (self->id == 0)
        offset = TOV0;
    if (self->id == 1)
        offset = TOV1;
    if (self->id == 2)
        offset = TOV2;
    SetBit(self->reg.tifr, offset, 1); /* write 1 to clear bit */
}

void Timer_ClearCompareMatchInterruptFlag(Timer self, const uint8_t unit)
{
    uint8_t offset;
    if (!Timer_HasCompareMatchUnit(self))
        return;
    if (self->id == 1 && unit == 0)
        offset = OCF1A;
    if (self->id == 1 && unit == 1)
        offset = OCF1B;
    if (self->id == 2)
        offset = OCF2;
    SetBit(self->reg.tifr, offset, 1); /* write 1 to clear bit */
}

void Timer_ClearInputCaptureInterruptFlag(Timer self)
{
    uint8_t offset = ICF1;
    if (!Timer_HasInputCaptureUnit(self))
        return;
    SetBit(self->reg.tifr, offset, 1); /* write 1 to clear bit */
}
