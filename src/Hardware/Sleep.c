#include "Sleep.h"
#include "Bits.h"

static uint8_t pmTarget;
static volatile uint8_t *pmReg_mcucr;
static uint8_t pmSeBitOffset;
static uint8_t pmModeOffset;
static uint8_t pmModeMax;

void Sleep_Create(const uint8_t target, volatile uint8_t *mcucr)
{
    pmTarget = target;
    pmReg_mcucr = mcucr;
    if (pmTarget == 0)
    {
        pmSeBitOffset = 7; /* ATmega8 */
        pmModeOffset = 4;
        pmModeMax = 6;
    }
    else
    {
        pmSeBitOffset = 0; /* ATmega328P */
        pmModeOffset = 1;
        pmModeMax = 7;
    }
}

void Sleep_Destroy(void)
{
}

void Sleep_SetEnable(const uint8_t enable)
{
    SetBit(pmReg_mcucr, pmSeBitOffset, enable);
}

void Sleep_SetMode(const uint8_t mode)
{
    const uint8_t mask = (1 << 2)|(1 << 1)|(1 << 0);
    uint8_t offset = pmModeOffset;
    uint8_t max = pmModeMax;
    if (mode > max)
        return;
    /* ignore reserved modes */
    if (mode == 4 || mode == 5)
        return;
    UpdateBits(pmReg_mcucr, (uint8_t)(mask << offset), (uint8_t)(mode << offset));
}
