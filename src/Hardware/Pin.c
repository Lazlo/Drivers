#include "Pin.h"
#include "Bits.h"
#include <stdlib.h>
#include <string.h>

typedef struct PinStruct
{
    uint8_t offset;
    volatile uint8_t *dir;
    volatile uint8_t *out;
    volatile uint8_t *in;
} PinStruct;

Pin Pin_Create(const uint8_t offset, volatile uint8_t *dirreg, volatile uint8_t *outreg, volatile uint8_t *inreg)
{
     Pin self = calloc(1, sizeof(PinStruct));
     self->offset = offset;
     self->dir = dirreg;
     self->out = outreg;
     self->in = inreg;
     return self;
}

void Pin_Destroy(Pin self)
{
    free(self);
}

uint8_t Pin_GetOffset(Pin self)
{
    return self->offset;
}

uint8_t Pin_GetDirection(Pin self)
{
    return BitIsSet(self->dir, self->offset);
}

static void Pin_SetDirection_impl(Pin self, const uint8_t dir)
{
    SetBit(self->dir, self->offset, dir);
}

void (*Pin_SetDirection)(Pin, const uint8_t) = Pin_SetDirection_impl;

uint8_t Pin_GetPullUpState(Pin self)
{
    return BitIsSet(self->out, self->offset);
}

void Pin_SetPullUpState(Pin self, const uint8_t enable)
{
    SetBit(self->out, self->offset, enable);
}

static void Pin_Write_impl(Pin self, const uint8_t level)
{
    SetBit(self->out, self->offset, level);
}

void (*Pin_Write)(Pin, const uint8_t) = Pin_Write_impl;

uint8_t Pin_Read(Pin self)
{
    return BitIsSet(self->in, self->offset);
}
