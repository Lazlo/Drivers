#ifndef D_Watchdog_H
#define D_Watchdog_H

/**********************************************************
 *
 * Watchdog is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void Watchdog_Create(const uint8_t target, volatile uint8_t *wdtcr);
void Watchdog_Destroy(void);


void Watchdog_Enable(const uint8_t enable);
void Watchdog_SetPrescaler(const uint16_t ps);

#endif  /* D_FakeWatchdog_H */
