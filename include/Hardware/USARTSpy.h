#ifndef D_USARTSpy_H
#define D_USARTSpy_H

/**********************************************************
 *
 * USARTSpy is responsible for ...
 *
 **********************************************************/

#include "USART.h"

void USARTSpy_Create(void);
void USARTSpy_Destroy(void);

void USARTSpy_DataRegisterEmpty_SetReturnTrueOnCall(const uint8_t nthCall);
uint8_t USARTSpy_DataRegisterEmpty_GetCallsMade(void);
uint8_t USARTSpy_DataRegisterEmpty(USART);
uint8_t USARTSpy_Putc_GetCallsMade(void);
void USARTSpy_Putc(USART, const char);
void USARTSpy_RxComplete_SetReturnTrueOnCall(const uint8_t nthCall);
uint8_t USARTSpy_RxComplete_GetCallsMade(void);
uint8_t USARTSpy_RxComplete(USART);

/* Needed to make this one extern to allow testing its setter function */
extern uint8_t USARTSpy_DataRegisterEmpty_ReturnTrueOnCall;
extern uint8_t USARTSpy_RxComplete_ReturnTrueOnCall;

#endif  /* D_FakeUSARTSpy_H */
