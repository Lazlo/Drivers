#ifndef D_ADC_H
#define D_ADC_H

/**********************************************************
 *
 * ADC is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void ADC_Create(volatile uint8_t *admux, volatile uint8_t *adcsra, volatile uint8_t *adch, volatile uint8_t *adcl);
void ADC_Destroy(void);

void ADC_SetReferenceVoltage(const uint8_t ref);
void ADC_SetLeftAdjustResult(const uint8_t enable);
void ADC_SetChannel(const uint8_t channel);
void ADC_Enable(const uint8_t enable);
void ADC_StartConversion(void);
uint8_t ADC_IsConversionComplete(void);
void ADC_SetFreeRunning(const uint8_t enable);
uint8_t ADC_GetInterruptFlag(void);
void ADC_SetInterruptEnable(const uint8_t enable);
void ADC_SetPrescaler(const uint8_t prescaler);
uint16_t ADC_GetResult(void);

extern uint8_t (*adc_fp_IO_Read)(const volatile uint8_t *);

#endif  /* D_FakeADC_H */
