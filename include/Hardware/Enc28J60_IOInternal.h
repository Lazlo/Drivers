#pragma once

enum {
    OP_RCR = 0,
    OP_RBM = (1 << 5),
    OP_WCR = (1 << 6),
    OP_WBM = (1 << 6)|(1 << 5),
    OP_BFS = (1 << 7),
    OP_BFC = (1 << 7)|(1 << 5),
    OP_SRC = (1 << 7)|(1 << 6)|(1 << 5),
};

enum {ADDR_MASK = 0x1F};
enum {BUFFER_MEMORY_ARG = (1 << 4)|(1 << 3)|(1 << 1)};
