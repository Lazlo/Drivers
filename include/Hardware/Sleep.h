#ifndef D_Sleep_H
#define D_Sleep_H

/**********************************************************
 *
 * Sleep is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

enum {
	SLEEP_MODE_IDLE = 0,
	SLEEP_MODE_ADC,
	SLEEP_MODE_PWRDOWN,
	SLEEP_MODE_PWRSAVE,
	SLEEP_MODE_RESERVED4,
	SLEEP_MODE_RESERVED5,
	SLEEP_MODE_STANDBY,
	SLEEP_MODE_EXTSTANDBY
};

void Sleep_Create(const uint8_t target, volatile uint8_t *mcucr);
void Sleep_Destroy(void);

void Sleep_SetEnable(const uint8_t enable);
void Sleep_SetMode(const uint8_t mode);

#endif  /* D_FakeSleep_H */
