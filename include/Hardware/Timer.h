#ifndef D_Timer_H
#define D_Timer_H

/**********************************************************************
 *
 * Timer is responsible for ...
 *
 **********************************************************************/

#include <stdint.h>

typedef struct TimerStruct * Timer;

Timer Timer_Create(const uint8_t target, const uint8_t id, volatile uint8_t *tccr, volatile uint8_t *tccrb,
                        volatile uint8_t *tcnt, volatile uint8_t *tcnth,
                        volatile uint8_t *ocr, volatile uint8_t *ocrh,
                        volatile uint8_t *ocrb, volatile uint8_t *ocrbh,
                        volatile uint8_t *icr, volatile uint8_t *icrh,
                        volatile uint8_t *timsk, volatile uint8_t *tifr);
void Timer_Destroy(Timer);

uint8_t Timer_GetId(Timer);
uint8_t Timer_Is16Bit(Timer);
uint8_t Timer_HasWaveGenerationUnit(Timer);
uint8_t Timer_HasCompareMatchUnit(Timer);
uint8_t Timer_HasDualCompareMatchUnit(Timer);
uint8_t Timer_HasInputCaptureUnit(Timer);
void Timer_SetClockSource(Timer, const uint8_t cs);
void Timer_SetPrescaler(Timer, const uint16_t ps);
void Timer_SetCompareMode(Timer, const uint8_t com, const uint8_t unit);
void Timer_SetWaveGenerationMode(Timer, const uint8_t wgm);
void Timer_SetCounter(Timer, const uint16_t cnt);
void Timer_SetCompareMatch(Timer, const uint16_t cnt, const uint8_t unit);
uint16_t Timer_GetInputCaptureValue(Timer);
void Timer_SetInputCaptureValue(Timer, const uint16_t val);
void Timer_SetOverflowInterrupt(Timer, const uint8_t enable);
void Timer_SetCompareMatchInterrupt(Timer, const uint8_t enable, const uint8_t unit);
void Timer_SetInputCaptureInterrupt(Timer, const uint8_t enable);
uint8_t Timer_GetOverflowInterruptFlag(Timer);
uint8_t Timer_GetCompareMatchInterruptFlag(Timer, const uint8_t unit);
uint8_t Timer_GetInputCaptureInterruptFlag(Timer);
void Timer_ClearOverflowInterruptFlag(Timer);
void Timer_ClearCompareMatchInterruptFlag(Timer, const uint8_t unit);
void Timer_ClearInputCaptureInterruptFlag(Timer);

extern void (*fp_Timer_SetClockSource)(Timer, const uint8_t);

#endif  /* D_FakeTimer_H */
