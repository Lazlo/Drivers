#ifndef D_WatchdogInternal_H
#define D_WatchdogInternal_H

/**********************************************************
 *
 * WatchdogInternal is responsible for ...
 *
 **********************************************************/

/* ATmega8 WDTCR register bits */
enum {WDCE = 4, WDE = 3, WDP2 = 2, WDP1 = 1, WDP0 = 0};
/* ATmega328P WDTCSR register bits that extend the ones of ATmega8 */
enum {WDP3 = 5};

#endif  /* D_FakeWatchdogInternal_H */
