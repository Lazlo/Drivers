#pragma once

/* ATmega8 SPCR register bits */

enum {SPIE = 7, SPE = 6, DORD = 5, MSTR = 4, CPOL = 3, CPHA = 2, SPR1 = 1, SPR0 = 0};

/* ATmega8 SPSR register bits */

enum {SPIF = 7, WCOL = 6, SPI2X = 0};
