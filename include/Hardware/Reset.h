#ifndef D_Reset_H
#define D_Reset_H

/**********************************************************
 *
 * Reset is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void Reset_Create(volatile uint8_t *mcucr);
void Reset_Destroy(void);

uint8_t Reset_WasCausedByWatchdog(void);
uint8_t Reset_WasCausedByBrownOut(void);
uint8_t Reset_WasCausedByExternalReset(void);
uint8_t Reset_WasCausedByPowerOnReset(void);

#endif  /* D_FakeReset_H */
