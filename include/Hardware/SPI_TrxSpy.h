#ifndef D_SPI_TrxSpy_H
#define D_SPI_TrxSpy_H

/**********************************************************
 *
 * SPI_TrxSpy is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void SPI_TrxSpy_Create(const uint8_t bytesToRecord);
void SPI_TrxSpy_Destroy(void);

uint8_t SPI_TrxSpy(const uint8_t byte);
uint8_t SPI_TrxSpy_GetLastByteWritten(void);
uint8_t SPI_TrxSpy_GetBytesWrittenCount(void);
uint8_t SPI_TrxSpy_GetBytesWrittenReturned(void);
uint8_t SPI_TrxSpy_GetByteWritten(void);
void SPI_TrxSpy_SetReturnByte(const uint8_t byte);

#endif  /* D_FakeSPI_TrxSpy_H */
