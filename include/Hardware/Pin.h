#ifndef D_Pin_H
#define D_Pin_H

/**********************************************************************
 *
 * Pin is responsible for ...
 *
 **********************************************************************/

#include <stdint.h>

typedef struct PinStruct * Pin;

Pin Pin_Create(const uint8_t offset, volatile uint8_t *dirreg, volatile uint8_t *outreg, volatile uint8_t *inreg);
void Pin_Destroy(Pin);

uint8_t Pin_GetOffset(Pin);
uint8_t Pin_GetDirection(Pin);
extern void (*Pin_SetDirection)(Pin, const uint8_t dir);
uint8_t Pin_GetPullUpState(Pin);
void Pin_SetPullUpState(Pin, const uint8_t enable);
extern void (*Pin_Write)(Pin, const uint8_t level);
uint8_t Pin_Read(Pin);

#endif  /* D_FakePin_H */
