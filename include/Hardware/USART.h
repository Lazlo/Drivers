#ifndef D_USART_H
#define D_USART_H

/**********************************************************************
 *
 * USART is responsible for ...
 *
 **********************************************************************/

#include <stdint.h>

typedef struct USARTStruct * USART;

USART USART_Create(volatile uint8_t *ucsra, volatile uint8_t *ucsrb, volatile uint8_t *ucsrc,
                        volatile uint8_t *ubrrl, volatile uint8_t *ubrrh, volatile uint8_t *udr);
void USART_Destroy(USART);

void USART_SetBaudRate(USART, const uint32_t baud, const uint32_t f_cpu);
void USART_SetMode(USART, const uint8_t sync);
void USART_SetParity(USART, const uint8_t mode);
void USART_SetStopBits(USART, const uint8_t bits);
void USART_SetCharacterSize(USART, const uint8_t bits);
void USART_SetClockPolarity(USART, const uint8_t mode);
void USART_SetRxCompleteInterruptEnable(USART, const uint8_t enable);
void USART_SetTxCompleteInterruptEnable(USART, const uint8_t enable);
void USART_SetDataRegisterEmptyInterruptEnable(USART, const uint8_t enable);
void USART_Enable(USART, const uint8_t enable);
void USART_Write(USART, const uint8_t byte);
uint8_t USART_Read(USART);
uint8_t USART_RxComplete(USART);
uint8_t USART_DataRegisterEmpty(USART);
uint8_t USART_HasFrameError(USART);
uint8_t USART_HasDataOverrun(USART);
uint8_t USART_HasParityError(USART);
void USART_Putc(USART, const char c);
void USART_Puts(USART, const char *s);
char USART_Getc(USART);

extern uint8_t (*fp_USART_DataRegisterEmpty)(USART);
extern void (*fp_USART_Putc)(USART, const char c);
extern uint8_t (*fp_USART_RxComplete)(USART);

#endif  /* D_FakeUSART_H */
