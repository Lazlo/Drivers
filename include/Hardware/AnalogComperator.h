#ifndef D_AnalogComperator_H
#define D_AnalogComperator_H

/**********************************************************
 *
 * AnalogComperator is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void AnalogComperator_Create(volatile uint8_t *sfior, volatile uint8_t *acsr);
void AnalogComperator_Destroy(void);

void AnalogComperator_MultiplexerEnable(const uint8_t enable);
void AnalogComperator_Disable(const uint8_t disable);
void AnalogComperator_SetBandgapSelect(const uint8_t set);
uint8_t AnalogComperator_GetOutput(void);
uint8_t AnalogComperator_GetInterruptFlag(void);
void AnalogComperator_SetInterruptEnable(const uint8_t enable);
void AnalogComperator_SetInputCaptureEnable(const uint8_t enable);
void AnalogComperator_SetInterruptMode(const uint8_t mode);

#endif  /* D_FakeAnalogComperator_H */
