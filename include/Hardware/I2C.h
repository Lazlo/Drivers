#ifndef D_I2C_H
#define D_I2C_H

/**********************************************************
 *
 * I2C is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void I2C_Create(volatile uint8_t *twbr, volatile uint8_t *twcr, volatile uint8_t *twsr, volatile uint8_t *twdr, volatile uint8_t *twar);
void I2C_Destroy(void);

void I2C_SetSlaveAddress(const uint8_t sla);
void I2C_SetGeneralCallEnable(const uint8_t enable);
uint8_t I2C_GetInterruptFlag(void);
void I2C_ClearInterruptFlag(void);
void I2C_SetAcknowledgeEnable(const uint8_t enable);
void I2C_Start(void);
void I2C_Stop(void);
uint8_t I2C_GetWriteCollisionFlag(void);
void I2C_Enable(const uint8_t enable);
void I2C_SetInterruptEnable(const uint8_t enable);
void I2C_SetPrescaler(const uint8_t prescaler);
uint8_t I2C_GetStatus(void);
uint8_t I2C_Read(void);
void I2C_Write(const uint8_t byte);
void I2C_SetClockFrequency(const uint16_t freq_khz, const uint32_t f_cpu);

#endif  /* D_FakeI2C_H */
