#ifndef D_Enc28J60_H
#define D_Enc28J60_H

/**********************************************************************
 *
 * Enc28J60 is responsible for ...
 *
 **********************************************************************/

#include "Pin.h"
#include <stdint.h>

enum {ENC28J60_RAM_SIZE = 8192};
enum {ENC28J60_RAM_END = ENC28J60_RAM_SIZE - 1};

typedef struct Enc28J60Struct * Enc28J60;

Enc28J60 Enc28J60_Create(const Pin csPin);
void Enc28J60_Destroy(Enc28J60);

/* Buffer Configuration */

extern void (*Enc28J60_SetRxStart)(Enc28J60 self, const uint16_t addr);
uint16_t Enc28J60_GetRxStart(Enc28J60 self);
extern void (*Enc28J60_SetRxEnd)(Enc28J60 self, const uint16_t addr);
uint16_t Enc28J60_GetRxEnd(Enc28J60 self);
void Enc28J60_SetRxReadPtr(Enc28J60 self, const uint16_t addr);
uint16_t Enc28J60_GetRxReadPtr(Enc28J60 self);
extern void (*Enc28J60_SetTxStart)(Enc28J60 self, const uint16_t addr);
uint16_t Enc28J60_GetTxStart(Enc28J60 self);
extern void (*Enc28J60_SetTxEnd)(Enc28J60 self, const uint16_t addr);
uint16_t Enc28J60_GetTxEnd(Enc28J60 self);
extern void (*Enc28J60_SetReadPtr)(Enc28J60 self, const uint16_t addr);
uint16_t Enc28J60_GetReadPtr(Enc28J60 self);
extern void (*Enc28J60_SetWritePtr)(Enc28J60 self, const uint16_t addr);
uint16_t Enc28J60_GetWritePtr(Enc28J60 self);

/* Ethernet Controller Configuration */

extern void (*Enc28J60_SetReceptionEnable)(Enc28J60 self, const uint8_t enable);
uint8_t Enc28J60_GetReceptionEnable(Enc28J60);
void Enc28J60_SetInterruptEnable(Enc28J60 self, const uint8_t enable_mask);
uint8_t Enc28J60_GetInterruptEnable(Enc28J60 self);

/* Ethernet Packet Filters */

void Enc28J60_SetBroadcastFilterEnable(Enc28J60 self, const uint8_t enable);
void Enc28J60_SetMulticastFilterEnable(Enc28J60 self, const uint8_t enable);
void Enc28J60_SetHashTableFilterEnable(Enc28J60 self, const uint8_t enable);
void Enc28J60_SetMagicPacketFilterEnable(Enc28J60 self, const uint8_t enable);
void Enc28J60_SetPatternMatchFilterEnable(Enc28J60 self, const uint8_t enable);
void Enc28J60_SetInvalidCrcFilterEnable(Enc28J60 self, const uint8_t enable);
void Enc28J60_SetAndOrFilterEnable(Enc28J60 self, const uint8_t enable);
void Enc28J60_SetUnicastFilterEnable(Enc28J60 self, const uint8_t enable);

/* Ethernet Controller Status */

uint8_t Enc28J60_GetInterruptFlags(Enc28J60 self);
uint8_t Enc28J60_GetPacketCount(Enc28J60 self);

/* Clock */

extern uint8_t (*Enc28J60_IsClockReady)(Enc28J60 self);

/* MAC Configuration */

extern void (*Enc28J60_SetMacReceiveEnable)(Enc28J60 self, const uint8_t enable);
uint8_t Enc28J60_GetMacReceptionEnable(Enc28J60 self);
extern void (*Enc28J60_SetTransmitChecksumEnable)(Enc28J60 self, const uint8_t enable);
uint8_t Enc28J60_GetTransmitChecksumEnable(Enc28J60 self);
extern void (*Enc28J60_SetDuplexMode)(Enc28J60 self, const uint8_t full);
uint8_t Enc28J60_GetDuplexMode(Enc28J60 self);
extern void (*Enc28J60_SetPaddingMode)(Enc28J60 self, const uint8_t mode);
uint8_t Enc28J60_GetPaddingMode(Enc28J60 self);
extern void (*Enc28J60_SetFrameLengthCheckEnable)(Enc28J60 self, const uint8_t enable);
uint8_t Enc28J60_GetFrameLengthCheckEnable(Enc28J60 self);
extern void (*Enc28J60_SetDeferTransmissionEnable)(Enc28J60 self, const uint8_t enable);
uint8_t Enc28J60_GetDeferTransmissionEnable(Enc28J60 self);
extern void (*Enc28J60_SetNoBackoffDuringBackpressureEnable)(Enc28J60 self, const uint8_t enable);
uint8_t Enc28J60_GetNoBackoffDuringBackpressureEnable(Enc28J60 self);
extern void (*Enc28J60_SetNoBackoffEnable)(Enc28J60 self, const uint8_t enable);
uint8_t Enc28J60_GetNoBackoffEnable(Enc28J60 self);
extern void (*Enc28J60_SetMaxFrameLen)(Enc28J60 self, const uint16_t max);
uint16_t Enc28J60_GetMaxFrameLen(Enc28J60 self);
extern void (*Enc28J60_SetInterPacketGapDelayForBackToBack)(Enc28J60 self, const uint8_t delay);
uint8_t Enc28J60_GetInterPacketGapDelayForBackToBack(Enc28J60 self);
extern void (*Enc28J60_SetInterPacketGapDelayForNonBackToBack)(Enc28J60 self, const uint16_t delay);
uint16_t Enc28J60_GetInterPacketGapDelayForNonBackToBack(Enc28J60 self);
extern void (*Enc28J60_SetMaxRetransmission)(Enc28J60 self, const uint8_t retmax);
uint8_t Enc28J60_GetMaxRetransmission(Enc28J60 self);
extern void (*Enc28J60_SetCollisionWindow)(Enc28J60 self, const uint8_t colwin);
uint8_t Enc28J60_GetCollisionWindow(Enc28J60 self);
extern void (*Enc28J60_SetMacAddr)(Enc28J60 self, const uint8_t *mac_addr);
void Enc28J60_GetMacAddr(Enc28J60 self, uint8_t *mac_addr);

/* Initialization Wrappers */

extern void (*Enc28J60_InitMac)(Enc28J60 self, const uint8_t full_duplex, const uint8_t *mac_addr, const uint16_t maxFrameLen);
void Enc28J60_Init(Enc28J60 self, const uint16_t rxBufSize, const uint8_t full_duplex, const uint8_t *mac_addr, const uint16_t maxFrameLen);

/* Ethernet Frame Transmission */

void Enc28J60_Transmit(Enc28J60 self, const uint8_t *buf, const uint16_t len);

#endif  /* D_FakeEnc28J60_H */
