#pragma once

/* register banks */
enum {
	BANK0		= 0,
	BANK1		= 1,
	BANK2		= 2,
	BANK3		= 3,
};

/* common registers */
enum {
	EIE		= 0x1B,
	EIR		= 0x1C,
	ESTAT		= 0x1D,
	/* TODO ECON2	= 0x1E, */
	ECON1		= 0x1F,
};

/* bank 0 registers */
enum {
	ERDPTL		= 0x00,
	ERDPTH		= 0x01,
	EWRPTL		= 0x02,
	EWRPTH		= 0x03,
	ETXSTL		= 0x04,
	ETXSTH		= 0x05,
	ETXNDL		= 0x06,
	ETXNDH		= 0x07,
	ERXSTL		= 0x08,
	ERXSTH		= 0x09,
	ERXNDL		= 0x0A,
	ERXNDH		= 0x0B,
	ERXRDPTL	= 0x0C,
	ERXRDPTH	= 0x0D,
	/* TODO ERXWRPTL	= 0x0E, */
	/* TODO ERXWRPTH	= 0x0F, */
	/* TODO EDMASTL		= 0x10, */
	/* TODO EDMASTH		= 0x11, */
	/* TODO EDMANDL		= 0x12, */
	/* TODO EDMANDH		= 0x13, */
	/* TODO EDMADSTL	= 0x14, */
	/* TODO EDMADSTH	= 0x15, */
	/* TODO EDMACSL		= 0x16, */
	/* TODO EDMACSH		= 0x17, */
	/* n/a (0x18) */
	/* n/a (0x19) */
	/* reserved (0x1A) */
};

/* bank 1 registers */
enum {
	/* TODO EHT0	= 0x00, */
	/* TODO EHT1	= 0x01, */
	/* TODO EHT2	= 0x02, */
	/* TODO EHT3	= 0x03, */
	/* TODO EHT4	= 0x04, */
	/* TODO EHT5	= 0x05, */
	/* TODO EHT6	= 0x06, */
	/* TODO EHT7	= 0x07, */
	/* TODO EPMM0	= 0x08, */
	/* TODO EPMM1	= 0x09, */
	/* TODO EPMM2	= 0x0A, */
	/* TODO EPMM3	= 0x0B, */
	/* TODO EPMM4	= 0x0C, */
	/* TODO EPMM5	= 0x0D, */
	/* TODO EPMM6	= 0x0E, */
	/* TODO EPMM7	= 0x0F, */
	/* TODO EPMCSL	= 0x10, */
	/* TODO EPMCSH	= 0x11, */
	/* n/a (0x12) */
	/* n/a (0x13) */
	/* TODO EPMOL	= 0x14, */
	/* TODO EPMOH	= 0x15, */
	/* reserved (0x16) */
	/* reserved (0x17) */
	ERXFCON		= 0x18,
	EPKTCNT		= 0x19,
	/* reserved (0x1A) */
};

/* bank 2 registers */
enum {
	MACON1		= 0x00,
	/* reserved (0x01) */
	MACON3		= 0x02,
	MACON4		= 0x03,
	MABBIPG		= 0x04,
	/* n/a (0x05) */
	MAIPGL		= 0x06,
	MAIPGH		= 0x07,
	MACLCON1	= 0x08,
	MACLCON2	= 0x09,
	MAMXFLL		= 0x0A,
	MAMXFLH		= 0x0B,
	/* reserved (0x0C) */
	/* reserved (0x0D) */
	/* reserved (0x0E) */
	/* n/a (0x0F) */
	/* reserved (0x10) */
	/* reserved (0x11) */
	MICMD		= 0x12,
	/* n/a (0x13) */
	MIREGADR	= 0x14,
	/* reserved (0x15) */
	MIWRL		= 0x16,
	MIWRH		= 0x17,
	MIRDL		= 0x18,
	MIRDH		= 0x19,
	/* reserved (0x1A) */
};

/* bank 3 registers */
enum {
	MAADR5		= 0x00,
	MAADR6		= 0x01,
	MAADR3		= 0x02,
	MAADR4		= 0x03,
	MAADR1		= 0x04,
	MAADR2		= 0x05,
	/* TODO EBSTSD	= 0x06, */
	/* TODO EBSTCON	= 0x07, */
	/* TODO EBSTCSL	= 0x08, */
	/* TODO EBSTCSH	= 0x09, */
	MISTAT		= 0x0A,
	/* n/a (0x0B) */
	/* n/a (0x0C) */
	/* n/a (0x0D) */
	/* n/a (0x0E) */
	/* n/a (0x0F) */
	/* n/a (0x10) */
	/* n/a (0x11) */
	/* TODO EREVID	= 0x12, */
	/* n/a (0x13) */
	/* n/a (0x14) */
	/* TODO ECOCON	= 0x15, */
	/* reserved (0x16) */
	/* TODO EFLOCON	= 0x17, */
	/* TODO EPAUSL	= 0x18, */
	/* TODO EPAUSH	= 0x19, */
	/* reserved (0x1A) */
};

/* PHY registers */

enum {
	PHCON1		= 0,
	/* TODO PHSTAT1	= 0x01, */
	/* TODO PHID1	= 0x02, */
	/* TODO PHID2	= 0x03, */
	/* TODO PHCON2	= 0x10, */
	/* TODO PHSTAT2	= 0x11, */
	/* TODO PHIE	= 0x12, */
	/* TODO PHIR	= 0x13, */
	/* TODO PHLCON	= 0x14, */
};

/* Common register bits **********************************************/

/* ESTAT register bits */
enum {
	CLKRDY		= 0,
};

/* EIE register bits */
enum {
	TXIE		= 3,
	INTIE		= 7,
};

/* EIR register bits */
enum {
	TXIF		= 3,
};

/* ECON1 register bits */
enum {
	BSEL0		= 0,
	BSEL1		= 1,
	RXEN		= 2,
	TXRTS		= 3,
};

/* bank 1 register bits **********************************************/

/* ERXFCON register bits */
enum {
	BCEN		= 0,
	MCEN		= 1,
	HTEN		= 2,
	MPEN		= 3,
	PMEN		= 4,
	CRCEN		= 5,
	ANDOR		= 6,
	UCEN		= 7,
};

/* bank 2 register bits *********************************************/

/* MACON1 register bits */
enum {
	MARXEN		= 0,
	RXPAUS		= 2,
	TXPAUS		= 3,
};

/* MACON3 register bits */
enum {
	FULDPX		= 0,
	FRMLNEN		= 1,
	TXCRCEN		= 4,
	PADCFG0		= 5,
	PADCFG1		= 6,
	PADCFG2		= 7,
};

/* MACON4 register bits */
enum {
	NOBKOFF		= 4,
	BPEN		= 5,
	DEFER		= 6,
};
/* MICMD register bits */
enum {
	MIIRD		= 0,
};

/* PHY register bits *************************************************/

/* PHCON1 register bits */
enum {
	PDPXMD		= 8,
	/* TODO PPWRSV	= 11, */
	/* TODO PLOOPBK	= 14, */
	/* TODO PRST	= 15, */
};

/* PHSTAT1 register bits */
/*enum {*/
	/* TODO JBSTAT	= 1, */
	/* TODO LLSTAT	= 2, */
	/* TODO PHDPX	= 11, */
	/* TODO PDFPX	= 12, */
/*};*/

/* PHCON2 register bits */
/* enum {*/
	/* TODO HDLDIS	= 8, */
	/* TODO JABBER	= 10, */
	/* TODO TXDIS	= 13, */
	/* TODO FRCLNK	= 14, */
/*};*/

/* PHSTAT2 register bits */
/* enum {*/
	/* TODO PLRITY	= 5, */
	/* TODO DPXSTAT	= 9, */
	/* TODO LSTAT	= 10, */
	/* TODO COLSTAT	= 11, */
	/* TODO RXSTAT	= 12, */
	/* TODO TXSTAT	= 13, */
/*};*/

/* PHIE register bits */
/* enum {*/
	/* TODO PGEIE	= 1, */
	/* TODO PLNKIE	= 4, */
/*};*/

/* PHIR register bits */
/* enum {*/
	/* TODO PGIF	= 2, */
	/* TODO PLNKIF	= 4, */
/*};*/

/* PHLCON reigster bits */
/* enum {*/
	/* TODO STRCH	= 1, */
	/* TODO LFRQ0	= 2, */
	/* TODO LFRQ1	= 3, */
	/* TODO LBCFG0	= 4, */
	/* TODO LBCFG1	= 5, */
	/* TODO LBCFG2	= 6, */
	/* TODO LBCFG3	= 7, */
	/* TODO LACFG0	= 8, */
	/* TODO LACFG1	= 9, */
	/* TODO LACFG2	= 10, */
	/* TODO LACFG3	= 11, */
/*};*/
