#ifndef D_Enc28J60_IO_H
#define D_Enc28J60_IO_H

/**********************************************************************
 *
 * Enc28J60_IO is responsible for ...
 *
 **********************************************************************/

#include "Pin.h"
#include <stdint.h>

typedef struct Enc28J60_IOStruct * Enc28J60_IO;

Enc28J60_IO Enc28J60_IO_Create(const Pin csPin);
void Enc28J60_IO_Destroy(Enc28J60_IO);

void Enc28J60_IO_Select(Enc28J60_IO self, const uint8_t select);
extern uint8_t (*Enc28J60_IO_Rcr)(Enc28J60_IO self, const uint8_t addr);
extern uint8_t (*Enc28J60_IO_Rmr)(Enc28J60_IO self, const uint8_t addr);
extern uint16_t (*Enc28J60_IO_Rpr)(Enc28J60_IO self, const uint8_t addr);
extern uint8_t (*Enc28J60_IO_Rbm)(Enc28J60_IO self, const uint8_t addr, uint8_t *buf, const uint16_t len);
extern void (*Enc28J60_IO_Wcr)(Enc28J60_IO self, const uint8_t addr, const uint8_t value);
extern void (*Enc28J60_IO_Wpr)(Enc28J60_IO self, const uint8_t addr, const uint16_t value);
extern void (*Enc28J60_IO_Wbm)(Enc28J60_IO self, const uint16_t addr, const uint8_t *buf, const uint16_t len);
extern void (*Enc28J60_IO_Bfs)(Enc28J60_IO self, const uint8_t addr, const uint8_t mask);
extern void (*Enc28J60_IO_Bfc)(Enc28J60_IO self, const uint8_t addr, const uint8_t mask);
extern void (*Enc28J60_IO_Src)(Enc28J60_IO self);
extern void (*Enc28J60_IO_Bank)(Enc28J60_IO self, const uint8_t bank);

#endif  /* D_FakeEnc28J60_IO_H */
