#ifndef D_SPI_H
#define D_SPI_H

/**********************************************************
 *
 * SPI is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void SPI_Create(volatile uint8_t *spcr, volatile uint8_t *spsr, volatile uint8_t *spdr);
void SPI_Destroy(void);

void SPI_SetInterruptEnable(const uint8_t enable);
void SPI_Enable(const uint8_t enable);
void SPI_SetDataOrder(const uint8_t lsb_first);
void SPI_SetMasterMode(const uint8_t master_mode);
void SPI_SetClockPolarity(const uint8_t mode);
void SPI_SetClockPhase(const uint8_t mode);
void SPI_SetClockRate(const uint8_t rate);
extern uint8_t (*SPI_GetInterruptFlag)(void);
uint8_t SPI_GetWriteCollisionFlag(void);
extern uint8_t (*SPI_Read)(void);
extern void (*SPI_Write)(const uint8_t byte);

extern uint8_t (*SPI_Trx)(const uint8_t byte);

#endif  /* D_FakeSPI_H */
