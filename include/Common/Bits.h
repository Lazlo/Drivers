#ifndef D_Bits_H
#define D_Bits_H

/**********************************************************
 *
 * Bits is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

uint8_t BitIsSet(volatile const uint8_t *var, const uint8_t offset);
void SetBit(volatile uint8_t *var, const uint8_t offset, const uint8_t set);
void UpdateBits(volatile uint8_t *var, const uint8_t mask, const uint8_t value);

#endif  /* D_FakeBits_H */
