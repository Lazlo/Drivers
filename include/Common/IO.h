#ifndef D_IO_H
#define D_IO_H

/**********************************************************
 *
 * IO is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void IO_Create(void);
void IO_Destroy(void);

uint8_t IO_Read(const volatile uint8_t *addr);
void IO_Write(volatile uint8_t *addr, const uint8_t data);

#endif  /* D_FakeIO_H */
