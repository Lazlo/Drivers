#ifndef D_IOSpy_H
#define D_IOSpy_H

/**********************************************************
 *
 * IOSpy is responsible for ...
 *
 **********************************************************/

#include <stdint.h>

void IOSpy_Create(const uint8_t recordReadCalls);
void IOSpy_Destroy(void);

volatile uint8_t *IOSpy_GetLastReadAddr(void);
uint8_t IOSpy_Read(const volatile uint8_t *addr);
uint8_t IOSpy_GetCallsMadeToRead(void);
volatile uint8_t *IOSpy_GetReadAddr(void);

volatile uint8_t *IOSpy_GetLastWriteAddr(void);
uint8_t IOSpy_GetLastWriteValue(void);
void IOSpy_Write(volatile uint8_t *addr, const uint8_t data);

#endif  /* D_FakeIOSpy_H */
