#ifndef D_DriversBuildTime_H
#define D_DriversBuildTime_H

///////////////////////////////////////////////////////////////////////////////
//
//  DriversBuildTime is responsible for recording and reporting when
//  this project library was built
//
///////////////////////////////////////////////////////////////////////////////

class DriversBuildTime
  {
  public:
    explicit DriversBuildTime();
    virtual ~DriversBuildTime();
    
    const char* GetDateTime();

  private:
      
    const char* dateTime;

    DriversBuildTime(const DriversBuildTime&);
    DriversBuildTime& operator=(const DriversBuildTime&);

  };

#endif  // D_DriversBuildTime_H
