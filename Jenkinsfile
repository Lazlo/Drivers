@Library('pipeline-library') _

pipeline {
    agent any
    triggers {
        cron('@daily')
    }
    environment {
        CPPUTEST_PROJECT_NAME   = '/tools/cpputest/master'
        CPPUTEST_ARTIFACT_NAME  = 'cpputest_*.tar.gz'
        CPPUTEST_HOME           = "${env.WORKSPACE}/usr"
    }
    stages {
        stage('Clean') {
            steps {
                echo 'Cleaning ...'
                sh 'git clean -dfx'
            }
        }
        stage('Setup') {
            steps {
                echo 'Setting up ...'
                timeout(time: 2, unit: 'MINUTES') {
                    retry(5) {
                        sh 'sudo apt-get -q install -y make g++'
                    }
                }
                copyArtifacts projectName: "${CPPUTEST_PROJECT_NAME}", filter: "${CPPUTEST_ARTIFACT_NAME}"
                sh "tar xzf ${CPPUTEST_ARTIFACT_NAME}"
            }
        }
        stage('Build') {
            steps {
                echo 'Building ...'
                sh 'make Drivers_tests'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing ...'
                sh 'make vtest'
                sh './Drivers_tests -ojunit'
            }
            post {
                always {
                    junit 'cpputest_*.xml'
                }
            }
        }
    }
    post {
        always {
            commonStepNotification()
        }
    }
}
